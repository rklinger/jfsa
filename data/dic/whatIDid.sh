cp ~/ub/data/subjectivity-lexicon/subjclueslen1-HLTEMNLP05.tff .

cat ../input/spina/*.csv | grep "target" | cut -f 5 | tr ' ' '\n' | sort | uniq -c | sort -g -k 1 > mytargets.list

cut -d ' ' -f 3,6 subjclueslen1-HLTEMNLP05.tff | sed 's/word1=//' | sed 's/priorpolarity=//' | tr ' ' '\t' > wilson2005.csv

curl https://trungng.googlecode.com/svn/asc/SentiWordNet_3.0.0.txt > SentiWordNet_3.0.0.txt

cat SentiWordNet_3.0.0.txt | perl -e 'while(<>){@a=split() ; $score = $a[2]-$a[3] ; if ($score < 0) { print("negative\t$_")} ; if ($score > 0) {print("positive\t$_")}}' | cut -f 1,6 | sed 's/#[0-9]//g' | while read i ; do SCORE=`echo $i|cut -f 1` ; echo $i | cut -f 2- | tr ' ' '\n' | while read j ; do echo "$SCORE\t$j" ; done ; done > sentiwordnet3.txt
awk ' { t = $1; $1 = $2; $2 = t; print; } ' sentiwordnet3.txt | tr ' ' '\t' > tmp ; mv tmp sentiwordnet3.txt 
