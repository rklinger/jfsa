/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */


package exp

import java.lang.String
import sc.rk.targsubj.data.{FlagDiff, Document, Token, Sentence}
import sc.rk.targsubj._
import cc.factorie.{Diff, DiffList}
import data.FlagDiff

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 02.07.13
 * Time: 16:14
 */

/**
 * In this class, there is only a small test of the data structures.
 */
object TestRemoveRelation {
  val obj = new SpanRelationObjective
  def main(args:Array[String]) : Unit = {
    val str = "Here comes a test text."
    val doc = new Document("TestDocument", "", str)
    val sentence = new Sentence(doc)(null)
    val t1 = new Token(sentence, "Token1 ") ; t1.setSentence(sentence)
    val t2 = new Token(sentence, "token2 ") ; t2.setSentence(sentence)
    val t3 = new Token(sentence, "token3 ") ; t3.setSentence(sentence)
    val t4 = new Token(sentence, "token4 ") ; t4.setSentence(sentence)
    val t5 = new Token(sentence, "token5 ") ; t5.setSentence(sentence)
    val t6 = new Token(sentence, "token6") ; t6.setSentence(sentence)

    val subjSeq = new NerSpan(doc,"subjective",5,1)(null)
    val previousSubjSpan = new NerSpan(doc,"subjective",4,1)(null)
    val tarSeq = new NerSpan(doc,"target",2,1)(null)
    val goldSubj = new GoldNerSpan(doc,"subjective",5,1,"","")(null)
    val goldTarg = new GoldNerSpan(doc,"target",2,1,"","")(null)
    goldSubj.relations += new GoldTSRelation(goldSubj,goldTarg)

    printstuff(doc,tarSeq,null,null)
    println("\nMake relations")
    val d0 = new DiffList
    val relation = subjSeq.addNerSpanAsRelation(tarSeq)(d0)
    val relation2 = previousSubjSpan.addNerSpanAsRelation(tarSeq)(d0)
    printstuff(doc,tarSeq,relation,relation2)
    println("\nUndo")
    d0.undo
    printstuff(doc,tarSeq,relation,relation2)
    System.exit(1)


    println("\nREMOVE")

    val d = new DiffList
    val diff = subjSeq.RemoveRelationDiff(relation)
    diff.redo // actually removing from the list of relations
    if (d != null) d += diff // to allow undo and another redo
    if (d != null) relation.delete(d)

    printstuff(doc,tarSeq,relation,relation2)
    println("\nUNDO")
    d.undo
    printstuff(doc,tarSeq,relation,relation2)

    println("---------------------------------------")
    printstuff(doc,tarSeq,relation,relation2)
    println("\nREMOVE ALL")
    val d2 = new DiffList
    subjSeq.relations.foreach(r => {
      subjSeq.removeRelation(r)(d2)
    })
    subjSeq.delete(d2)
    printstuff(doc,tarSeq,relation,relation2)
    println("\nUNDO")
    d2.undo
    printstuff(doc,tarSeq,relation,relation2)
    println("\nREDO")
    d2.redo
    printstuff(doc,tarSeq,relation,relation2)
    println("\nUNDO AGAIN")
    d2.undo
    // NOW TEST THE NEXT THING...
    println("\n\nNEXT BIG THING... Join subjspan with previous span.") // SOMETHING WEIRD IS HAPPENING HERE!
    val d3 = new DiffList
    var span = subjSeq
    if (span.hasPredecessor(1)) {
      println("Doing something...")
      val prevSpans = span.predecessor(1).endsSpansOfClass[NerSpan]
      val prevSpan: NerSpan = if (prevSpans.size > 0) prevSpans.head else null
      if (prevSpan != null && prevSpan.label.intValue == span.label.intValue) {
        span.prepend(prevSpan.length)(d3)
        prevSpan.delete(d3)
        prevSpan.deleteMeAsTargetFromAllRelations(d3)
        prevSpan.removeAllRelations()(d3)
        (span.relations++span.document.relationsWithTarget(span)).foreach(r => d3 += new FlagDiff(r))
      }
    }
    printstuff(doc,tarSeq,relation,relation2)
    println("\nUNDO")
    d3.undo
    printstuff(doc,tarSeq,relation,relation2)
    // and next
    val d4 = new DiffList
    span = tarSeq
    println("\nNext big thing... try to remove the span as target from all others...")
    var labelValue = TSNerDomain._elements.filter(x => x.category == "subjective").head
    if (span.label.categoryValue == "target") { // relevant when being joint
      println("Doing something...")
      span.deleteMeAsTargetFromAllRelations(d4)
      span.label.set(labelValue)(d4)
    }
    printstuff(doc,tarSeq,relation,relation2)
    println("UNDO")
    d4.undo
    printstuff(doc,tarSeq,relation,relation2)
  }
  def printstuff(doc:Document,tarSeq:NerSpan,relation:TSRelation,relation2:TSRelation) = {
    doc.tsRelations.foreach(x => println(x))
    doc.spansOfClass[NerSpan].foreach(x => println(x))
    if (relation != null) println(relation+" ==> "+relation.partialScore)
    if (relation != null)    println(relation2+" ==> "+relation2.partialScore)
  }
}
