/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

// this is just a small experiment to test a dictionary. Not relevant.

package exp

import sc.rk.targsubj.data.LoadTargetSubjectivity
import sc.rk.targsubj.GoldNerSpan
import io.Source
import java.io.File
import collection.mutable

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 27.03.13
 * Time: 13:26
 */
object CheckDictionaryContent {
//  val dicfile = "data/dic/camera_attributes"
//  val trainFile = "data/input/jdpa/camera.csv"
  val dicfile = "data/dic/car_attributes"
  val trainFile = "data/input/jdpa/car.csv"
  def main(a:Array[String]) = {
    val dic = new mutable.HashSet[String]
    Source.fromFile(new File(dicfile)).getLines().foreach(x => {dic += x.split("\t")(0)})
    dic.foreach(x => println(x))
    val docs = LoadTargetSubjectivity.fromFilename(trainFile,true,false,false)
    var foundInDic = 0
    var notFoundInDic = 0
    for (d <- docs) {
      for(target <- d.spansOfClass[GoldNerSpan].filter(_.labelString=="target")) {
        println(target.phrase)
        if (dic.exists(x => target.phrase.contains(x)))
          foundInDic += 1
        else
          notFoundInDic += 1
      }
    }
    println("found="+foundInDic+" ; not-found="+notFoundInDic+" (sum="+(foundInDic+notFoundInDic)+")")
  }
}
