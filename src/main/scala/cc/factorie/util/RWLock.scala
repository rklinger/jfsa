package cc.factorie.util

/**
 * ok, that is now weird.
 * This file was originally part of FACTORIE and has only been made Serializable, no other changes.
 *
 */
class RWLock extends Serializable {
  private val _lock = new java.util.concurrent.locks.ReentrantReadWriteLock
  def readLock() { _lock.readLock().lock() }
  def writeLock() { _lock.writeLock().lock() }
  def readUnlock() { _lock.readLock().unlock() }
  def writeUnlock() { _lock.writeLock().unlock() }
  def withReadLock[T](value: => T) = {
    readLock()
    try { value } finally { readUnlock() }
  }
  def withWriteLock[T](value: => T) = {
    writeLock()
    try { value } finally { writeUnlock() }
  }
}
