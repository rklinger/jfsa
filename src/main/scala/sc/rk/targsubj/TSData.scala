/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

// This file contains the fundamental data structures used in the models, objectives and samplers.
// It also contains the feature definitions for the models.

package sc.rk.targsubj

import data._
import cc.factorie._
import app.strings.Stopwords
import collection.mutable.{ArrayBuffer, ListBuffer}
import collection.mutable
import edu.stanford.nlp.trees.{GrammaticalRelation, TreeGraphNode}
import scala.math._
import java.io.File
import tools.{WilsonDictionaryPriorPolarityChecker, SentiWordNetDictionaryPriorPolarityChecker, NegationChecker}
import scala.collection.JavaConversions._

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 04.02.13
 * Time: 15:42
 */

// probably not in use
object PosDomain extends CategoricalDomain[String]
class PosLabel(val token:Token, targetValue:String) extends LabeledCategoricalVariable(targetValue) { def domain = PosDomain }

class SubjectiveLabel(val sentence:Sentence, targetValue:Boolean) extends LabeledBooleanVariable(targetValue)

// Should contain target and subjective
//val names = new scala.collection.mutable.ArrayBuffer[String] ++=
object TSNerDomain extends CategoricalDomain[String](List("subjective", "target"))

// the label variable on predicted spans
class TSSpanNerLabel(span:NerSpan, initialValue:String) extends SpanNerLabel(span, initialValue) {
  def domain = TSNerDomain
}

class SRLTokenEdge(val label:String, val target: Token, val forward: Boolean) {
  def direction = if (forward) "-UP" else "-DOWN"
}
/*
To denote if a change proposal is made out of Gold, never really used as variable.
 */
class GoldDenoter()(implicit d: DiffList) extends Var {
  def domain = BooleanDomain
  def value = 0
  val diff = new NewGoldDenoterDiff
  d += diff
  case class NewGoldDenoterDiff() extends Diff {
    def undo = {}
    def redo = {}
    def variable = GoldDenoter.this
  }
}

object PolarityDomain extends CategoricalDomain[String](List("Mixed", "Negative", "Neutral", "Positive", "Unknown"))

class Polarity(span:NerSpan, initialValue:String) extends SpanNerLabel(span, initialValue) {
  def domain = PolarityDomain
}

class GoldTSRelation(val subjectiveNerSpan:GoldNerSpan,val targetNerSpan:GoldNerSpan) {
  assert (targetNerSpan.label.categoryValue == "target")
  assert (subjectiveNerSpan.label.categoryValue == "subjective")

  def =~=(that:TSRelation) : Boolean = { that.subjectiveNerSpan.partialScore(this.subjectiveNerSpan) > 0 && that.targetNerSpan.partialScore(this.targetNerSpan) > 0 }
  def ===(that:TSRelation) : Boolean = { this.subjectiveNerSpan === that.subjectiveNerSpan && this.targetNerSpan === that.targetNerSpan }
  def ===(that:GoldTSRelation) : Boolean = { this.subjectiveNerSpan === that.subjectiveNerSpan && this.targetNerSpan === that.targetNerSpan }

  override def toString : String = "TSRelation:"+subjectiveNerSpan+" -> "+targetNerSpan

}

class TSRelation(val subjectiveNerSpan:NerSpan,val targetNerSpan:NerSpan)(implicit d: DiffList) extends Var {
  assert (targetNerSpan.label.categoryValue == "target")
  assert (subjectiveNerSpan.label.categoryValue == "subjective")
  def value = (subjectiveNerSpan,targetNerSpan)
  def domain = RelationFeaturesDomain // xxx
  var present = false
  val diff = new NewRelationDiff
  diff.redo
  if (d != null) d += diff

  def delete(implicit d: DiffList) = {
    val diff = new DeleteRelationDiff
    diff.redo
    if (d != null) d += diff
  }
  case class DeleteRelationDiff() extends Diff {
    def undo = { present = true }
    def redo = { present = false }
    def variable = if (present) TSRelation.this else null // new
  }
  case class NewRelationDiff() extends Diff {
    def undo = { present = false }
    def redo = { present = true }
    def variable = if (present) TSRelation.this else null
  }

  def ===(that:TSRelation) : Boolean = { this.subjectiveNerSpan === that.subjectiveNerSpan && this.targetNerSpan === that.targetNerSpan }
  def ===(that:GoldTSRelation) : Boolean = { this.subjectiveNerSpan === that.subjectiveNerSpan && this.targetNerSpan === that.targetNerSpan }

  def goldRelations = subjectiveNerSpan.document.goldNerSpansSubjective.map(_.relations).flatten
  def isCorrect = goldRelations.exists(r => r === this)
  def associatedGoldRelation = goldRelations.find(r => r === this)

  override def toString : String = "TSRelation:"+subjectiveNerSpan+" -> "+targetNerSpan+"; "+(if (present) "present" else "nonpresent")

  def partialScore : Double = {
    val subjSeq = subjectiveNerSpan
    val tarSeq = targetNerSpan
    var result = 0.0
    val getAGold = subjSeq.head.document.goldTsRelations.find(goldRel => subjSeq.partialScore(goldRel.subjectiveNerSpan) > 0 && tarSeq.partialScore(goldRel.targetNerSpan) > 0)
    if (getAGold.isDefined) {
      val score = 0.5*subjSeq.partialScore(getAGold.get.subjectiveNerSpan) + 0.5*tarSeq.partialScore(getAGold.get.targetNerSpan)
      result = score
    } else {
      //result = -1
    }
    result
  }
}

// To mark gold spans on sentences with target or subjective
class TSGoldSpanNerLabel(span:GoldNerSpan, initialValue:String) extends GoldSpanNerLabel(span, initialValue) {
  def domain = TSNerDomain
}

class GoldPolarity(span:GoldNerSpan, initialValue:String) extends GoldSpanNerLabel(span, initialValue) { // this inheritance should not be a problem, as no domain or so is attached
  def domain = PolarityDomain
}

// the spans to be sampled, representing either a target or a subjective
class NerSpan(doc:Document, labelString:String, start:Int, initialLength:Int, polarityString:String = "Unknown")(implicit d:DiffList) extends TokenSpan(doc, start, initialLength) with TokenSpanMention {
//  def length = this.toSeq.size
  val goldNerSpansOnDocument = this.head.document.spansOfClass[GoldNerSpan]
  val label = new TSSpanNerLabel(this, labelString)
  val polarity = new Polarity(this, polarityString)
  val relations = new ArrayBuffer[TSRelation]
  def ===(other: GoldNerSpan) : Boolean = {this.head.position == other.head.position && this.last.position == other.last.position && this.label.value == other.label.value}
  def ===(other: NerSpan) : Boolean = {this.head.position == other.head.position && this.last.position == other.last.position && this.label.value == other.label.value}
  def ===(other: Seq[Token]) : Boolean = {this.head.position == other.head.position && this.last.position == other.last.position}
  def =~=(other: GoldNerSpan) : Boolean = this.partialScore(other) > 0
  val correct = goldNerSpansOnDocument.exists(sampledSpan => sampledSpan.head == this.head && sampledSpan.last == this.last && sampledSpan.label.value == this.label.value)
  def hasCorrectPolarity(acceptPartialMatchAsTrue:Boolean) : Boolean = { // xxx
    if (label.categoryValue == "target") return true
    val subjectiveGoldSpans = goldNerSpansOnDocument.filter(s => s.label.categoryValue == "subjective")
    if (acceptPartialMatchAsTrue)
      subjectiveGoldSpans.exists(s => this =~= s && s.polarity.value == polarity.value) // TODO debug!
    else
      subjectiveGoldSpans.exists(s => s === this && s.polarity.value == polarity.value)
  }
  def isCorrect = doc.spansOfClass[GoldNerSpan].exists(goldSpan => {goldSpan.head == this.head && goldSpan.last == this.last && goldSpan.label.intValue == this.label.intValue})
  def isPositive = this.polarity.categoryValue == "Positive"
  def isNegative = this.polarity.categoryValue == "Negative"
  def getPolarityString = if (isPositive) "Positive" else "Negative"
  def isSubjective = this.label.categoryValue == "subjective"
  def isTarget = this.label.categoryValue == "target"
  def bestGoldSpan = NerSpan.maxNumOfSameTokens(this,this.label.value,goldNerSpansOnDocument)._3
  def bestGoldSpanWithPolarityCheck = NerSpan.maxNumOfSameTokens(this,this.label.value,this.polarity.value,goldNerSpansOnDocument)._3
  //  System.err.println("Generating NerSpan "+(for(t <- this) yield t.docSubstring).mkString("_")+" as "+labelString+", "+start+", "+length+" "+(if (correct) "true" else "false")+
  //    " frac="+(NerSpan.maxNumOfSameTokens(this,this.label.value,goldNerSpansOnDocument) / this.length.toDouble))
  override def toString = "NerSpan("+head.sentencePosition+","+(last.sentencePosition-head.sentencePosition+1)+","+label.categoryValue+":"+polarity.categoryValue+":"+this.phrase+"; "+(if (present) "present" else "nonpresent")+")"
  def nextNERSpan : NerSpan = {
    val sortedSpans = doc.spansOfClass[NerSpan].filter(s => s.head.sentencePosition > this.last.sentencePosition).sortWith((a1,a2) => a1.head.sentencePosition < a2.head.sentencePosition)
    if (sortedSpans.nonEmpty) sortedSpans.head else null.asInstanceOf[NerSpan]
  }
  def nextNERSpanOfLabel(labelString : String) : NerSpan = {
    val sortedSpans = doc.spansOfClass[NerSpan].filter(s => s.head.sentencePosition > this.last.sentencePosition).filter(f => f.label.categoryValue == labelString).sortWith((a1,a2) => a1.head.sentencePosition < a2.head.sentencePosition)
    if (sortedSpans.nonEmpty) sortedSpans.head else null.asInstanceOf[NerSpan]
  }
  def previousNERSpan : NerSpan = {
    val sortedSpans = doc.spansOfClass[NerSpan].filter(s => s.last.sentencePosition < this.head.sentencePosition).sortWith((a1,a2) => a1.head.sentencePosition < a2.head.sentencePosition)
    if (sortedSpans.nonEmpty) sortedSpans.last else null.asInstanceOf[NerSpan]
  }
  def previousNERSpanOfLabel(labelString : String) : NerSpan = {
    val sortedSpans = doc.spansOfClass[NerSpan].filter(s => s.last.sentencePosition < this.head.sentencePosition).filter(f => f.label.categoryValue == labelString).sortWith((a1,a2) => a1.head.sentencePosition < a2.head.sentencePosition)
    //if (sortedSpans.nonEmpty) println("ALLSPANS "+sortedSpans.map(_.last.sentencePosition).mkString(" < "))
    if (sortedSpans.nonEmpty) sortedSpans.last else null.asInstanceOf[NerSpan]
  }
  // relational stuff
  case class AddRelationDiff(relation:TSRelation) extends Diff {
    def undo = { NerSpan.this.relations -= relation }
    def redo = { NerSpan.this.relations += relation }
//    def variable = NerSpan.this // xxx // TODO not clear if here check for present or not should be used or the relation variable should be used
    def variable =  if (relation.present) relation else null
  }

  case class RemoveRelationDiff(relation: TSRelation) extends Diff {
    def undo = { NerSpan.this.relations += relation }
    def redo = { NerSpan.this.relations -= relation }
//    def variable = NerSpan.this // xxx // TODO not clear if here check for present or not should be used or the relation variable should be used
    def variable =  if (relation.present) relation else null
  }

  // TODO think about: flagDiff the span?
  // only storing relations in subjective nerspans
  def addNerSpanAsRelation(targetSpan: NerSpan)(implicit d: DiffList) : TSRelation = {
    if (this.label.categoryValue == "target") throw new Exception("This should not happen! Relations are only stored in subjective terms!")
//    println(NerSpan.this+" ; "+targetSpan)
    val rel = new TSRelation(NerSpan.this,targetSpan)(d)
    val diff = new AddRelationDiff(rel)
    diff.redo // really adds the argument
    if (d != null) d += diff // to allow undo and another redo
    rel
  }

  def removeRelation(relation: TSRelation)(implicit d: DiffList) = {
    if (relation == null) { // TODO think about why that can happen, still not solved!
      //println("Tried to remove a null-relation from "+relations.mkString(";"))
      //throw new Exception("Called a null relation on "+this)
    }
    else {
      if (this.label.categoryValue == "target") throw new Exception("This should not happen! Relations are only stored in subjective terms!")
      val diff = new RemoveRelationDiff(relation)
      diff.redo // actually removing from the list of relations
      if (d != null) d += diff // to allow undo and another redo
      if (d != null) relation.delete(d)
      // TODO check if evaluation is actually called right!, think about flagdiffs, here and there
      //    if (d != null) d += new FlagDiff(relation)
    }
  }

  def removeAllRelations()(implicit d: DiffList) = {
    if (this.label.categoryValue == "target") {
      //throw new Exception("This should not happen! Relations are only stored in subjective terms!")
      // nothing
    } else {
      for(relationindex <- 0 until relations.size) {

      }
      for(relation <- relations) {
        removeRelation(relation)(d)
      }
    }
  }

  def deleteMeAsTargetFromAllRelations(implicit d: DiffList) = {
    if (this.label.categoryValue != "target") {
      // nothing
    } else {
      val relationsWithTarget = document.relationsWithTarget(this)
      if (d!= null) {
        relationsWithTarget.foreach(x => {
          x.subjectiveNerSpan.removeRelation(x)(d) // perhaps this is a problem? with the empty bug?
        })
      }
    }
  }

//  def partialScore : Double = {
//    val (numOfTrueSuccTokens,maxGoldLength) = NerSpan.maxNumOfSameSucceedingTokens(this.toSeq,label.categoryValue,document.spansOfClass[GoldNerSpan])
//    val fractionOfTrueSuccTokens = if (maxGoldLength == 0) 0 else numOfTrueSuccTokens/maxGoldLength
//    fractionOfTrueSuccTokens
//  }

  def partialScore(gold:GoldNerSpan) : Double = { // xxx
    val debug = false //this.head.sentencePosition == 5 && gold.head.sentencePosition == 5
    if (debug) println("Computing between "+this+" and "+gold+" (+"+this.length+";"+gold.length+"+)")
    if (gold.label.value != this.label.value) 0.0 else {
      var numOfTrueSuccTokens = NerSpan.numOfSameSucceedingTokens(this.toSeq,gold.toSeq)
      if (debug) println("numOfTrueSuccTokens="+numOfTrueSuccTokens)
      val length = scala.math.max(gold.length.toDouble,this.length.toDouble)
      if (debug) println("length="+length)
      if (numOfTrueSuccTokens/length>1) println("Problem with "+(numOfTrueSuccTokens/length)+": "+this+"/"+gold+" ("+numOfTrueSuccTokens+";"+length+")")
      val r = if (length == 0) 0.0 else numOfTrueSuccTokens/length
      val rr = if (this.length > gold.length.toDouble) r+0.1 else r // better value for too long than for too short
      if (debug) println("rr="+rr)
      rr
    }
  }
}

object TestNumOfSameSucceedingTokens {
  def main(args:Array[String]) {
    val str = "Here comes a test text."
    val doc = new Document("TestDocument", "", str)
    val sentence = new Sentence(doc)(null)
    val t1 = new Token(sentence, "Here ") ; t1.setSentence(sentence)
    val t2 = new Token(sentence, "comes ") ; t2.setSentence(sentence)
    val t3 = new Token(sentence, "a ") ; t3.setSentence(sentence)
    val t4 = new Token(sentence, "test ") ; t4.setSentence(sentence)
    val t5 = new Token(sentence, "text ") ; t5.setSentence(sentence)
    val t6 = new Token(sentence, ".") ; t6.setSentence(sentence)

    val subjSeq = new NerSpan(doc,"subjective",5,1)(null)
    val tarSeq = new NerSpan(doc,"target",3,2)(null)
    val goldSubj = new GoldNerSpan(doc,"subjective",5,1,"","")(null)
    val goldTarg = new GoldNerSpan(doc,"target",3,1,"","")(null)
    goldSubj.relations += new GoldTSRelation(goldSubj,goldTarg)
    val getAGold = subjSeq.head.document.goldTsRelations.find(
      goldRel => {val r = subjSeq.partialScore(goldRel.subjectiveNerSpan); println(r) ; r} > 0 &&
        {val r = tarSeq.partialScore(goldRel.targetNerSpan) ; println(r) ; r} > 0)
    if (getAGold.isDefined) {
      val score = 0.5*subjSeq.partialScore(getAGold.get.subjectiveNerSpan) + 0.5*tarSeq.partialScore(getAGold.get.targetNerSpan)
      println("Score:"+getAGold.get+" matches "+getAGold.get+" with "+score+" ("+subjSeq.partialScore(getAGold.get.subjectiveNerSpan)+" and "+tarSeq.partialScore(getAGold.get.targetNerSpan)+")")
    }
  }
}

object NerSpan {
  def numOfSameTokens(span1:TokenSpan, span2:NerSpan#Value) : Double = {
    var count = 0.0
    for(t <- span1.tokens ; if span2.contains(t)) count += 1.0
    count
  }
  // this is only counting the first occurrence in the gold span... but whatever... close enough.
//  def numOfSameSucceedingTokens(span1:TokenSpan, span2:NerSpan#Value) : Double = {
//    var count = 0.0
//    for(i <- 0 until span1.tokens.length) {
//      if (span2.contains(span1(i))) {
//        for(j <- i until span1.tokens.length) {
//          if (! span2.contains(span1(j))) return count
//          count += 1.0
//        }
//      }
//    }
//    count
//  }
  // it is only counting the first overlap... not the second or further, even if that overlap might be longer!
  def numOfSameSucceedingTokens(span1:Seq[Token], span2:Seq[Token]) : Double = { // TODO next! test this
    val debug = false //span1.head.docSubstring == "model" && span1.last.docSubstring=="is" && span2.head.docSubstring == "model" && span2.length == 1 && span1.length==2
    var count = 0.0
    for(i <- 0 until span1.length ; if (count == 0.0)) {
      if (span2.contains(span1(i))) {
        var span1index = i
        var span2index = span2.indexOf(span1(i))
        while(span1index < span1.length && span2index < span2.length && span1(span1index).position == span2(span2index).position ) {
          if (debug) println("Comparing two tokens: "+span1(span1index)+" == "+span2(span2index))
          span1index += 1
          span2index += 1
          count += 1
        }
      }
    }
    if (debug) println("numOfSameSucceedingTokens result for "+span1+" and "+span2+" is "+count)
    count
  }
  // ACL2013 version (parameter types adapted):
  def numOfSameSucceedingTokensACL2013(span1:Seq[Token], span2:Seq[Token]) : Double = {
    var count = 0.0
    for(i <- 0 until span1.length) {
      if (span2.contains(span1(i))) {
        for(j <- i until span1.length) {
          if (! span2.contains(span1(j))) return count
          count += 1.0
        }
      }
    }
    count
  }
  def numOfSameTokens(span1:TokenSpan, span2:NerSpan) : Double = {
    var count = 0.0
    for(t <- span1.tokens ; if span2.contains(t)) count += 1.0
    count
  }
  def maxNumOfSameTokens(span:NerSpan#Value, spanLabel:NerLabel#Value, spans:Seq[GoldNerSpan]) : (Double,Double) = {
    var maxNumOfSameTokens = 0.0
    var maxGoldLength = 0.0
    for(goldSpan <- spans) {
      if (goldSpan.label.value == spanLabel) {
        val num = NerSpan.numOfSameTokens(goldSpan,span)
        if (num > maxNumOfSameTokens) {
          maxNumOfSameTokens = num
          maxGoldLength = goldSpan.length.toDouble
        }
      }
    }
    (maxNumOfSameTokens,maxGoldLength)
  }
  def maxNumOfSameSucceedingTokens(span:NerSpan#Value, spanLabel:NerLabel#Value, spans:Seq[GoldNerSpan]) : (Double,Double) = {
    var maxNumOfSameTokens = 0.0
    var maxGoldLength = 0.0
    for(goldSpan <- spans) {
      if (goldSpan.label.value == spanLabel) {
        val num = NerSpan.numOfSameSucceedingTokens(goldSpan.toSeq,span)
        if (num > maxNumOfSameTokens) {
          maxNumOfSameTokens = num
          maxGoldLength = goldSpan.length.toDouble
        }
      }
    }
    (maxNumOfSameTokens,maxGoldLength)
  }
  def maxNumOfSameSucceedingTokens(span:Seq[Token], spanLabelString:String, spans:Seq[GoldNerSpan]) : (Double,Double) = {
    var maxNumOfSameTokens = 0.0
    var maxGoldLength = 0.0
    for(goldSpan <- spans) {
      if (goldSpan.label.categoryValue == spanLabelString) {
        val num = NerSpan.numOfSameSucceedingTokens(goldSpan.toSeq,span)
        if (num > maxNumOfSameTokens) {
          maxNumOfSameTokens = num
          maxGoldLength = goldSpan.length.toDouble
        }
      }
    }
    (maxNumOfSameTokens,maxGoldLength)
  }
  def maxNumOfSameTokens(span:NerSpan, spanLabel:NerLabel#Value, spans:Seq[GoldNerSpan]) : (Double,Double,GoldNerSpan) = {
    var maxNumOfSameTokens = 0.0
    var maxGoldLength = 0.0
    var toReturnBestGoldSpan:GoldNerSpan = null
    for(goldSpan <- spans) {
      val num = NerSpan.numOfSameTokens(goldSpan,span)
      if (goldSpan.label.value == spanLabel && num > maxNumOfSameTokens) {
        maxNumOfSameTokens = num
        maxGoldLength = goldSpan.length.toDouble
        toReturnBestGoldSpan = goldSpan
      }
    }
    (maxNumOfSameTokens,maxGoldLength,toReturnBestGoldSpan)
  }
  // This and this are nearly copies, not good style
  def maxNumOfSameTokens(span:NerSpan, spanLabel:NerLabel#Value, spanPolarity:Polarity#Value, spans:Seq[GoldNerSpan]) : (Double,Double,GoldNerSpan) = {
    // TODO CHECK THIS xxx
    var maxNumOfSameTokens = 0.0
    var maxGoldLength = 0.0
    var toReturnBestGoldSpan:GoldNerSpan = null
    for(goldSpan <- spans) {
      val num = NerSpan.numOfSameTokens(goldSpan,span)
//      System.err.println(num+"---"+(goldSpan.polarity.value == spanPolarity)+goldSpan.polarity.value +"---"+ spanPolarity)
      if (goldSpan.polarity.value == spanPolarity && goldSpan.label.value == spanLabel && num > maxNumOfSameTokens) {
        maxNumOfSameTokens = num
        maxGoldLength = goldSpan.length.toDouble
        toReturnBestGoldSpan = goldSpan
      }
    }
    (maxNumOfSameTokens,maxGoldLength,toReturnBestGoldSpan)
  }
}

object GoldNerSpan {
  def createNewGoldNerSpan(doc:Document, labelString:String, start:Int, length:Int, entityId: String, polarityString:String = "Unknown") : GoldNerSpan = {
    val tmp = new GoldNerSpan(doc,labelString,start,length,entityId,polarityString)(null)
    val string = doc.tokens.map(_.string).mkString("_")
    tmp
  }
}

// to store the gold spans
class GoldNerSpan(doc:Document, val labelString:String, start:Int, length:Int, val entityId: String, val polarityString:String = "Unknown")(implicit d:DiffList) extends TokenSpan(doc, start, length) with TokenSpanMention {
  val label = new TSGoldSpanNerLabel(this, labelString)
  val polarity = new GoldPolarity(this, polarityString)
  val relations = new ArrayBuffer[GoldTSRelation]
  override def name = label.categoryValue
  override def toString = "GoldNerSpan("+head.sentencePosition+","+length+","+labelString+":"+this.phrase+")"
  def ===(other: GoldNerSpan) : Boolean = {this.head.position == other.head.position && this.last.position == other.last.position && this.label.value == other.label.value}
  def ===(other: NerSpan) : Boolean = {this.head.position == other.head.position && this.last.position == other.last.position && this.label.value == other.label.value}
  def ===(other: Seq[Token]) : Boolean = {this.head.position == other.head.position && this.last.position == other.last.position}
  def =~=(other: Seq[Token]) : Boolean = {this.exists(t => other.contains(t))}
  def isPositive = this.polarity.categoryValue == "Positive"
  def isNegative = this.polarity.categoryValue == "Negative"
  def isSubjective = this.label.categoryValue == "subjective"
  def sgmlString: String = {
    val buf = new StringBuffer
    var openTagQueue = new ArrayBuffer[String]
    for (token <- this.prevWindow(4) ++ this.toSeq ++ this.nextWindow(4)) {
      //if (token.isSentenceStart) buf.append("<sentence>")
      token.startsSpansOfClass[GoldNerSpan].foreach(span => {buf.append("<"+span.name+">") ; openTagQueue += span.name})
      buf.append(token.string)
      token.endsSpansOfClass[GoldNerSpan].foreach(span => {buf.append("</"+span.name+">") ; openTagQueue -= span.name})
      //if (token.isSentenceEnd) buf.append("</sentence>")
      buf.append(" ")
    }
    openTagQueue.foreach(x => buf.append("</"+x+">"))
    buf.toString
  }
  def sgmlArea : (Int,Int) = {
    var start = if (this.prevWindow(4).nonEmpty) this.prevWindow(4).head.position else this.head.position
    var end = if (this.nextWindow(4).nonEmpty) this.nextWindow(4).last.position else this.last.position
    (start,end+1)
  }
}

object SubjectiveClassificationFeaturesDomain extends CategoricalVectorDomain[String]{dimensionDomain.maxSize=1000000} // was CategoricalDimensionTensorDomain
class SubjectiveClassificationFeatures(val sentenceValue: Sentence#Value) extends BinaryFeatureVectorVariable[String] {
  def domain = SubjectiveClassificationFeaturesDomain
  override def skipNonCategories = true
  // n-gram
  // maybe kick that out:
  List(1,2).foreach(n =>
    sentenceValue.sliding(n).foreach(x =>
      this += (n+"GRAM="+x.map(y => cc.factorie.app.strings.simplifyDigits(y.docSubstring)).mkString("_") )
    )
  )
  // numbers of pos tags
  val numOfPOSTags = new mutable.HashMap[String,Int]
  for(token <- sentenceValue) {
    val tag = token.posTag
    if (!numOfPOSTags.keySet.contains(tag)) numOfPOSTags += tag -> 0
    numOfPOSTags += tag -> (numOfPOSTags(tag)+1)
  }
  for ((tag,num) <- numOfPOSTags) {
    if (num > 2) this += tag+">=3"
    if (num > 3) this += tag+">=4"
    if (num > 4) this += tag+">=5"
  }

  if (TSProp.jointClassificationSpanPrediction) {
    if (sentenceValue.exists(t => t.spansOfClass[NerSpan].exists(s => s.label.categoryValue == "subjective")))
      this += "SUBJECTIVE-SPAN-EXISTS"
    //    if (sentenceValue.exists(t => t.spansOfClass[NerSpan].exists(s => s.label.categoryValue == "target")))
    //      this += "TARGET-SPAN-EXISTS"
  }
}


object PolarityClassificationFeaturesDomain extends CategoricalVectorDomain[String]{dimensionDomain.maxSize=1000000}// was CategoricalDimensionTensorDomain
/**
 * Features used to determine the polarity of a subjective span. *
 * @param span
 * @param polarityValue
 */
class PolarityClassificationFeatures(val span: NerSpan#Value, val polarityValue: Polarity#Value) extends BinaryFeatureVectorVariable[String] {
//  val relevantSpanArea = span.head.sentence.toSeq // of course not the whole sentence should be used...
  val relevantSpanArea = span.head.prevWindow(2) ++ span ++ span.last.nextWindow(2)
  val leftSpanArea = span.head.prevWindow(4)++span
  def domain = SubjectiveClassificationFeaturesDomain
  override def skipNonCategories = true
  val isNegated = leftSpanArea.exists((t:Token) => { NegationChecker.tokenIsNegation(t)})
  // do the negation from parser, only if parsing information is available
  val sdnegated =
    if (span.head.sentence.fw != null) {
      val graph = span.head.sentence.fw.getGraph
      (span.exists(token => graph.containsVertex(token.node) && graph.edgesOf(token.node).exists(edge => (edge._1.getShortName == "neg" || edge._1.getShortName == "conj negcc"))))
  } else
      false

//  if (isNegated) println("Negated "+relevantSpanArea)
  // n-gram
  List(1,2).foreach(n =>
    relevantSpanArea.sliding(n).foreach(x =>
      this += (n+"GRAM"+"="+x.map(y => cc.factorie.app.strings.simplifyDigits(y.docSubstring)).mkString("_") )
    )
  )
  List(1,2).foreach(n =>
    span.sliding(n).foreach(x =>
      this += (n+"SPAN"+"="+x.map(y => cc.factorie.app.strings.simplifyDigits(y.docSubstring)).mkString("_") )+(if (isNegated || sdnegated) "_NEG" else "")
    )
  )

  // this did not work well:
  // compare to previous mention (discourse in simple!)
//  val previousSubjectiveSpan = span.head.spansOfClass[NerSpan].head.previousNERSpanOfLabel("subjective")
//  if (previousSubjectiveSpan != null) {
//    val inBetween = span.head.sentence.tokens.filter(t => t.sentencePosition > previousSubjectiveSpan.last.sentencePosition && t.sentencePosition < span.head.sentencePosition)
//    for(token <- inBetween)
//    this += "PREVIOUSPOLARITY="+previousSubjectiveSpan.polarity.categoryValue+"_"+token.string
//  }

  // all subjective phrases in this sentence have the same polarity and it is...
//  if (span.head.sentence.subjectiveSpans.forall(s => s.isPositive)) this += "ALLPOSITIVE"
//  if (span.head.sentence.subjectiveSpans.forall(s => s.isNegative)) this += "ALLNEGATIVE"

  // aspect phrase if related
  val associatedRelation = span.head.sentence.relations.find(r => r.subjectiveNerSpan == span)
  if (associatedRelation.isDefined) {
    this += "TARGET="+associatedRelation.get.targetNerSpan.phrase
  }

  // pos tag
  for(t <- span) this += "POS="+t.posTag
  // pos tag with word
  for(t <- span) this += "POS="+t.posTag+"-WORD="+cc.factorie.app.strings.simplifyDigits(t.docSubstring)

  // other polarities in the same sentence
  for(otherSubjSpan <- span.head.sentence.subjectiveSpans ; if (!(otherSubjSpan === span))) {
    this += "OTHERPOLARITY="+otherSubjSpan.polarity.categoryValue
  }

  // with path to other polarities
  if (TSProp.stanfordParser) { // needs to be adapted when another parser is used
    for(otherSubjSpan <- span.head.sentence.subjectiveSpans ; if (!(otherSubjSpan === span))) {
      ParsingFeatures.parsingFeatures(otherSubjSpan,span,"S-S=").foreach(this += otherSubjSpan.polarity.categoryValue+"-"+_)
    }
  }

  // dictionary Wilson 2005
  for (t <- span) this += "WILSON="+TargSubjSpanNER.modelContainer.wilson.polarity(t)+(if (isNegated || sdnegated) "_NEG" else "")
  // dictionary Sentiwordnet 3
//  for (t <- span) this += "SWN="+SentiWordNetDictionaryPriorPolarityChecker.polarity(t)+(if (isNegated || sdnegated) "_NEG" else "")


  // DEBUG Gold Feature
//  val goldNerSpansOnDocument = span.head.document.spansOfClass[GoldNerSpan].filter((s:GoldNerSpan) => s.label.categoryValue == "subjective")
//  if (goldNerSpansOnDocument.exists((s:GoldNerSpan) => s =~= span && s.polarity.value == polarityValue) )
//    this += "GOLD"
//  println(span.map(_.string).mkString("_")+"\t"+this.activeCategories.mkString(";"))
}


object RelationFeaturesDomain extends CategoricalVectorDomain[String]{dimensionDomain.maxSize=1000000/*;dimensionDomain.gatherCounts = true*/} // was CategoricalDimensionTensorDomain before updating to M7
object TSSpanFeaturesDomain extends CategoricalVectorDomain[String]{dimensionDomain.maxSize=1000000/*;dimensionDomain.gatherCounts = true*/} // was CategoricalDimensionTensorDomain
object TSSpanFeaturesJointDomain extends CategoricalVectorDomain[String]{dimensionDomain.maxSize=1000000} // was CategoricalDimensionTensorDomain
object TSSpanFeatures{
  def tokenFeatures(t : Token,label:String) : ListBuffer[String] = {
    var r = new ListBuffer[String]
    //r += "SIMPLY="+cc.factorie.app.strings.simplifyDigits(t.docSubstring).toLowerCase()
    r += "POS="+t.posTag
    //if (TSProp.parsingOneEdge && DistanceFeatures.hasOneEdgerToDifferentLabelSpan(t,label)) r += "POS="+t.posTag + "_" + "ONE-EDGE"
    r += "SHAPE="+cc.factorie.app.strings.stringShape(t.docSubstring, 3)
    if (TSProp.suffix3) r += "SUFFIX3="+t.docSubstring.takeRight(3).toLowerCase
    if (TSProp.prefix3) r += "PREFIX3="+t.docSubstring.take(3).toLowerCase
    if (t.isCapitalized) r += "CAPITALIZED"
    if (t.containsDigit) r += "NUMERIC"
    if (t.isPunctuation) r += "ISPUNC"
    if (TSProp.iobwordfeature) r+= t.docSubstring.toLowerCase
    if (TSProp.iobwordfeature) r+= t.docSubstring.toLowerCase + "_" + t.posTag
    r
  }
}

object DistanceFeatures{
  lazy val importantEdges = Set("amod","nmod","dobj","advmod","nn","rcmod","cop")

  def distanceFeatures(pos1:Int,pos2:Int,prefix:String) : ListBuffer[String] = {
    var r = new ListBuffer[String]
    if (scala.math.abs(pos2-pos1) > 0) r += prefix+"1"
    if (scala.math.abs(pos2-pos1) > 1) r += prefix+"2"
    if (scala.math.abs(pos2-pos1) > 2) r += prefix+"3"
    if (scala.math.abs(pos2-pos1) > 3) r += prefix+"4"
    if (scala.math.abs(pos2-pos1) > 4) r += prefix+"5"
    if (scala.math.abs(pos2-pos1) > 5) r += prefix+"6"
    if (scala.math.abs(pos2-pos1) > 6) r += prefix+"7"
    if (scala.math.abs(pos2-pos1) > 7) r += prefix+"8"
    r
  }
  def hasOneEdgerToDifferentLabelSpan(t:Token,currentLabel:String) : Boolean = {
    val graph = t.sentence.fw.getGraph
    val valu = t.sentence.tokens.exists(someTokenInSentence => someTokenInSentence.startsSpansOfClass[NerSpan].exists(someSpanOnSentence => {
      someSpanOnSentence.label.categoryValue != currentLabel &&
      someSpanOnSentence.exists(someTokenInOtherSpanOnSentence => {
        graph.containsEdge(someTokenInOtherSpanOnSentence.node,t.node) || graph.containsEdge(t.node,someTokenInOtherSpanOnSentence
          .node)
      })
    }))
    valu
  }
  def edgeToDifferentLabelSpan(span:Seq[Token],currentLabel:String) : String = {
    val graph = span.head.sentence.fw.getGraph
    var edge = null.asInstanceOf[(GrammaticalRelation,Boolean,Int)]
    for (t <- span ; someTokenInSentence <- t.sentence.tokens ; someSpanOnSentence <- someTokenInSentence.startsSpansOfClass[NerSpan]) {
      if (someSpanOnSentence.label.categoryValue != currentLabel) {
        for(someTokenInOtherSpanOnSentence <- someSpanOnSentence) {
          if (graph.containsEdge(someTokenInOtherSpanOnSentence.node,t.node))
            edge = graph.getEdge(someTokenInOtherSpanOnSentence.node,t.node)
          if (graph.containsEdge(t.node,someTokenInOtherSpanOnSentence.node))
            edge = graph.getEdge(t.node,someTokenInOtherSpanOnSentence.node)
        }
      }
    }
    if (edge != null) ParsingFeatures.edgeToString(edge) else null.asInstanceOf[String]
  }
}

object ParsingFeatures{
  def edgeToString(edge:(GrammaticalRelation,Boolean,Int)) : String = {edge._1.toString+(if (edge._2) "↑" else "↓" )}
  def parsingFeatures(span1:NerSpan,span2:Seq[Token],prefix:String) : ListBuffer[String] = parsingFeatures(span1.toSeq,span2,prefix)
  def parsingFeatures(span1:Seq[Token],span2:Seq[Token],prefix:String) : ListBuffer[String] = {
    var r = new ListBuffer[String]
    // do this only with head would be much better, but first try
    if (span1.head.sentence != span2.head.sentence)
      r // empty
    else {
      val fw = span1.head.sentence.fw
      val graph = fw.getGraph
//      println(span1.sentence)
//      println(span1.value.mkString(" ")+ " --> " + span2.mkString(" "))
      for(t1 <- span1; t2 <- span2 ; if (graph.containsVertex(t1.node) && graph.containsVertex(t2.node))) {
        val path = fw.getShortestPath(t1.node,t2.node)
        if (path != null) {
          val edges = path.getEdgeList.toArray.map(o => o.asInstanceOf[(GrammaticalRelation,Boolean,Int)])
          val edgesStringArray = edges.map(o => edgeToString(o))
          val pathString = edgesStringArray.mkString("->")
          r += prefix+"-"+pathString
//          println(pathString)
          // ngrams on path
          //edgesStringArray.sliding(2).foreach(x=> r += prefix+"-sliding"+x )
          // pos-edge-walk, could be done
        }
      }
      r
    }
  }
}

// do not use the second parameter for something else than debugging
//class TSSpanFeatures(val nerSpanValue: NerSpan#Value, val labelValue: TSSpanNerLabel#Value) extends BinaryFeatureVectorVariable[String] {
class TSSpanFeatures(val nerSpanValue: Seq[Token], val labelString: String, ignoreSimpleTokenBasedFeatureOnlyWithJoint:Boolean = false, useRelationForSpanDetection:Boolean = false) extends BinaryFeatureVectorVariable[String] {
  def domain = TSSpanFeaturesDomain
  // Tim says: This is important for unseen data:
  override def skipNonCategories = true

  val preprocessedTokens = nerSpanValue.filter(t => Stopwords.contains(t.docSubstring)) // FIXME this might be a bug, only keep stopwords?! 26.11.2013
  val rawString = (for(t <- preprocessedTokens) yield t.docSubstring).mkString("_")
  val rawString2 = (for(t <- preprocessedTokens) yield t.docSubstring).mkString(" ")
  val firstToken = nerSpanValue.head
  val lastToken = nerSpanValue.last
  val intermediateTokens = for(t <- preprocessedTokens ; if (t != firstToken)) yield t
  val fullword = cc.factorie.app.strings.simplifyDigits(rawString)
  val goldNerSpansOnDocument = nerSpanValue.head.document.spansOfClass[GoldNerSpan]
  val correctSpanStartAndEnd = goldNerSpansOnDocument.exists(sampledSpan => {
    sampledSpan.head == nerSpanValue.head && sampledSpan.last == nerSpanValue.last  && sampledSpan.label.categoryValue == labelString})
  val correctSpanStart = goldNerSpansOnDocument.exists(sampledSpan => sampledSpan.head == nerSpanValue.head && sampledSpan.label.categoryValue == labelString)
  val correctSpanEnd = goldNerSpansOnDocument.exists(sampledSpan => sampledSpan.last == nerSpanValue.last && sampledSpan.label.categoryValue == labelString)

  // fake features // leads to very good results on train, and near perfect on test, target is still difficult
//  if (labelValue.category == "subjective") {
//    if (correctSpanStartAndEnd) this += "CORRECTSPAN"+labelValue
//    if (correctSpanStart) this += "CORRECTSPANSTART"+labelValue
//    if (correctSpanEnd) this += "CORRECTSPANEND"+labelValue
//  }
  //  this += "W="+fullword.toLowerCase+correctSpanStartAndEnd

  // full word
  //this += "W="+fullword.toLowerCase
  //this += "RAW="+rawString
  // IOB
  if (!TSProp.simpleTokenBasedFeatureOnlyWithJoint || ignoreSimpleTokenBasedFeatureOnlyWithJoint) {
    TSSpanFeatures.tokenFeatures(firstToken,labelString).foreach(x => this += "B-"+x)
    TSSpanFeatures.tokenFeatures(lastToken,labelString).foreach(x => this += "E-"+x)
    for(t <- nerSpanValue ; featureString <- TSSpanFeatures.tokenFeatures(t,labelString)) this += "I-"+featureString


    // Dictionaries, note that only adjectives (respectively nouns are checked), latter did not work well? (&& (x.posTag == "N" || x.posTag == "^"))
    if (TSProp.dictionaries) if (nerSpanValue.exists(x => TargSubjSpanNER.modelContainer.bootstrappedLexicons("subjective").contains(x.docSubstring) && x.posTag == "A")) { this += "SUBJ-DIC" }
    if (TSProp.dictionaries) if (nerSpanValue.exists(x => TargSubjSpanNER.modelContainer.bootstrappedLexicons("target").contains(x.docSubstring) && (x.posTag == "N" || x.posTag == "^"))) { this += "TARG-DIC" }

    // external dictionary
    if (TSProp.dictionaries) if (nerSpanValue.exists(x => TargSubjSpanNER.modelContainer.externalLexicons("wiebe-subjectivity").contains(x.docSubstring))) { this += "JAN-WIEBE-SUBJ-DIC" }

    // full sequence of POS
    // NORMALLY IN
    this += "POS-SEQ="+((for(t <- nerSpanValue) yield t.posTag).mkString("-"))
  }
  if (firstToken.sentence.subjective.categoryValue) {this += "SUBJECTIVE-SENTENCE"}

  // features of token left and right of this span
  // NORMALLY 3
  val offset = TSProp.offsetConjunction
  for(o <- 1 to offset) {
    if (firstToken.prev(o) != null) {
      TSSpanFeatures.tokenFeatures(firstToken.prev(o),labelString).foreach(x => this += "@+"+o+x)
      TSSpanFeatures.tokenFeatures(firstToken.prev(o),labelString).foreach(x => {this += "@-"+o+x ; this += "@-"+o+x+"L-"+(if (firstToken.prev(o).spansOfClass[NerSpan].nonEmpty) firstToken.prev(o).spansOfClass[NerSpan].head.label.categoryValue else "O")})
      //this += "@-"+o+"L-"+(if (firstToken.prev(o).spansOfClass[NerSpan].nonEmpty) firstToken.prev(o).spansOfClass[NerSpan].head.label.categoryValue else "O")
    }
    if (lastToken.next(o) != null) {
      TSSpanFeatures.tokenFeatures(firstToken.next(o),labelString).foreach(x => this += "@+"+o+x)
      TSSpanFeatures.tokenFeatures(lastToken.next(o),labelString).foreach(x => {this += "@-"+o+x ; this += "@-"+o+x+"L-"+(if (lastToken.next(o).spansOfClass[NerSpan].nonEmpty) lastToken.next(o).spansOfClass[NerSpan].head.label.categoryValue else "O")})
    }
  }
  // WINDOW
  if (TSProp.prevwindow) firstToken.prevWindow(3).map(t2 => "PREVWINDOW="+t2.docSubstring.toLowerCase).foreach(x => this += x)

  // CHECK THIS AGAIN!!!
  //xxx
  if (useRelationForSpanDetection) { // new after ACL2013, CANT WORK!
    val relationWithThisSpanOption = nerSpanValue.head.sentence.relations.find(ts => (nerSpanValue.exists(tokenInThisSpan => (ts.subjectiveNerSpan.toSeq.contains(tokenInThisSpan) || ts.subjectiveNerSpan.toSeq.contains(tokenInThisSpan)))))
    if (relationWithThisSpanOption.isDefined) {
//      println("Found as part of span: "+nerSpanValue+" "+relationWithThisSpanOption.get)
      this += "SPAN-IS-PART-OF-A-PREDICTED-RELATION" // xxx
    }
  }
}

// TODO add: number of targets and subjectives in tweet
//class TSSpanFeaturesJoint(val nerSpanValue: NerSpan#Value, val labelValue: TSSpanNerLabel#Value) extends BinaryFeatureVectorVariable[String] {
class TSSpanFeaturesJoint(val nerSpanValue: Seq[Token], val labelString: String) extends BinaryFeatureVectorVariable[String] {
  def domain = TSSpanFeaturesJointDomain
  override def skipNonCategories = true
  val firstToken = nerSpanValue.head
  val lastToken = nerSpanValue.last
  def ji(given:String) = if (TSProp.mergeJointIdentifierToOne) "JOINT-" else given
  if (TSProp.jointTargetSubjectivity) {
    val simpleFeatures = new TSSpanFeatures(nerSpanValue,labelString,true)
    // is there a span directly left of this span or directly right of this span? -- simple JOINT!
    if (TSProp.directjoint) { // direct-joint
      if (firstToken.sentencePosition > 0) {
        val nerSpans = firstToken.sentence(firstToken.sentencePosition-1).spansOfClass[NerSpan]
        if (nerSpans.nonEmpty) nerSpans.foreach(x => if (x.label.value == "target") this += "TARGET-SPAN-LEFT")
        if (nerSpans.nonEmpty) nerSpans.foreach(x => if (x.label.value == "subjective") this += "SUBJ-SPAN-LEFT")
      }
      if (lastToken.sentencePosition+2 < lastToken.sentence.length) {
        val nerSpans = lastToken.sentence(lastToken.sentencePosition+1).spansOfClass[NerSpan]
        if (nerSpans.nonEmpty) nerSpans.foreach(x => if (x.label.categoryValue == "target") this += "TARGET-SPAN-RIGHT")
        if (nerSpans.nonEmpty) nerSpans.foreach(x => if (x.label.categoryValue == "subjective") this += "SUBJ-SPAN-RIGHT")
      }
    }
    // are there both a target and a subjective on this sentence?
    if (TSProp.booleanjoint) { // boolean joint
      if (firstToken.sentence.exists(t => t.spansOfClass[NerSpan].exists(s => s.label.categoryValue != labelString))) {
        //this += "BOTH-THERE"
        simpleFeatures.activeCategories.foreach(x => this += ji("BOTH-THERE-")+x)
      } else {
        this += "NOT-BOTH-THERE"
//        simpleFeatures.activeCategories.foreach(x => this += ji("NOT-BOTH-THERE-")+x) // exp
      }
    }
    // text between...
    if (TSProp.phrasebetweenjoint) {
    val previousNERSpan = firstToken.startsSpansOfClass[NerSpan].head.previousNERSpan
      if (previousNERSpan != null && scala.math.abs(firstToken.sentencePosition-previousNERSpan.last.sentencePosition) < 5)
        this += previousNERSpan.label.categoryValue+"-"+
          firstToken.sentence.tokens.filter(t => t.sentencePosition > previousNERSpan.last.sentencePosition && t.sentencePosition < firstToken.sentencePosition).map(_.docSubstring).mkString("_") +
          "-"+labelString
      val nextNERSpan = firstToken.startsSpansOfClass[NerSpan].head.nextNERSpan
      if (nextNERSpan != null && scala.math.abs(nextNERSpan.head.sentencePosition-lastToken.sentencePosition) < 5)
        this += nextNERSpan.label.categoryValue+"-"+
          firstToken.sentence.tokens.filter(t => t.sentencePosition < nextNERSpan.head.sentencePosition && t.sentencePosition > lastToken.sentencePosition).map(_.docSubstring).mkString("_") +
          "-"+labelString
    }
    // words between
    if (TSProp.wordsbetweenjoint) {
      val previousNERSpan = firstToken.startsSpansOfClass[NerSpan].head.previousNERSpan
      if (previousNERSpan != null && scala.math.abs(firstToken.sentencePosition-previousNERSpan.last.sentencePosition) < 5)
        firstToken.sentence.tokens.filter(t =>
          t.sentencePosition > previousNERSpan.last.sentencePosition && t.sentencePosition < firstToken.sentencePosition).map(_.docSubstring).foreach(x=>{
          this += previousNERSpan.label.categoryValue+"-"+x+"-"+labelString
        })
      val nextNERSpan = firstToken.startsSpansOfClass[NerSpan].head.nextNERSpan
      if (nextNERSpan != null && scala.math.abs(nextNERSpan.head.sentencePosition-lastToken.sentencePosition) < 5)
        firstToken.sentence.tokens.filter(t =>
          t.sentencePosition < nextNERSpan.head.sentencePosition && t.sentencePosition > lastToken.sentencePosition).map(_.docSubstring).foreach(x=>{
          this += nextNERSpan.label.categoryValue+"-"+"-"+x+"-"+labelString
        })
    }
    // number of tokens left of it, distance, cumulative binning
    if (TSProp.distancejoint) { // distance joint
      // different label
      if (labelString == "target") {
        // previous
        val previousNERSpanOfLabelSubjective = firstToken.startsSpansOfClass[NerSpan].head.previousNERSpanOfLabel("subjective")
        if (previousNERSpanOfLabelSubjective != null) {
          DistanceFeatures.distanceFeatures(previousNERSpanOfLabelSubjective.last.sentencePosition,nerSpanValue.head.sentencePosition,"S-T<-").foreach(this += _)
        }
        // next

        val nextNERSpanOfLabelSubjective = firstToken.startsSpansOfClass[NerSpan].head.nextNERSpanOfLabel("subjective")
        if (nextNERSpanOfLabelSubjective != null) {
          DistanceFeatures.distanceFeatures(nextNERSpanOfLabelSubjective.head.sentencePosition,nerSpanValue.last.sentencePosition,"S-T->").foreach(this += _)
        }
      }
      if (labelString == "subjective") {
        // previous
        val previousNERSpanOfLabelTarget = firstToken.startsSpansOfClass[NerSpan].head.previousNERSpanOfLabel("target")
        if (previousNERSpanOfLabelTarget != null) {
          DistanceFeatures.distanceFeatures(previousNERSpanOfLabelTarget.last.sentencePosition,nerSpanValue.head.sentencePosition,"T-S<-").foreach(this += _)
        }
        // next
        val nextNERSpanOfLabelTarget = firstToken.startsSpansOfClass[NerSpan].head.nextNERSpanOfLabel("target")
        if (nextNERSpanOfLabelTarget != null) {
          DistanceFeatures.distanceFeatures(nextNERSpanOfLabelTarget.head.sentencePosition,nerSpanValue.last.sentencePosition,"T-S->").foreach(this += _)
        }
      }
      // same label
      if (labelString == "target") {
        // previous
        val previousNERSpanOfLabelTarget = firstToken.startsSpansOfClass[NerSpan].head.previousNERSpanOfLabel("target")
        if (previousNERSpanOfLabelTarget != null) {
          DistanceFeatures.distanceFeatures(previousNERSpanOfLabelTarget.last.sentencePosition,nerSpanValue.head.sentencePosition,"T-T<-").foreach(this += _)
        }
        // next
        val nextNERSpanOfLabelTarget = firstToken.startsSpansOfClass[NerSpan].head.nextNERSpanOfLabel("target")
        if (nextNERSpanOfLabelTarget != null) {
          DistanceFeatures.distanceFeatures(nextNERSpanOfLabelTarget.head.sentencePosition,nerSpanValue.last.sentencePosition,"T-T->").foreach(this += _)
        }
      }
      if (labelString == "subjective") {
        // previous
        val previousNERSpanOfLabelSubjective = firstToken.startsSpansOfClass[NerSpan].head.previousNERSpanOfLabel("subjective")
        if (previousNERSpanOfLabelSubjective != null) {
          DistanceFeatures.distanceFeatures(previousNERSpanOfLabelSubjective.last.sentencePosition,nerSpanValue.head.sentencePosition,"S-S<-").foreach(this += _)
        }
        // next
        val nextNERSpanOfLabelSubjective = firstToken.startsSpansOfClass[NerSpan].head.nextNERSpanOfLabel("subjective")
        if (nextNERSpanOfLabelSubjective != null) {
          DistanceFeatures.distanceFeatures(nextNERSpanOfLabelSubjective.head.sentencePosition,nerSpanValue.last.sentencePosition,"S-S->").foreach(this += _)
        }
      }
    }
    if (TSProp.parsingShortestEdgePath) {
      // different label, maybe do it for same label as well!?
      if (labelString == "target") {
        // previous
        val previousNERSpanOfLabelSubjective = firstToken.startsSpansOfClass[NerSpan].head.previousNERSpanOfLabel("subjective")
        if (previousNERSpanOfLabelSubjective != null) {
          ParsingFeatures.parsingFeatures(previousNERSpanOfLabelSubjective,nerSpanValue,"S-T-SHORTESTPARSE=").foreach(this += _)
        }
        // next
        val nextNERSpanOfLabelSubjective = firstToken.startsSpansOfClass[NerSpan].head.nextNERSpanOfLabel("subjective")
        if (nextNERSpanOfLabelSubjective != null) {
          ParsingFeatures.parsingFeatures(nextNERSpanOfLabelSubjective,nerSpanValue,"T-S-SHORTESTPARSE=").foreach(this += _)
        }
      }
      if (labelString == "subjective") {
        // previous
        // debug
        if (firstToken.startsSpansOfClass[NerSpan].isEmpty) {
//          println("EMPTY: "+firstToken+" in "+nerSpanValue)
//          firstToken.startsSpansOfClass[NerSpan].head
        }
        // TODO the following if is a hack!
        val previousNERSpanOfLabelTarget = /*if (firstToken.startsSpansOfClass[NerSpan].isEmpty) null else*/ firstToken.startsSpansOfClass[NerSpan].head.previousNERSpanOfLabel("target")
        if (previousNERSpanOfLabelTarget != null) {
          ParsingFeatures.parsingFeatures(previousNERSpanOfLabelTarget,nerSpanValue,"T-S-SHORTESTPARSE=").foreach(this += _)
        }
        // next
        // TODO the following if is a hack!
        val nextNERSpanOfLabelTarget = /*if (firstToken.startsSpansOfClass[NerSpan].isEmpty) null else*/ firstToken.startsSpansOfClass[NerSpan].head.nextNERSpanOfLabel("target")
        if (nextNERSpanOfLabelTarget != null) {
          ParsingFeatures.parsingFeatures(nextNERSpanOfLabelTarget,nerSpanValue,"S-T-SHORTESTPARSE").foreach(this += _)
        }
      }
    }
    if (TSProp.parsingOneEdge) {
      if (nerSpanValue.exists(t => DistanceFeatures.hasOneEdgerToDifferentLabelSpan(t,labelString))) {
//        this += "ONE-EDGE"
        if (TSProp.onlyImportantEdges) {
          val edge = DistanceFeatures.edgeToDifferentLabelSpan(nerSpanValue,labelString)
          if (DistanceFeatures.importantEdges.exists(e => edge.contains(e))) {
            simpleFeatures.activeCategories.foreach(x => this += ji("ONE-EDGE-"+edge+"-")+x)
          }
        } else
          simpleFeatures.activeCategories.foreach(x => this += ji("ONE-EDGE-")+x)
      } else {
        this += "NO-SINGLE-EDGE"
//        simpleFeatures.activeCategories.foreach(x => this += ji("NO-SINGLE-EDGE-")+x) // exp
      }
    }
    if (TSProp.identifyNounCloseToSubjective) {
      // get all nouns on this sentence
      val relevantPOSTags = Set("NN","NNS","NNP","NNPS")
      var featureSet = false
      if (nerSpanValue.exists(x => relevantPOSTags.contains(x.posTag))) {
        val sentence = nerSpanValue.head.sentence
        val subjectives = sentence.toSeq.filter(t => t.spansOfClass[NerSpan].exists(_.label.categoryValue=="subjective"))
        if (subjectives.nonEmpty) {
          val nouns = sentence.toSeq.filter(t => relevantPOSTags.contains(t.posTag))
          if (nouns.nonEmpty) {
            // for each subjective, we search for the closest nouns, these are the relevant ones
            val closestNouns = for(subj <- subjectives) yield nouns.minBy(noun => scala.math.abs(noun.sentencePosition-subj.sentencePosition))
            if (closestNouns.exists(cn => nerSpanValue.contains(cn))) {
//              this += "HAS-CLOSE-NOUN-TO-SUBJECTIVE"
              simpleFeatures.activeCategories.foreach(x => this += ji("CLOSE-NOUN-")+x)
              featureSet = true
            }
//          println("=====================================")
          }
        }
      }
      if (!featureSet) {
        this += "HAS-NO-CLOSE-NOUN-TO-SUBJECTIVE"
//        simpleFeatures.activeCategories.foreach(x => this += ji("HAS-NO-CLOSE-NOUN-TO-SUBJECTIVE-")+x) // exp
      }
    }
  }
  }

// new after ACL2013
// accessed in deep joint as well as in pipeline
  class RelationFeatures(subjSpan: Seq[Token], targSpan:Seq[Token]) extends BinaryFeatureVectorVariable[String] {
    def domain = RelationFeaturesDomain
    override def skipNonCategories = true
 val strPair = subjSpan.map(_.docSubstring).mkString("-")+"------"+targSpan.map(_.docSubstring).mkString("-")
    // DEBUGGING GOLD
//    if (subjSpan.head.document.goldTsRelations.exists(gr => gr.subjectiveNerSpan === subjSpan && gr.targetNerSpan === targSpan )) {
//      this += "CORRECT-RELATION" // xxx
//    } else
//      this += "NON-CORRECT-RELATION"
//    //


    val targetsimpleFeatures = new TSSpanFeatures(targSpan,"tar",true) // TODO think: should I use them?
    val subjectivesimpleFeatures = new TSSpanFeatures(subjSpan,"sub",true) // TODO think: should I use them?
    // are these two guys very close to each other?
    if (TSProp.directjoint) { // direct-joint
      if (subjSpan.head.position < targSpan.last.position && subjSpan.head.position - targSpan.last.position < 2) this += "S<T-REL"
      if (targSpan.head.position < subjSpan.last.position && targSpan.head.position - subjSpan.last.position < 2) this += "S<T-REL"
    }
    // are both in one sentence (currently meaningless, because I only sample from one sentence)
    if (TSProp.booleanjoint) { // boolean joint
      if (subjSpan.head.sentence == targSpan.head.sentence) {
        this += "BOTH-THERE" // TODO ?
        targetsimpleFeatures.activeCategories.foreach(x => this += ("BOTH-THERE-T-")+x)
        subjectivesimpleFeatures.activeCategories.foreach(x => this += ("BOTH-THERE-S-")+x)
      } else {
        this += "NOT-BOTH-THERE"
        //        simpleFeatures.activeCategories.foreach(x => this += ji("NOT-BOTH-THERE-")+x) // exp // TODO ??
      }
    }
    // words between
    if (TSProp.wordsbetweenjoint) {
      val r = min(subjSpan.last.position,targSpan.last.position)
      val l = max(subjSpan.head.position,targSpan.head.position)
      if (r-l > 1) this += "TEXT-IN-BETWEEN="+subjSpan.head.sentence.toSeq.slice(r+1,l-1).map(_.docSubstring).mkString("-")
    }
    if (TSProp.parsingShortestEdgePath) {
      ParsingFeatures.parsingFeatures(subjSpan,targSpan,"S-T-SHORTESTPARSE=").foreach(this += _)
      ParsingFeatures.parsingFeatures(targSpan,subjSpan,"T-S-SHORTESTPARSE=").foreach(this += _)
    }
    if (TSProp.parsingOneEdge) {
      if (subjSpan.head.sentence.fw == null) throw new Exception("FW is null! Did you do parsing?")
      val graph = subjSpan.head.sentence.fw.getGraph
      if (subjSpan.exists(st => targSpan.exists(tt => graph.containsEdge(st.node,tt.node) || graph.containsEdge(tt.node,st.node)))) {
        this += "ONE-EDGE" // TODO ?
//        targetsimpleFeatures.activeCategories.foreach(x => this += ("ONE-EDGE-T-")+x)
//        subjectivesimpleFeatures.activeCategories.foreach(x => this += ("ONE-EDGE-S-")+x)
      } else {
        this += "NO-SINGLE-EDGE"
        //        simpleFeatures.activeCategories.foreach(x => this += ji("NO-SINGLE-EDGE-")+x) // TODO ??
      }
    }
    if (TSProp.identifyNounCloseToSubjective) {
      // get all nouns on this sentence
      val relevantPOSTags = Set("NN","NNS","NNP","NNPS")
      var featureSet = false
      if (targSpan.exists(x => relevantPOSTags.contains(x.posTag))) { // does the target have a noun?
        val sentence = targSpan.head.sentence
        val allSubjectiveTokens = sentence.toSeq.filter(t => t.spansOfClass[NerSpan].exists(_.label.categoryValue=="subjective"))
        val subjectiveTokensNotInCurrentSubjectiveSpan = sentence.toSeq.filter(t => t.spansOfClass[NerSpan].exists(_.label.categoryValue=="subjective"))
        val nouns = sentence.toSeq.filter(t => relevantPOSTags.contains(t.posTag))
        if (nouns.nonEmpty) {
          // for subjective, we search for the closest nouns, these are the relevant ones
          val closestNouns = for(subj <- subjSpan) yield nouns.minBy(noun => scala.math.abs(noun.sentencePosition-subj.sentencePosition))
          if (closestNouns.exists(cn => targSpan.contains(cn))) {
            this += "HAS-CLOSE-NOUN-TO-SUBJECTIVE" // TODO ?
//            targetsimpleFeatures.activeCategories.foreach(x => this += ("CLOSE-NOUN-TO-SUBJ-T-")+x)
//            subjectivesimpleFeatures.activeCategories.foreach(x => this += ("CLOSE-NOUN-TO-SUBJ-S-")+x)
            featureSet = true
          }
          // check for each subjective span different from this one, if the target may be closer to such
//          if ((for(subjToken <- allSubjectiveTokens ; if (!subjSpan.contains(subjToken)))
//            yield nouns.minBy(noun => scala.math.abs(noun.sentencePosition-subjToken.sentencePosition)))
//              .exists(cn => targSpan.contains(cn)))
//            this += "TARG-CLOSER-TO-OTHER-SUBJECTIVE"
        }
      }
      if (!featureSet) {
        this += "HAS-NO-CLOSE-NOUN-TO-SUBJECTIVE"
        //        simpleFeatures.activeCategories.foreach(x => this += ji("HAS-NO-CLOSE-NOUN-TO-SUBJECTIVE-")+x) // exp // ??
      }
    }
  if (TSProp.srl) {
    // srl relations are stored in both directions, so it sufficient to check one direction
    for(subjToken <- subjSpan ; targToken <- targSpan) {
      val srlLinks = subjToken.srlLinks.find((link:SRLTokenEdge) => link.target == targToken)
      for(srlLink <- srlLinks) {
        this += subjToken.docSubstring+"-"+srlLink.label+srlLink.direction
        this += srlLink.label+srlLink.direction
        this += srlLink.label+srlLink.direction+"-"+srlLink.target.docSubstring
        // TODO feature with both strings
      }
    }
  }
//    this.activeCategories.foreach(println(_))
  }
