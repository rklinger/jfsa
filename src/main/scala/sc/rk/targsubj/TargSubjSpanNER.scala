/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * This Class is the Main Entry Point for JSFA. *
 */

package sc.rk.targsubj

import cc.factorie._
import cc.factorie.optimize._
import sc.rk.targsubj.RelationFeaturesDomain._
import sc.rk.targsubj.TSSpanFeaturesDomain._
import sc.rk.targsubj.TSSpanFeaturesJointDomain._
import sc.rk.targsubj.data._
import compat.Platform
import collection.mutable
import collection.mutable.ArrayBuffer
import scala.math.min
import tools._
import scala.util.Random

class 
TargSubjSpanNER {
  val random = new Random(1)
  val subjectivityObjective = new SubjectivityObjective
  val relationObjective = new RelationObjective
  val spanrelationobjective = new SpanRelationObjective
  val objective = new SpanNerObjective
  val polarityObjective = new PolarityObjective
  var tokenizer:Tokenizer = new SimpleTokenizer
  var srl:SemanticRoleLabeler = null
  var training = false
  var modelContainer:SerializableContainerModel = new SerializableContainerModel()
  
  /**
   * Predict spans. The predictor decides how that is happening. *
   * @param testDocuments
   * @param valDocuments
   * @param predictor
   * @param clearSubjectives
   * @param clearTargets
   */
  def predictSpans(testDocuments:Seq[Document], valDocuments:Seq[Document],predictor:SettingsSampler[Token],clearSubjectives:Boolean,clearTargets:Boolean) {
    val testTokens = if (TSProp.randomizeTokens) testDocuments.map(_.tokens).flatten.shuffle(random) else testDocuments.map(_.tokens).flatten
    val valTokens = if (TSProp.randomizeTokens) valDocuments.map(_.tokens).flatten.shuffle(random) else valDocuments.map(_.tokens).flatten
      println("Predictor processes all test and val documents...")
      val s = Platform.currentTime
      for(d <- testDocuments) d.clearNerSpans(clearSubjectives,clearTargets)
      for(d <- valDocuments) d.clearNerSpans(clearSubjectives,clearTargets)
      println(Printer.evalListHeader("             \t","|",'~'))
      //        averager.setWeightsToAverage(model.weightsTensor)
      for(j <- 1 to TSProp.predictionIterations) {
        predictor.processAll(testTokens)
        println("TEST   "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(testDocuments,"|"))
        if (valDocuments.nonEmpty) {
          predictor.processAll(valTokens)
          println("VAL    "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(valDocuments,"|"))
        }
      }
      println(Printer.line(160,'~'))
      println("Done in "+((Platform.currentTime-s)/1000)+"s")

  }

  /**
   * Predict polarities, again, the predictor sampler is specifying how. *
   * @param trainDocuments
   * @param testDocuments
   * @param valDocuments
   * @param predictor
   */
  def predictPolarity(trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document],predictor:SettingsSampler[NerSpan]) {
    if (predictor != null) {
      val testSpans = testDocuments.map(_.spansOfClass[NerSpan]).flatten.filter(s => s.label.categoryValue=="subjective")
      val valSpans = valDocuments.map(_.spansOfClass[NerSpan]).flatten.filter(s => s.label.categoryValue=="subjective")
      val trainSpans = trainDocuments.map(_.spansOfClass[NerSpan]).flatten.filter(s => s.label.categoryValue=="subjective")
      println("Polarity predictor processes all test and val spans to predict polarities only!")
      println(Printer.evalListHeader("             \t","|",'~'))
      for(j <- 1 to TSProp.predictionIterations) {
        if (trainDocuments.nonEmpty) {
          predictor.processAll(trainSpans)
          println("TRAIN  "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(trainDocuments,"|"))
        }
        predictor.processAll(testSpans)
        println("TEST   "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(testDocuments,"|"))
        if (valDocuments.nonEmpty) {
          predictor.processAll(valSpans)
          println("VAL    "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(valDocuments,"|"))
        }
      }
      println(Printer.line(160,'~'))
      println("Done.")
    }
  }

  /**
   * Predict relations. *
   * @param trainDocuments
   * @param testDocuments
   * @param valDocuments
   * @param predictor
   */
  def predictRelations(trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document],predictor:SettingsSampler[NerSpan]) {
    if (predictor != null) {
      val testSpans = testDocuments.map(_.spansOfClass[NerSpan]).flatten
      val valSpans = valDocuments.map(_.spansOfClass[NerSpan]).flatten
      val trainSpans = trainDocuments.map(_.spansOfClass[NerSpan]).flatten
      println("Predictor processes all test and val spans to predict relations only!")
      for(d <- testDocuments) d.clearRelations
      if (valDocuments.nonEmpty) for(d <- valDocuments) d.clearRelations
      println(Printer.evalListHeader("             \t","|",'~'))
      for(j <- 1 to TSProp.predictionIterations) {
        if (trainDocuments.nonEmpty) {
          predictor.processAll(trainSpans)
          println("TRAIN  "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(trainDocuments,"|"))
        }
        predictor.processAll(testSpans)
        // end of debugging
        println("TEST   "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(testDocuments,"|"))
        if (valDocuments.nonEmpty) {
          predictor.processAll(valSpans)
          println("VAL    "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(valDocuments,"|"))
        }
      }
      println(Printer.line(160,'~'))
      println("Done.")
    }
  }

  /**
   * Start the training of the relations only. This is typically only called when spans are already existent. *
   * @param interations
   * @param learner
   * @param predictor
   * @param trainDocuments
   * @param testDocuments
   * @param valDocuments
   * @param results
   * @param perfectionize
   */
  def trainRelationOnly(
                 interations: Int, learner:SampleRankTrainer[NerSpan], predictor:SettingsSampler[NerSpan],
                 trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document]=Nil,
                 results:Results=null, perfectionize:Boolean) = {
//    System.err.println("Number of gold relations in trainDocuments:      "+trainDocuments.map(d => d.goldTsRelations).flatten.size) // always zero... TODO FIX BUG
//    System.err.println("Number of predicted relations in trainDocuments:      "+trainDocuments.map(d => d.goldTsRelations).flatten.size) // always zero... TODO FIX BUG
    if (perfectionize) {
      (trainDocuments++testDocuments++valDocuments).foreach(d => {LoadTargetSubjectivity.perfectionize(d,"target",true) ; LoadTargetSubjectivity.perfectionize(d,"subjective",true)})
    }
    val trainSpans = trainDocuments.map(_.spansOfClass[NerSpan]).flatten//.take(5).takeRight(1)
    for (i <- 1 to interations) {
      println("Iteration "+i)
      if ((i+1) % TSProp.removeSpansEvery == 0) {
        println("Removing all relations...");
        for(d <- trainDocuments) d.clearRelations
      }
      println("Learner processes all train spans...")
      learner.processContexts(trainSpans)
      if (i >= TSProp.iterationNumBeforePredict) {
        predictRelations(Nil, testDocuments, valDocuments, predictor)
      }
      Printer.printAll(i,trainDocuments,testDocuments,valDocuments,"|",'-')
      if (results != null)
        results += new ResultSet(Result.evalToResult("TRAIN",i,trainDocuments),Result.evalToResult("TEST",i,testDocuments),Result.evalToResult("VAL",i,valDocuments))
    }
    if (perfectionize) {
      (trainDocuments++testDocuments++valDocuments).foreach(_.clearAllNerSpans)
      (trainDocuments++testDocuments++valDocuments).foreach(_.clearRelations())
    }
  }

  /**
   * Train the polarities for spans. Typically, this is only called when spans are already existent. *
   * @param interations
   * @param learner
   * @param predictor
   * @param trainDocuments
   * @param testDocuments
   * @param valDocuments
   * @param results
   * @param perfectionize
   * @param clearSpansAndRelationsAfterRun
   */
  def trainPolarityOnly(
                         interations: Int, learner:SampleRankTrainer[NerSpan], predictor:SettingsSampler[NerSpan],
                         trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document]=Nil,
                         results:Results=null, perfectionize:Boolean, clearSpansAndRelationsAfterRun:Boolean) = {
    if (perfectionize) {
      (trainDocuments).foreach(d => {LoadTargetSubjectivity.perfectionize(d,"target",true) ; LoadTargetSubjectivity.perfectionize(d,"subjective",true)})
      (testDocuments++valDocuments).foreach(d => {LoadTargetSubjectivity.perfectionize(d,"target",false) ; LoadTargetSubjectivity.perfectionize(d,"subjective",false)})
    }
    // randomize labels
//    (trainDocuments++testDocuments++valDocuments).map(_.spansOfClass[NerSpan]).flatten.filter(s => s.label.categoryValue=="subjective").foreach(s => s.polarity.setRandomly())
    val trainSpans = trainDocuments.map(_.spansOfClass[NerSpan]).flatten.filter(s => s.label.categoryValue=="subjective")
    for (i <- 1 to interations) {
      //trainSpans.take(5).foreach(s => println(s.toString))
      println("Iteration "+i)
      println("Learner processes all train spans...")
      learner.processContexts(trainSpans)
      if (i >= TSProp.iterationNumBeforePredict) {
        predictPolarity(trainDocuments, testDocuments, valDocuments, predictor)
      }
      Printer.printAll(i,trainDocuments,testDocuments,valDocuments,"|",'-')
      if (results != null)
        results += new ResultSet(Result.evalToResult("TRAIN",i,trainDocuments),Result.evalToResult("TEST",i,testDocuments),Result.evalToResult("VAL",i,valDocuments))
    }
    if (clearSpansAndRelationsAfterRun) {
      (trainDocuments++testDocuments++valDocuments).foreach(_.clearAllNerSpans)
      (trainDocuments++testDocuments++valDocuments).foreach(_.clearRelations())
    }
  }

// encapsulating some of the relevant stuff for all experiments.
  def trainIter(
        interations: Int, learner:SampleRankTrainer[Token], predictor:SettingsSampler[Token], clearSubjectives:Boolean, clearTargets:Boolean,
        trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document]=Nil,
        subjectivityClassTrainer:SampleRankTrainer[Sentence],subjectivityClassPredictor:SubjectivityPredictor,
        relationLearner:SampleRankTrainer[NerSpan],relationPredictor:SettingsSampler[NerSpan], learnSampler:TokenSpanSampler=null, results:Results=null, learningRates:AdaptiveLearningRate with ParameterAveraging=null,model:TemplateModel with Parameters=null,
        polarityPredictor: PolarityPredictor = null
    ) = {

    val trainTokens = (if (TSProp.randomizeTokens) trainDocuments.map(_.tokens).flatten.shuffle(random) else trainDocuments.map(_.tokens).flatten)

    for (i <- 1 to interations) {
      val numOfTargets = trainTokens.map(_.spansOfClass[NerSpan]).flatten.filter(_.label.categoryValue == "target").size
      val numOfSubj = trainTokens.map(_.spansOfClass[NerSpan]).flatten.filter(_.label.categoryValue == "subjective").size
      println("Iteration "+i+" ("+numOfTargets+" tar, "+numOfSubj+" subj)")
      // Every third iteration remove all the predictions
      if ((i+1) % TSProp.removeSpansEvery == 0) {
        println("Removing spans");
        if (!TSProp.initSpansWithGold) for(d <- trainDocuments) d.clearNerSpans(clearSubjectives,clearTargets)
        if (relationLearner != null) {
          println("Removing all relations...");
          for(d <- trainDocuments) d.clearRelations
        }
      }
      println("Learner processes all train documents...")
      var s = Platform.currentTime
      // learner is of type SampleRankTrainer, which uses TokenSpanSampler
      learner.processContexts(trainTokens)
      if (learnSampler != null) {
        learnSampler.printNumberProposals
        learnSampler.resetNumberOfProposals
      }
      // JOINT HERE?
      if (relationLearner != null) {
        println("Learner processes all train spans...")
        val trainSpans = trainTokens.map(_.startsSpansOfClass[NerSpan]).flatten
//        val trainSpans = trainDocuments.map(_.spansOfClass[NerSpan]).flatten
        relationLearner.processContexts(trainSpans)
      }
      println("Done in "+((Platform.currentTime-s)/1000)+"s")
      if (i >= TSProp.iterationNumBeforePredict) {
        if (learningRates != null) learningRates.setWeightsToAverage(model.parameters)
        predictSpans(testDocuments,valDocuments,predictor,clearSubjectives,clearTargets)
        if (learningRates != null) learningRates.unSetWeightsToAverage(model.parameters)
        predictRelations(if (relationPredictor == null) Nil else trainDocuments,testDocuments,valDocuments,relationPredictor)
        predictPolarity(if (polarityPredictor == null) Nil else trainDocuments,testDocuments,valDocuments,polarityPredictor)
      }
      if (i >= TSProp.iterationNumBeforePredict && TSProp.jointClassificationSpanPrediction)
        classificationTraining(subjectivityClassTrainer,TSProp.jointClassificationtrainiterations,trainDocuments,testDocuments,subjectivityClassPredictor)
      Printer.printAll(i,trainDocuments,testDocuments,valDocuments,"|",'-')
      if (results != null)
        results += new ResultSet(Result.evalToResult("TRAIN",i,trainDocuments),Result.evalToResult("TEST",i,testDocuments),Result.evalToResult("VAL",i,valDocuments))
    }
  }

  // This calls the actual experiments. // xxx
  def train(trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document]=Nil,outFile:String): Results = {
    if (TSProp.polarityExp) { // only relevant for experiments
      polarityExp(trainDocuments,testDocuments,valDocuments,outFile)
    }
    else if (TSProp.deepjointRelation) // important
      deepjointRelSpan(trainDocuments,testDocuments,valDocuments,outFile)
    else if (TSProp.pipelineRelation) // important
      trainWithRelationPipeline(trainDocuments,testDocuments,valDocuments,outFile)
    else { // not relevant any more
      trainPipeline(trainDocuments,testDocuments,valDocuments,outFile)
    }
  }

  def predict(testDocuments:Seq[Document],outFile:String): Results = {
    if (TSProp.deepjointRelation) // important
      deepjointRelSpanPrediction(testDocuments,outFile)
    else if (TSProp.pipelineRelation) // important
      relationPipelinePrediction(testDocuments,outFile)
    else { // not relevant any more
      System.out.println("For this experiment, the prediction phase is not defined.")
      System.exit(2)
      null
    }
  }

  def  predictJointSpanRelation(jointpredictor:SettingsSampler[Token],testDocuments:Seq[Document],results:Results=null) = {
    val testTokens = if (TSProp.randomizeTokens) testDocuments.map(_.tokens).flatten.shuffle(random) else testDocuments.map(_.tokens).flatten

    val s = Platform.currentTime
    for(d <- testDocuments) d.clearNerSpans(true,true)
    println(Printer.evalListHeader("             \t","|",'~'))
    for(j <- 1 to TSProp.predictionIterations) {
      jointpredictor.processAll(testTokens)
      println("TEST   "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(testDocuments,"|"))
    }
    println(Printer.line(160,'~'))
    println("Done in "+((Platform.currentTime-s)/1000)+"s")

    Printer.printAll(-1,Nil,testDocuments,Nil,"|",'-')
    if (results != null)
      results += new ResultSet(Result.evalToResult("TRAIN",-1,Nil),Result.evalToResult("TEST",-1,testDocuments),Result.evalToResult("VAL",-1,Nil))
  }


  // This is a subroutine for the joint experiment. It contains the actual training and testing when the data structures are initialized.
  def  trainJointSpanRelation(
                               interations: Int, jointlearner:SampleRankTrainer[Token], jointpredictor:SettingsSampler[Token],
                               trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document]=Nil,learnSampler:SpanRelationPolaritySampler,
                               results:Results=null) = {

    val trainTokens = (if (TSProp.randomizeTokens) trainDocuments.map(_.tokens).flatten.shuffle(random) else trainDocuments.map(_.tokens).flatten)
    val testTokens = if (TSProp.randomizeTokens) testDocuments.map(_.tokens).flatten.shuffle(random) else testDocuments.map(_.tokens).flatten
    val valTokens = if (TSProp.randomizeTokens) valDocuments.map(_.tokens).flatten.shuffle(random) else valDocuments.map(_.tokens).flatten

    for (i <- 1 to interations) {
      println("Iteration "+i)
      // Every third iteration remove all the predictions
      if ((i+1) % TSProp.removeSpansEvery == 0) {
        println("Removing all spans");
        for(d <- trainDocuments) d.clearNerSpans(true,true)
        println("Removing all relations...");
        for(d <- trainDocuments) d.clearRelations
      }
      println("Learner processes all train documents...")
      var s = Platform.currentTime
      jointlearner.processContexts(trainTokens)
      if (learnSampler != null) {
        learnSampler.printNumberProposals
        learnSampler.resetNumberOfProposals
      }
      println("Done in "+((Platform.currentTime-s)/1000)+"s")
      if (i >= TSProp.iterationNumBeforePredict) {
        println("Predictor processes all test and val documents...")
        val s = Platform.currentTime
        for(d <- testDocuments) d.clearNerSpans(true,true)
        for(d <- valDocuments) d.clearNerSpans(true,true)
        println(Printer.evalListHeader("             \t","|",'~'))
        for(j <- 1 to TSProp.predictionIterations ; if (testDocuments.nonEmpty)) {
          jointpredictor.processAll(testTokens)
          println("TEST   "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(testDocuments,"|"))
          if (valDocuments.nonEmpty) {
            jointpredictor.processAll(valTokens)
            println("VAL    "+"%3d".format(j)+" "+"|"+"\t"+Result.evalList(valDocuments,"|"))
          }
        }
        println(Printer.line(160,'~'))
        println("Done in "+((Platform.currentTime-s)/1000)+"s")
      }
      Printer.printAll(i,trainDocuments,testDocuments,valDocuments,"|",'-')
      if (results != null)
        results += new ResultSet(Result.evalToResult("TRAIN",i,trainDocuments),Result.evalToResult("TEST",i,testDocuments),Result.evalToResult("VAL",i,valDocuments))
//      Printer.printComparison(testDocuments,showFalseResults=true, showLengthDistribution=false, showAverageNumOfSpansPerToken=false)
    }
//    println("TRAIN COMPARISON")
//    Printer.printComparison(trainDocuments,showFalseResults=true, showLengthDistribution=false, showAverageNumOfSpansPerToken=false)
//    println("TEST COMPARISON")
//    Printer.printComparison(testDocuments,showFalseResults=true, showLengthDistribution=false, showAverageNumOfSpansPerToken=false)
  }

  def deepjointRelSpanPrediction(testDocuments:Seq[Document],outFile:String): Results = {
    val sampler = new SpanRelationPolaritySampler(modelContainer.jointModel.asInstanceOf[SpanRelationPolarityModel],TargSubjSpanNER.spanrelationobjective,false)(random){ temperature = 0.001 }
    val adagrad = new AdaGrad(1.0,0.1) with ParameterAveraging // l1,rate,delta
    val learner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(sampler, adagrad) else new SampleRankTrainer(sampler, adagrad)

    val prevVal = TSProp.simpleTokenBasedFeatureOnlyWithJoint
    TSProp.simpleTokenBasedFeatureOnlyWithJoint = false
    println("Train target and subjectivity offsets...")
    
    // the iteration has a slight impact?!, uncommenting that and it works...
    //val trainDocuments = LoadTargetSubjectivity.fromFilename(TSProp.traincsv,true,false,true,TSProp.take,TSProp.srl)
//    val tokens = trainDocuments.map(_.tokens).flatten
//    for (i <- 1 to 5) {
//      println("Iteration "+i)
//      learner.processContexts(tokens) // MUST BE SOMETHING HERE!!! DEBUG FROM HERE! TRACK!
//      //Printer.printAll(i,trainDocuments,Nil,Nil,"|",'-') // (it is not that, turning that of just makes it faster)
//    }

    // HERE STARTS THE ACTUAL PREDICTION, THE STUFF ABOVE IS JUST TRAINING FOR DEBUGGING
    // old... seems to work ok, but leads to wrong results :-(:
    println("DEEP JOINT PREDICTION! -- IMPLEMENTATION IN PROGRESS")
    println("TSSpanFeaturesDomain size="+TSSpanFeaturesDomain.dimensionSize+" ("+TSSpanFeaturesDomain.dimensionDomain.maxSize+")")
    println("LabelDomain "+TSNerDomain.toList)
    println("PolarityDomain "+PolarityDomain.toList)

    val loadedpredictor =
      new SpanRelationPolarityPredictor(
        modelContainer.jointModel.asInstanceOf[SpanRelationPolarityModel])(random){temperature = 0.0001}

    val predictedresults = new Results
    println("Train target and subjectivity offsets...")
    predictJointSpanRelation(loadedpredictor,testDocuments,predictedresults)
    TSProp.simpleTokenBasedFeatureOnlyWithJoint = prevVal
    Printer.saveResultsToFile(outFile,testDocuments)

    predictedresults
  }


  /*
   * THE JOINT MODEL
   */
  def deepjointRelSpan(trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document]=Nil,outFile:String): Results = {
    println("DEEP JOINT!")
    println("docSizes\t"+trainDocuments.size+"\t"+testDocuments.size+"\t"+valDocuments.size)
    println("Using "+trainDocuments.flatMap(_.sentences).size+" training sentences, and "+testDocuments.flatMap(_.sentences).size+" testing ")
    buildDictionaries(trainDocuments)
    println("Have "+trainDocuments.map(_.length).sum+" trainTokens "+testDocuments.map(_.length).sum+" testTokens")
    println("TSSpanFeaturesDomain size="+TSSpanFeaturesDomain.dimensionSize+" ("+TSSpanFeaturesDomain.dimensionDomain.maxSize+")")
    println("LabelDomain "+TSNerDomain.toList)
    println("PolarityDomain "+PolarityDomain.toList)

    
    
    // model stuff joint
    val doGoldcomparison = false
    val model = new SpanRelationPolarityModel
    val sampler = new SpanRelationPolaritySampler(model,TargSubjSpanNER.spanrelationobjective,doGoldcomparison)(random){ temperature = 0.001 } // which objective?
    val adagrad = new AdaGrad(1.0,0.1) with ParameterAveraging // l1,rate,delta
//    val adagrad = new AdaGradDualAveraging(0.00001,1.0,0.1) // l1,rate,delta
    val learner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(sampler, adagrad) else new SampleRankTrainer(sampler, adagrad)
    val predictor = new SpanRelationPolarityPredictor(model)(random){temperature = 0.0001}

    // model stuff pre
//    val preSampler = new TokenSpanSampler(model, objective,true,true,false) { temperature = 0.001 }
//    val preLearner = new SampleRankTrainer(preSampler, adagrad)
//    val prePredictor = new SpanNerPredictor(model,true,true,false){temperature = 0.0001}
//
//    // relation pre stuff
//    val relationSampler = new RelationSampler(model, relationObjective){ temperature = 0.001 }
//    val relationLearner = new SampleRankTrainer(relationSampler, adagrad)
//    val relationPredictor = new RelationPredictor(model)

    println("All preparations finished. Start Training...")

    //trainRelationOnly(TSProp.trainiterations, relationLearner, relationPredictor, trainDocuments, testDocuments, valDocuments, null, true)

    val prevVal = TSProp.simpleTokenBasedFeatureOnlyWithJoint
    TSProp.simpleTokenBasedFeatureOnlyWithJoint = false
    // Train!
    val results = new Results
    println("Train target and subjectivity offsets...")
//    trainIter(TSProp.trainiterations, jointLearner, predictor, true,true,
//      trainDocuments, testDocuments, valDocuments,null,null,null,null,results)
    // pretraining
//    println("RELATION-PRE-TRAINING")
//    TSProp.useRelationForSpanDetection = false
//    trainRelationOnly(TSProp.trainiterations, relationLearner, relationPredictor, trainDocuments, testDocuments, valDocuments, null, true)
//    TSProp.useRelationForSpanDetection = true
//    println("PRETRAINING")
//    trainIter(5, preLearner, prePredictor, !TSProp.perfectSubjectivity,!TSProp.perfectTargets,
//      trainDocuments, testDocuments, valDocuments,null,null,null,null, results)
    // joint
    println("JOINTTRAINING")
    if (TSProp.initSpansWithGold) LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true)
    if (TSProp.initRelationsWithGold) {LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true);LoadTargetSubjectivity.perfectionizeRelation(trainDocuments)}
    trainJointSpanRelation(TSProp.trainiterations,learner,predictor,trainDocuments,testDocuments,valDocuments,sampler,results)

    // model to be saved:
    modelContainer.jointModel = model

    TSProp.simpleTokenBasedFeatureOnlyWithJoint = prevVal
    Printer.saveResultsToFile(outFile,testDocuments)
    
    //
    // as a test:
//    deepjointRelSpanPrediction(LoadTargetSubjectivity.fromFilename(TSProp.testcsv,true,false,true,TSProp.srl),"/Users/rklinger/Desktop/testout.txt",modelContainer)
    //
    results
  }

// only for experiments
  def polarityExp(trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document]=Nil,outFile:String): Results = {
    println("POLARITY EXP")
    println("Using "+trainDocuments.flatMap(_.sentences).size+" training sentences, and "+testDocuments.flatMap(_.sentences).size+" testing ")
    buildDictionaries(trainDocuments)
    println("Have "+trainDocuments.map(_.length).sum+" trainTokens "+testDocuments.map(_.length).sum+" testTokens")
    println("TSSpanFeaturesDomain size="+TSSpanFeaturesDomain.dimensionSize+" ("+TSSpanFeaturesDomain.dimensionDomain.maxSize+")")
    println("LabelDomain "+TSNerDomain.toList)
    println("PolarityDomain "+PolarityDomain.toList)

    // model stuff
    val model = new PolaritySpanRelationModel

    // polarity only
    val polaritySampler = new PolarityOnlySampler(model, TargSubjSpanNER.polarityObjective)(random) { temperature = 0.001 }
    val adaGrad = new AdaGrad(1.0,0.1) with ParameterAveraging
//    val adaGrad = new AdaGradDualAveraging(0.00001,1.0,0.1)
    val polarityLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(polaritySampler, adaGrad) else new SampleRankTrainer(polaritySampler, adaGrad)
    val polarityPredictor = new PolarityPredictor(model)(random){temperature = 0.0001}

//    //    // Joint
//    val jointSampler = new TokenSpanSampler(model, objective,!TSProp.perfectSubjectivity,!TSProp.perfectTargets, true) { temperature = 0.001 }
//    val adagradJoint = new AdaGradDualAveraging(0.00001,1.0,0.1) // l1,rate,delta
//    val jointLearner = new SampleRankTrainer(jointSampler, adagradJoint)
//    val predictor = new SpanNerPredictor(model,!TSProp.perfectSubjectivity,!TSProp.perfectTargets, true){temperature = 0.0001}
//
//    //    // subjective only learner
//    val subjectivitySpanModel = if (TSProp.perfectSubjectivity) new SpanNerModel else model // with pre training but joint, same model use
//    val subjectiveSpanSampler = new TokenSpanSampler(subjectivitySpanModel, objective, true, false, true) { temperature = 0.001 } // TODO fix to subj
//    val adagradSubjectiveSpan = if (TSProp.perfectSubjectivity) new AdaGradDualAveraging(0.00001,1.0,0.1) else adagradJoint  // with pre training but joint, same model use
//    val subjectiveSpanLearner = new SampleRankTrainer(subjectiveSpanSampler, adagradSubjectiveSpan)
//    val subjectivitySpanPredictor = new SpanNerPredictor(subjectivitySpanModel,true,false, true){temperature = 0.0001}
//    //
//    //    // target only learner
//    val targetSpanModel = if (TSProp.perfectTargets) new SpanNerModel else model // with pre training but joint, same model use
//    val targetSpanSampler = new TokenSpanSampler(targetSpanModel, objective, false, true, true) { temperature = 0.001 } // TODO fix to subj
//    val adagradTargetSpan = if (TSProp.perfectTargets) new AdaGradDualAveraging(0.00001,1.0,0.1) else adagradJoint  // with pre training but joint, same model use
//    val targetSpanLearner = new SampleRankTrainer(targetSpanSampler, adagradTargetSpan)
//    val targetSpanPredictor = new SpanNerPredictor(targetSpanModel,false,true, true){temperature = 0.0001}
//    //
//    //    // subjectivity class learner
//    val subjectivityClassModel = new SubjectivityModel
//    val subjectivityClassSampler = new SubjectivityClassificationSampler(subjectivityClassModel, subjectivityObjective)
//    val subjectivityClassLearner = new SampleRankTrainer(subjectivityClassSampler, new SimpleMIRA())
//    val subjectivityClassPredictor = new SubjectivityPredictor(subjectivityClassModel)
//    //
//    //    // targ-subj relations
//    val relationModel = new TargSubjRelationModel
//    val relationSampler = new RelationSampler(relationModel, relationObjective){ temperature = 0.001 }
//    //val relationSampler = new RelationSampler(relationModel, spanrelationobjective){ temperature = 0.001 }
//    val relationLearner = new SampleRankTrainer(relationSampler, new AdaGradDualAveraging(0.00001,1.0,0.1))
//    val relationPredictor = new RelationPredictor(relationModel)

    println("All preparations finished. Start Training...")

    val results = new Results

    // first, train the relation extraction model with perfect information
    println("Train polarity detector on perfect spans.")
    trainPolarityOnly(TSProp.trainiterations, polarityLearner, polarityPredictor, trainDocuments, testDocuments, valDocuments, results, true, false)

    // now, do subjectivity/target training as usual, use the relation extractor only for application -- this might be joint as well!

    // if subjectivity first, ie. pipeline architecture, train subjectivity model alone first, this is a totally different model!
//    if (TSProp.subjectivityFirst) {
//      // do subjective training, target comes later
//      val prevVal = TSProp.simpleTokenBasedFeatureOnlyWithJoint
//      TSProp.simpleTokenBasedFeatureOnlyWithJoint = false
//      println("Pipeline workflow, do subjectivity only first...")
//      trainIter(min(TSProp.trainiterations,10), subjectiveSpanLearner, subjectivitySpanPredictor, true,false,
//        trainDocuments, testDocuments, valDocuments, subjectivityClassLearner, subjectivityClassPredictor, null, null, results) // no rel pred
//      TSProp.simpleTokenBasedFeatureOnlyWithJoint = prevVal
//    }
//
//    if (TSProp.targetFirst) {
//      // do target training, subjective comes later
//      val prevVal = TSProp.simpleTokenBasedFeatureOnlyWithJoint
//      TSProp.simpleTokenBasedFeatureOnlyWithJoint = false
//      println("Pipeline workflow, do target only first...")
//      trainIter(min(TSProp.trainiterations,10), targetSpanLearner, targetSpanPredictor, false,true,
//        trainDocuments, testDocuments, valDocuments, subjectivityClassLearner, subjectivityClassPredictor, null, null, results) // no rel pred, here it could make sense
//      TSProp.simpleTokenBasedFeatureOnlyWithJoint = prevVal
//    }
//
//    // Train! Now the real thing where both subjective and target are predicted. If pipeline model, subjectivity is just as the predictor does.
//    if (!TSProp.perfectSubjectivity || !TSProp.perfectTargets) { // this was && before
//      (trainDocuments++testDocuments++valDocuments).foreach(x => {x.clearRelations() ; /*x.clearAllNerSpans*/})
//      println("Train target and subjectivity offsets...")
//      trainIter(TSProp.trainiterations, jointLearner, predictor, !TSProp.perfectSubjectivity,!TSProp.perfectTargets,
//        trainDocuments, testDocuments, valDocuments,subjectivityClassLearner,subjectivityClassPredictor,null,relationPredictor, results)
//    }

    //Printer.printComparison(testDocuments,true,true,true)
    Printer.printPolarityResults(testDocuments)
    results
  }

  /**
   * Pipeline prediction
   */
  def relationPipelinePrediction(testDocuments:Seq[Document],outFile:String): Results = {
    System.err.println("Not yet implemented...")
    System.exit(3)
//    println("RELATION PIPELINE")
//    println("TSSpanFeaturesDomain size="+TSSpanFeaturesDomain.dimensionSize+" ("+TSSpanFeaturesDomain.dimensionDomain.maxSize+")")
//    println("LabelDomain "+TSNerDomain.toList)
//    println("LabelDomain "+PolarityDomain.toList)
//
//    // model stuff
//    val model = new SpanNerModel
//    val delta = 10 // default: 0.1
//    //    // Joint
//    val jointSampler = new TokenSpanSampler(model, TargSubjSpanNER.objective,!TSProp.perfectSubjectivity,!TSProp.perfectTargets, true)(random) { temperature = 0.001 }
//    //    val adagradJoint = new AdaGradDualAveraging(0.00001,1.0,0.1) // l1,rate,delta
//    val adagradJoint = new AdaGrad(1.0,delta) with ParameterAveraging
//    val jointLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(jointSampler, adagradJoint) else new SampleRankTrainer(jointSampler, adagradJoint)
//    val predictor = new SpanNerPredictor(model,!TSProp.perfectSubjectivity,!TSProp.perfectTargets, true)(random){temperature = 0.0001}
//
//    //    // subjective only learner
//    val subjectivitySpanModel = if (TSProp.perfectSubjectivity) new SpanNerModel else model // with pre training but joint, same model use
//    val subjectiveSpanSampler = new TokenSpanSampler(subjectivitySpanModel, TargSubjSpanNER.objective, true, false, true)(random) { temperature = 0.001 } // TODO fix to subj
//    val adagradSubjectiveSpan = if (TSProp.perfectSubjectivity) new AdaGrad(1.0,delta) with ParameterAveraging else adagradJoint  // with pre training but joint, same model use
//    //    val adagradSubjectiveSpan = if (TSProp.perfectSubjectivity) new AdaGradDualAveraging(0.00001,1.0,0.1) else adagradJoint  // with pre training but joint, same model use
//    val subjectiveSpanLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(subjectiveSpanSampler, adagradSubjectiveSpan) else new SampleRankTrainer(subjectiveSpanSampler, adagradSubjectiveSpan)
//    val subjectivitySpanPredictor = new SpanNerPredictor(subjectivitySpanModel,true,false, true)(random){temperature = 0.0001}
//    //
//    //    // target only learner
//    val targetSpanModel = if (TSProp.perfectTargets) new SpanNerModel else model // with pre training but joint, same model use
//    val targetSpanSampler = new TokenSpanSampler(targetSpanModel, TargSubjSpanNER.objective, false, true, true)(random) { temperature = 0.001 } // TODO fix to subj
//    val adagradTargetSpan = if (TSProp.perfectTargets) new AdaGrad(1.0,delta) with ParameterAveraging else adagradJoint  // with pre training but joint, same model use
//    //    val adagradTargetSpan = if (TSProp.perfectTargets) new AdaGradDualAveraging(0.00001,1.0,0.1) else adagradJoint  // with pre training but joint, same model use
//    val targetSpanLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(targetSpanSampler, adagradTargetSpan) else new SampleRankTrainer(targetSpanSampler, adagradTargetSpan)
//    val targetSpanPredictor = new SpanNerPredictor(targetSpanModel,false,true, true)(random){temperature = 0.0001}
//    //
//    //    // subjectivity class learner
//    val subjectivityClassModel = new SubjectivityModel
//    val subjectivityClassSampler = new SubjectivityClassificationSampler(subjectivityClassModel, TargSubjSpanNER.subjectivityObjective)(random)
//    //    val subjectivityClassLearner = new SampleRankTrainer(subjectivityClassSampler, new SimpleMIRA())
//    val subjectivityClassLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(subjectivityClassSampler, new AdaMira(1.0,0.1,1.0)) else new SampleRankTrainer(subjectivityClassSampler, new AdaMira(1.0,0.1,1.0))
//    val subjectivityClassPredictor = new SubjectivityPredictor(subjectivityClassModel)(random)
//    //
//    //    // targ-subj relations
//    val relationModel = new TargSubjRelationModel
//    val relationSampler = new RelationSampler(relationModel, TargSubjSpanNER.relationObjective)(random){ temperature = 0.001 }
//    //val relationSampler = new RelationSampler(relationModel, spanrelationobjective){ temperature = 0.001 }
//    //    val relationLearner = new SampleRankTrainer(relationSampler, new AdaGradDualAveraging(0.00001,1.0,0.1))
//    val relationLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(relationSampler, new MIRA() /*with ParameterAveraging*/) else new SampleRankTrainer(relationSampler, new MIRA() /*with ParameterAveraging*/)
//    val relationPredictor = new RelationPredictor(relationModel)(random)
//
//    // polarity, similarly pretrain and then apply as in relations
//    val polarityModel = new PolaritySpanRelationModel
//    // polarity only
//    val polaritySampler = new PolarityOnlySampler(polarityModel, TargSubjSpanNER.polarityObjective)(random) { temperature = 0.001 }
//    val adaGrad = new AdaGrad(1.0,0.1) with ParameterAveraging
//    val polarityLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(polaritySampler, adaGrad) else new SampleRankTrainer(polaritySampler, adaGrad)
//    val polarityPredictor = new PolarityPredictor(polarityModel)(random){temperature = 0.0001}
//
//    println("All preparations finished. Start Training...")
//    println("Train classifier for subjectivity of the whole sentence...")
//
//
    val results = new Results

//    // do polarity
//    if (TSProp.doPolarity) { // false by default
//      println("Train polarity detector on perfect information")
//      trainPolarityOnly(scala.math.min(5,TSProp.trainiterations), polarityLearner, polarityPredictor, trainDocuments, testDocuments, valDocuments, results, true, true)
//    }
//
//    // first, train the relation extraction model with perfect information
//    println("Train relation extractor on perfect information.")
//    if (TSProp.initRelationsWithGold) {LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true);LoadTargetSubjectivity.perfectionizeRelation(trainDocuments)}
//    TSProp.useRelationForSpanDetection = false
//    trainRelationOnly(TSProp.trainiterations, relationLearner, relationPredictor, trainDocuments, testDocuments, valDocuments, null, ! TSProp.initRelationsWithGold)
//    TSProp.useRelationForSpanDetection = true
//
//    // now, do subjectivity/target training as usual, use the relation extractor only for application -- this might be joint as well!
//    // if subjectivity first, ie. pipeline architecture, train subjectivity model alone first, this is a totally different model!
//    if (TSProp.subjectivityFirst) {
//      // maybe it helps to now start span training from the gold position
//      if (TSProp.initSpansWithGold) LoadTargetSubjectivity.perfectionize(trainDocuments, "subjective",true) // NEW!!!
//      if (TSProp.initRelationsWithGold) {LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true);LoadTargetSubjectivity.perfectionizeRelation(trainDocuments)}
//      // do subjective training, target comes later
//      val prevVal = TSProp.simpleTokenBasedFeatureOnlyWithJoint
//      TSProp.simpleTokenBasedFeatureOnlyWithJoint = false
//      println("Pipeline workflow, do subjectivity only first...")
//      trainIter(min(TSProp.trainiterations,10), subjectiveSpanLearner, subjectivitySpanPredictor, true,false,
//        trainDocuments, testDocuments, valDocuments, subjectivityClassLearner, subjectivityClassPredictor, null, null, subjectiveSpanSampler, results, adagradSubjectiveSpan, model,
//        polarityPredictor = polarityPredictor
//      ) // no rel pred
//      TSProp.simpleTokenBasedFeatureOnlyWithJoint = prevVal
//    }
//
//
//    if (TSProp.targetFirst) {
//      if (TSProp.initSpansWithGold) LoadTargetSubjectivity.perfectionize(trainDocuments, "target",true) // NEW!!!
//      if (TSProp.initRelationsWithGold) {LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true);LoadTargetSubjectivity.perfectionizeRelation(trainDocuments)}
//      // do target training, subjective comes later
//      val prevVal = TSProp.simpleTokenBasedFeatureOnlyWithJoint
//      TSProp.simpleTokenBasedFeatureOnlyWithJoint = false
//      println("Pipeline workflow, do target only first...")
//      trainIter(min(TSProp.trainiterations,10), targetSpanLearner, targetSpanPredictor, false,true,
//        trainDocuments, testDocuments, valDocuments, subjectivityClassLearner, subjectivityClassPredictor, null, null, targetSpanSampler, results) // no rel pred, here it could make sense
//      TSProp.simpleTokenBasedFeatureOnlyWithJoint = prevVal
//    }
//
//    // Train! Now the real thing where both subjective and target are predicted. If pipeline model, subjectivity is just as the predictor does.
//    if (!TSProp.perfectSubjectivity || !TSProp.perfectTargets) { // this was && before
//      if (TSProp.initSpansWithGold) LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true) // NEW!!!
//      if (TSProp.initRelationsWithGold) {LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true);LoadTargetSubjectivity.perfectionizeRelation(trainDocuments)}
//      (trainDocuments++testDocuments++valDocuments).foreach(x => {x.clearRelations() ; /*x.clearAllNerSpans*/})
//      println("Train target and subjectivity offsets...")
//      trainIter(TSProp.trainiterations, jointLearner, predictor, !TSProp.perfectSubjectivity,!TSProp.perfectTargets,
//        trainDocuments, testDocuments, valDocuments,subjectivityClassLearner,subjectivityClassPredictor,null,relationPredictor,jointSampler, results, polarityPredictor = if (TSProp.doPolarity) polarityPredictor else null)
//    }
//
//    // Put together models to save
//    results.lastModel = new SerializableContainerModel(
//      subjectivitySpanModel = subjectivitySpanModel,
//      targetSpanModel = targetSpanModel,
//      subjectivityClassModel = subjectivityClassModel,
//      relationModel = relationModel,
//      polarityModel = polarityModel)
//
//    //Printer.printComparison(testDocuments,true,true,true)
//    if (TSProp.saveWeightsToFile != "false") Printer.saveWeightsToFile(model,TSProp.saveWeightsToFile)
//    Printer.saveResultsToFile(outFile,testDocuments)
    results
  }
  
  /**
   * The PIPELINE MODEL
   * @param trainDocuments
   * @param testDocuments
   * @param valDocuments
   * @param outFile
   * @return
   */
  def trainWithRelationPipeline(trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document]=Nil,outFile:String): Results = {
    println("RELATION PIPELINE")
    println("Using "+trainDocuments.flatMap(_.sentences).size+" training sentences, and "+testDocuments.flatMap(_.sentences).size+" testing ")
    buildDictionaries(trainDocuments)
    println("Have "+trainDocuments.map(_.length).sum+" trainTokens "+testDocuments.map(_.length).sum+" testTokens")
    println("TSSpanFeaturesDomain size="+TSSpanFeaturesDomain.dimensionSize+" ("+TSSpanFeaturesDomain.dimensionDomain.maxSize+")")
    println("LabelDomain "+TSNerDomain.toList)
    println("LabelDomain "+PolarityDomain.toList)

    // model stuff
    val model = new SpanNerModel
    val delta = 10 // default: 0.1
    //    // Joint
    val jointSampler = new TokenSpanSampler(model, TargSubjSpanNER.objective,!TSProp.perfectSubjectivity,!TSProp.perfectTargets, true)(random) { temperature = 0.001 }
//    val adagradJoint = new AdaGradDualAveraging(0.00001,1.0,0.1) // l1,rate,delta
    val adagradJoint = new AdaGrad(1.0,delta) with ParameterAveraging
    val jointLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(jointSampler, adagradJoint) else new SampleRankTrainer(jointSampler, adagradJoint)
    val predictor = new SpanNerPredictor(model,!TSProp.perfectSubjectivity,!TSProp.perfectTargets, true)(random){temperature = 0.0001}

    //    // subjective only learner
    val subjectivitySpanModel = if (TSProp.perfectSubjectivity) new SpanNerModel else model // with pre training but joint, same model use
    val subjectiveSpanSampler = new TokenSpanSampler(subjectivitySpanModel, TargSubjSpanNER.objective, true, false, true)(random) { temperature = 0.001 } // TODO fix to subj
    val adagradSubjectiveSpan = if (TSProp.perfectSubjectivity) new AdaGrad(1.0,delta) with ParameterAveraging else adagradJoint  // with pre training but joint, same model use
//    val adagradSubjectiveSpan = if (TSProp.perfectSubjectivity) new AdaGradDualAveraging(0.00001,1.0,0.1) else adagradJoint  // with pre training but joint, same model use
    val subjectiveSpanLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(subjectiveSpanSampler, adagradSubjectiveSpan) else new SampleRankTrainer(subjectiveSpanSampler, adagradSubjectiveSpan)
    val subjectivitySpanPredictor = new SpanNerPredictor(subjectivitySpanModel,true,false, true)(random){temperature = 0.0001}
    //
    //    // target only learner
    val targetSpanModel = if (TSProp.perfectTargets) new SpanNerModel else model // with pre training but joint, same model use
    val targetSpanSampler = new TokenSpanSampler(targetSpanModel, TargSubjSpanNER.objective, false, true, true)(random) { temperature = 0.001 } // TODO fix to subj
    val adagradTargetSpan = if (TSProp.perfectTargets) new AdaGrad(1.0,delta) with ParameterAveraging else adagradJoint  // with pre training but joint, same model use
//    val adagradTargetSpan = if (TSProp.perfectTargets) new AdaGradDualAveraging(0.00001,1.0,0.1) else adagradJoint  // with pre training but joint, same model use
    val targetSpanLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(targetSpanSampler, adagradTargetSpan) else new SampleRankTrainer(targetSpanSampler, adagradTargetSpan)
    val targetSpanPredictor = new SpanNerPredictor(targetSpanModel,false,true, true)(random){temperature = 0.0001}
    //
    //    // subjectivity class learner
    val subjectivityClassModel = new SubjectivityModel
    val subjectivityClassSampler = new SubjectivityClassificationSampler(subjectivityClassModel, TargSubjSpanNER.subjectivityObjective)(random)
//    val subjectivityClassLearner = new SampleRankTrainer(subjectivityClassSampler, new SimpleMIRA())
    val subjectivityClassLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(subjectivityClassSampler, new AdaMira(1.0,0.1,1.0)) else new SampleRankTrainer(subjectivityClassSampler, new AdaMira(1.0,0.1,1.0))
    val subjectivityClassPredictor = new SubjectivityPredictor(subjectivityClassModel)(random)
    //
    //    // targ-subj relations
    val relationModel = new TargSubjRelationModel
    val relationSampler = new RelationSampler(relationModel, TargSubjSpanNER.relationObjective)(random){ temperature = 0.001 }
    //val relationSampler = new RelationSampler(relationModel, spanrelationobjective){ temperature = 0.001 }
//    val relationLearner = new SampleRankTrainer(relationSampler, new AdaGradDualAveraging(0.00001,1.0,0.1))
    val relationLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(relationSampler, new MIRA() /*with ParameterAveraging*/) else new SampleRankTrainer(relationSampler, new MIRA() /*with ParameterAveraging*/)
    val relationPredictor = new RelationPredictor(relationModel)(random)

    // polarity, similarly pretrain and then apply as in relations
    val polarityModel = new PolaritySpanRelationModel
    // polarity only
    val polaritySampler = new PolarityOnlySampler(polarityModel, TargSubjSpanNER.polarityObjective)(random) { temperature = 0.001 }
    val adaGrad = new AdaGrad(1.0,0.1) with ParameterAveraging
    val polarityLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(polaritySampler, adaGrad) else new SampleRankTrainer(polaritySampler, adaGrad)
    val polarityPredictor = new PolarityPredictor(polarityModel)(random){temperature = 0.0001}

    println("All preparations finished. Start Training...")
    println("Train classifier for subjectivity of the whole sentence...")


    val results = new Results

    // do polarity
    if (TSProp.doPolarity) { // false by default
      println("Train polarity detector on perfect information")
      trainPolarityOnly(scala.math.min(5,TSProp.trainiterations), polarityLearner, polarityPredictor, trainDocuments, testDocuments, valDocuments, results, true, true)
    }

    // first, train the relation extraction model with perfect information
    println("Train relation extractor on perfect information.")
    if (TSProp.initRelationsWithGold) {LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true);LoadTargetSubjectivity.perfectionizeRelation(trainDocuments)}
    TSProp.useRelationForSpanDetection = false
    trainRelationOnly(TSProp.trainiterations, relationLearner, relationPredictor, trainDocuments, testDocuments, valDocuments, null, ! TSProp.initRelationsWithGold)
    TSProp.useRelationForSpanDetection = true

    // now, do subjectivity/target training as usual, use the relation extractor only for application -- this might be joint as well!
    // if subjectivity first, ie. pipeline architecture, train subjectivity model alone first, this is a totally different model!
    if (TSProp.subjectivityFirst) {
      // maybe it helps to now start span training from the gold position
      if (TSProp.initSpansWithGold) LoadTargetSubjectivity.perfectionize(trainDocuments, "subjective",true) // NEW!!!
      if (TSProp.initRelationsWithGold) {LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true);LoadTargetSubjectivity.perfectionizeRelation(trainDocuments)}
      // do subjective training, target comes later
      val prevVal = TSProp.simpleTokenBasedFeatureOnlyWithJoint
      TSProp.simpleTokenBasedFeatureOnlyWithJoint = false
      println("Pipeline workflow, do subjectivity only first...")
      trainIter(min(TSProp.trainiterations,10), subjectiveSpanLearner, subjectivitySpanPredictor, true,false,
        trainDocuments, testDocuments, valDocuments, subjectivityClassLearner, subjectivityClassPredictor, null, null, subjectiveSpanSampler, results, adagradSubjectiveSpan, model,
        polarityPredictor = polarityPredictor
      ) // no rel pred
      TSProp.simpleTokenBasedFeatureOnlyWithJoint = prevVal
    }


    if (TSProp.targetFirst) {
      if (TSProp.initSpansWithGold) LoadTargetSubjectivity.perfectionize(trainDocuments, "target",true) // NEW!!!
      if (TSProp.initRelationsWithGold) {LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true);LoadTargetSubjectivity.perfectionizeRelation(trainDocuments)}
      // do target training, subjective comes later
      val prevVal = TSProp.simpleTokenBasedFeatureOnlyWithJoint
      TSProp.simpleTokenBasedFeatureOnlyWithJoint = false
      println("Pipeline workflow, do target only first...")
      trainIter(min(TSProp.trainiterations,10), targetSpanLearner, targetSpanPredictor, false,true,
        trainDocuments, testDocuments, valDocuments, subjectivityClassLearner, subjectivityClassPredictor, null, null, targetSpanSampler, results) // no rel pred, here it could make sense
      TSProp.simpleTokenBasedFeatureOnlyWithJoint = prevVal
    }

    // Train! Now the real thing where both subjective and target are predicted. If pipeline model, subjectivity is just as the predictor does.
    if (!TSProp.perfectSubjectivity || !TSProp.perfectTargets) { // this was && before
      if (TSProp.initSpansWithGold) LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true) // NEW!!!
      if (TSProp.initRelationsWithGold) {LoadTargetSubjectivity.perfectionize(trainDocuments, "all",true);LoadTargetSubjectivity.perfectionizeRelation(trainDocuments)}
      (trainDocuments++testDocuments++valDocuments).foreach(x => {x.clearRelations() ; /*x.clearAllNerSpans*/})
      println("Train target and subjectivity offsets...")
      trainIter(TSProp.trainiterations, jointLearner, predictor, !TSProp.perfectSubjectivity,!TSProp.perfectTargets,
        trainDocuments, testDocuments, valDocuments,subjectivityClassLearner,subjectivityClassPredictor,null,relationPredictor,jointSampler, results, polarityPredictor = if (TSProp.doPolarity) polarityPredictor else null)
    }
    
    // Put together models to save
    modelContainer.subjectivitySpanModel = subjectivitySpanModel
    modelContainer.targetSpanModel = targetSpanModel
    modelContainer.subjectivityClassModel = subjectivityClassModel
    modelContainer.relationModel = relationModel
    modelContainer.polarityModel = polarityModel

    //Printer.printComparison(testDocuments,true,true,true)
    if (TSProp.saveWeightsToFile != "false") Printer.saveWeightsToFile(model,TSProp.saveWeightsToFile)
    Printer.saveResultsToFile(outFile,testDocuments)
    results
  }

// PIPELINE MODEL OUTDATED!
  def trainPipeline(trainDocuments:Seq[Document], testDocuments:Seq[Document], valDocuments:Seq[Document]=Nil,outFile:String): Results = {
    println("PIPELINE")
    println("Using "+trainDocuments.flatMap(_.sentences).size+" training sentences, and "+testDocuments.flatMap(_.sentences).size+" testing ")
    buildDictionaries(trainDocuments)
    println("Have "+trainDocuments.map(_.length).sum+" trainTokens "+testDocuments.map(_.length).sum+" testTokens")
    println("TSSpanFeaturesDomain size="+TSSpanFeaturesDomain.dimensionSize+" ("+TSSpanFeaturesDomain.dimensionDomain.maxSize+")")
    println("LabelDomain "+TSNerDomain.toList)

    // model stuff
    // 3rd step models
    val model = new SpanNerModel
    // 1st step models
    val subjectivitySpanModel = if (TSProp.perfectSubjectivity) new SpanNerModel else model // with pre training but joint, same model use
    val targetSpanModel = if (TSProp.perfectTargets) new SpanNerModel else model // with pre training but joint, same model use
    // classication model
    val subjectivityClassModel = new SubjectivityModel

    // Joint, TODO think about not using same adagrad in subjective span, even when joint
    val jointSampler = new TokenSpanSampler(model, TargSubjSpanNER.objective,!TSProp.perfectSubjectivity,!TSProp.perfectTargets, true)(random) { temperature = 0.001 }
//    val adagradJoint = new AdaGradDualAveraging(0.00001,1.0,0.1) // l1,rate,delta
    val adagradJoint = new AdaGrad(1.0,0.1) with ParameterAveraging
    val jointLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(jointSampler, adagradJoint) else new SampleRankTrainer(jointSampler, adagradJoint)
    val predictor = new SpanNerPredictor(model,!TSProp.perfectSubjectivity,!TSProp.perfectTargets, true)(random){temperature = 0.0001}

    // intermediate one, for joint, uses same model and same adagrad
    val targetOnlySampler = new TokenSpanSampler(model, TargSubjSpanNER.objective,false,true,true)(random) { temperature = 0.001 }
    val targetOnlyLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(targetOnlySampler, adagradJoint) else new SampleRankTrainer(targetOnlySampler, adagradJoint)
    val targetOnlyPredictor = new SpanNerPredictor(model,false,true,true)(random){temperature = 0.0001} // only in use when joint

    // subjective only learner
    val subjectiveSpanSampler = new TokenSpanSampler(subjectivitySpanModel, TargSubjSpanNER.objective, true, false, true)(random) { temperature = 0.001 } // TODO fix to subj
    val adagradSubjectiveSpan = if (TSProp.perfectSubjectivity) new AdaGrad(1.0,0.1) with ParameterAveraging else adagradJoint  // with pre training but joint, same model use
    //val adagradSubjectiveSpan = if (TSProp.perfectSubjectivity) new AdaGradDualAveraging(0.00001,1.0,0.1) else adagradJoint  // with pre training but joint, same model use
    val subjectiveSpanLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(subjectiveSpanSampler, adagradSubjectiveSpan) else new SampleRankTrainer(subjectiveSpanSampler, adagradSubjectiveSpan)
    val subjectivitySpanPredictor = new SpanNerPredictor(subjectivitySpanModel,true,false,true)(random){temperature = 0.0001}

    // target only learner
    val targetSpanSampler = new TokenSpanSampler(targetSpanModel, TargSubjSpanNER.objective, false, true, true)(random) { temperature = 0.001 } // TODO fix to subj
    val adagradTargetSpan = if (TSProp.perfectTargets) new AdaGrad(1.0,0.1) with ParameterAveraging else adagradJoint  // with pre training but joint, same model use // was new AdaGradDualAveraging(0.00001,1.0,0.1
//    val adagradTargetSpan = if (TSProp.perfectTargets) new AdaGradDualAveraging(0.00001,1.0,0.1) else adagradJoint  // with pre training but joint, same model use // was new AdaGradDualAveraging(0.00001,1.0,0.1
    val targetSpanLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(targetSpanSampler, adagradTargetSpan) else new SampleRankTrainer(targetSpanSampler, adagradTargetSpan)
    val targetSpanPredictor = new SpanNerPredictor(targetSpanModel,false,true,true)(random){temperature = 0.0001}

    // subjectivity class learner
    val subjectivityClassSampler = new SubjectivityClassificationSampler(subjectivityClassModel, TargSubjSpanNER.subjectivityObjective)(random)
    val subjectivityClassLearner = if (TSProp.forwardSampleRank) new FactoriePort.SampleRankTrainer(subjectivityClassSampler, new AdaMira(1.0,0.1,1.0)) else new SampleRankTrainer(subjectivityClassSampler, new AdaMira(1.0,0.1,1.0)) // was new SimpleMIRA instead of AdaMira , numbers unclear
    val subjectivityClassPredictor = new SubjectivityPredictor(subjectivityClassModel)(random)

    println("All preparations finished. Start Training...")
    println("Train classifier for subjectivity of the whole sentence...")
    // train subjectivity classification
    if (!TSProp.jointClassificationSpanPrediction) // can easily be commented out to do a pretraining
      classificationTraining(
        subjectivityClassLearner,
        if (TSProp.jointClassificationSpanPrediction) TSProp.jointClassificationtrainiterations else TSProp.classificationtrainiterations,
        trainDocuments,testDocuments, subjectivityClassPredictor
    )

    // if subjectivity first, ie. pipeline architecture, train subjectivity model alone first, this is a totally different model!
    if (TSProp.subjectivityFirst) {
      val prevVal = TSProp.simpleTokenBasedFeatureOnlyWithJoint
      TSProp.simpleTokenBasedFeatureOnlyWithJoint = false
      println("Pipeline workflow, do subjectivity only first...")
      trainIter(min(TSProp.trainiterations,10), subjectiveSpanLearner, subjectivitySpanPredictor, true,false,
        trainDocuments, testDocuments, valDocuments, subjectivityClassLearner, subjectivityClassPredictor,null,null,null)
      TSProp.simpleTokenBasedFeatureOnlyWithJoint = prevVal
    }

    if (TSProp.targetFirst) {
      val prevVal = TSProp.simpleTokenBasedFeatureOnlyWithJoint
      TSProp.simpleTokenBasedFeatureOnlyWithJoint = false
      println("Pipeline workflow, do target only first...")
      trainIter(min(TSProp.trainiterations,10), targetSpanLearner, targetSpanPredictor, false,true,
        trainDocuments, testDocuments, valDocuments, subjectivityClassLearner, subjectivityClassPredictor,null,null,null)
      TSProp.simpleTokenBasedFeatureOnlyWithJoint = prevVal
    }

    // Train! Now the real thing where both subjective and target are predicted. If pipeline model, subjectivity is just just as the predictor does.
    val results = new Results
    println("Train target and subjectivity offsets...")
    trainIter(TSProp.trainiterations, jointLearner, predictor, !TSProp.perfectSubjectivity,!TSProp.perfectTargets,
      trainDocuments, testDocuments, valDocuments,subjectivityClassLearner,subjectivityClassPredictor,null,null,null,results)

    // guess/
    if (TSProp.guessrelation) {
      guessRelation(trainDocuments)
      guessRelation(testDocuments)
      guessRelation(valDocuments)
      Printer.printAll(9999,trainDocuments,testDocuments,valDocuments,"|",'-')
    }

     //Printer.printComparison(testDocuments,true,true,true)
    if (TSProp.saveWeightsToFile != "false") Printer.saveWeightsToFile(model,TSProp.saveWeightsToFile)
    Printer.saveResultsToFile(outFile,testDocuments)
    results
  }

  /**
   * This is just guessing the relations instead of really using the ones which has been predicted. This is as a baseline and for sanity checks.
   * @param docs
   */
  def guessRelation(docs:Seq[Document]) = {
    for(doc <- docs) {
      // first try, in each sentence, all predicted span pairs are in relation
      val subjSpans = doc.spansOfClass[NerSpan].filter(_.label.categoryValue == "subjective")
      val targSpans = doc.spansOfClass[NerSpan].filter(_.label.categoryValue == "target")
      for(subjective <- subjSpans) {
        for(target <- targSpans) {
          subjective.addNerSpanAsRelation(target)(null)
        }
      }
    }
  }

  // build subj and target dic
  def buildDictionaries(documents: Seq[Document]) = {
    // cleanup
    TargSubjSpanNER.modelContainer.bootstrappedLexicons.clear
    TargSubjSpanNER.modelContainer.externalLexicons.clear
    TargSubjSpanNER.modelContainer.bootstrappedLexicons += "target" -> new Dictionary("target")
    for(goldSpan: GoldNerSpan <- documents.map(_.spansOfClass[GoldNerSpan]).flatten; if (goldSpan.labelString == "target"))
      TargSubjSpanNER.modelContainer.bootstrappedLexicons("target") ++= goldSpan.phrase // adding full entry, tokenizing again
    println("Read "+TargSubjSpanNER.modelContainer.bootstrappedLexicons("target").size+" entries to target dictionary with "+TargSubjSpanNER.modelContainer.bootstrappedLexicons("target").tokenSize+" tokens.")
    TargSubjSpanNER.modelContainer.bootstrappedLexicons += "subjective" -> new Dictionary("subjective")
    for(goldSpan: GoldNerSpan <- documents.map(_.spansOfClass[GoldNerSpan]).flatten; if (goldSpan.labelString == "subjective"))
      TargSubjSpanNER.modelContainer.bootstrappedLexicons("subjective") ++= goldSpan.phrase // adding full entry, tokenizing again
    println("Read "+TargSubjSpanNER.modelContainer.bootstrappedLexicons("subjective").size+" entries to subjective dictionary with "+TargSubjSpanNER.modelContainer.bootstrappedLexicons("subjective").tokenSize+" tokens.")
    //lexicons.foreach(_._2.cleanFromStopWords)
    // Jan Wiebe subjectivity dictionary
    if (TSProp.dictionaries) {
      val wiebeSubjectivity = new Dictionary("wiebe-subjectivity")
      wiebeSubjectivity.readFromJanWiebeFile("data/dic/subjclueslen1-HLTEMNLP05.tff")
      TargSubjSpanNER.modelContainer.externalLexicons += "wiebe-subjectivity" -> wiebeSubjectivity
    }
  }

  def classificationTraining(trainer:SampleRankTrainer[Sentence],iterations:Int, trainDocuments:Seq[Document], testDocuments:Seq[Document], predictor:SubjectivityPredictor) = {
    val trainSentences = trainDocuments.map(_.sentences).flatten
    val testSentences = testDocuments.map(_.sentences).flatten
    val trainLabels = trainSentences.map(_.subjective)
    val testLabels = testSentences.map(_.subjective)
    (trainLabels ++ testLabels).foreach(_.setRandomly(random))
    for (i <- 1 to iterations) {
      println("Iteration "+i)
      var s = Platform.currentTime
      trainer.processContexts(trainSentences)
      println("Done in "+((Platform.currentTime-s)/1000)+"s")
      println("Predictor processes all test documents...")
      s = Platform.currentTime
      predictor.processAll(testSentences)
      println("Done in "+((Platform.currentTime-s)/1000)+"s")
      println ("Train accuracy = "+ TargSubjSpanNER.subjectivityObjective.accuracy(trainLabels))
      println ("Test  accuracy = "+ TargSubjSpanNER.subjectivityObjective.accuracy(testLabels))
    }

  }

  /**
   * Really Start. This method makes the first interpretations of the ini file and triggers reading the data. It then starts the relevant subroutines based on the data read.
   * @return
   */
  def startTrainProcess = {
    val readRelations = TSProp.pipelineRelation || TSProp.guessrelation || TSProp.deepjointRelation
    if (TSProp.cv == "false") {
      // normal training, simple
      println("Started on train and test set!")
      val result = train(LoadTargetSubjectivity.fromFilename(TSProp.traincsv,true,false,readRelations,TSProp.take,TSProp.srl), LoadTargetSubjectivity.fromFilename(TSProp.testcsv,true,false,readRelations,TSProp.srl), Nil, TSProp.outcsv)
      println(" ======== FINAL RESULT =========")
      if (TSProp.testcsv != "false") result.bestResultByTest.print("")
      IOHelper.saveModel(modelContainer,TSProp.modelFileName)
    } else if (TSProp.cv == "10fold") {
      // to be able to parallelize everything; introduce a new parameter which is only evaluated if existent
      // 10 fold cv
      val docFolds = loadDocumentFolds(readRelations)
      val results = new ArrayBuffer[Results] // each Results contains all iterations for all subsets, this list contains all these iterations for all folds
      println("10 fold CV started!")
      for(i <- 0 until docFolds.size ; if (TSProp.onlyfold == -1 || TSProp.onlyfold == i)) {
        println(i+"th iteration of CV. Validation Set: "+((i+1)%10)+", Test Set: "+i)
        val valSet = docFolds(i)
        val testSet = docFolds((i+1)%9)
        val trainSet = new ArrayBuffer[Document]
        for(j <- 0 until docFolds.size ; if (j != i && j != ((i+1)%9))) trainSet ++= docFolds(j)
        results += train(trainSet.take(TSProp.takeFromTrain1fold),testSet,valSet,TSProp.outcsv.replaceAll(".csv","")+"-"+i+".csv")
        // CLEAN UP?
        (trainSet++testSet++valSet).foreach(_.clearAllNerSpans)
      }
      println(" ======== FINAL RESULTS ========")
      for(result <- results)
        result.bestResultByValidation.print("FOLD-"+results.indexOf(result))
    } else if (TSProp.cv == "1fold") {
      println("1/10 of a 10 fold CV started!")
      val docFolds = loadDocumentFolds(readRelations)
      val result = train(docFolds.slice(0,7).flatten.take(TSProp.takeFromTrain1fold), docFolds(9), docFolds(8),TSProp.outcsv)
      println(" ======== FINAL RESULT =========")
      result.bestResultByValidation.print("")
      IOHelper.saveModel(modelContainer,TSProp.modelFileName)
    } else {
      println("CV was not well specified! Exiting...")
      System.exit(1)
    }
  }

  /**
   * Start prediction.
   */
  def startPredictionProcess(inputfile:String,outputfile:String) = {
    System.out.println("Starting the prediction process...")
    val readRelations = TSProp.pipelineRelation || TSProp.guessrelation || TSProp.deepjointRelation
    if (TSProp.cv == "false" || TSProp.cv == "1fold") { // both could have lead to a model to be stored, handling in prediction not different.
      println("Started to predict on"+inputfile)
      //val result = train(LoadTargetSubjectivity.fromFilename(TSProp.traincsv,true,false,readRelations,TSProp.take,TSProp.srl), LoadTargetSubjectivity.fromFilename(TSProp.testcsv,true,false,readRelations,TSProp.srl), Nil, TSProp.outcsv)
      
      val result = predict(LoadTargetSubjectivity.fromFilename(inputfile,true,false,readRelations,TSProp.srl), outputfile)
    } else {
        System.out.println("Prediction based on 10 fold Cross Validation does not work (or some other parameter). Exiting.")
        System.exit(2)
    }
  }


  def loadDocumentFolds(readRelations:Boolean) : ArrayBuffer[Seq[Document]] = {
    // like 10 fold, but only one run
    // read all data, together
    val documents = if (TSProp.testcsv != "false" && TSProp.testcsv != "")
      LoadTargetSubjectivity.fromFilename(TSProp.traincsv,true,false,readRelations,TSProp.take) ++ LoadTargetSubjectivity.fromFilename(TSProp.testcsv,true,false,readRelations,TSProp.srl)
    else
      LoadTargetSubjectivity.fromFilename(TSProp.traincsv,true,false,readRelations,TSProp.take,TSProp.srl)
    // split in 10 parts
    val splitNum = documents.size / 10
    val documentFolds = new ArrayBuffer[Seq[Document]]
    for(i <- 0 until splitNum*10 by splitNum) {
      documentFolds += documents.slice(i,i+splitNum)
    }
    documentFolds
  }
}


/**
 * Start the whole procedure with the configuration from an ini-file.
 */
object TargSubjSpanNER extends TargSubjSpanNER {
  // The "main", examine the command line and do some work
  def main(args: Array[String]): Unit = {
    println("Parameters specified: "+args.mkString("\n"))
    if (args.length == 0) {
      println("Call with an ini file (1 parameter) or\nwith a model file, input data, output data (3 parameters)")
      System.exit(1)
    }
    val parameter = args(0)
    if (parameter.endsWith(".ini")) {
      println("Starting the training/testing phase.")
      println("Reading ini file from " + parameter)
      //    SentiWordNetDictionaryPriorPolarityChecker.readDictionary("data/dic/sentiwordnet3.txt")
      TargSubjSpanNER.training = true
      TSProp.read(args(0))
      // prepare model data structure, even of not saved later, this used in several places.
      modelContainer = new SerializableContainerModel(wilson = new WilsonDictionaryPriorPolarityChecker("data/dic/wilson2005.csv"))
    } else if (parameter.endsWith(".jfsa")) {
      println("reading model file from "+parameter)
      println("This is not yet implemented. Exiting.")
      if (args.length < 3) {
        println("When calling with a model file, three parameters are needed: a model file, input data, output data.")
        System.exit(1)
      }
      //System.exit(1)
      modelContainer = IOHelper.loadModel(parameter)
      // TODO finalize
    } else {
      println("The only parameters allowed are files ending with .ini or with .jfsa\nto specify a parameter file or a model file.")
    }

    TargSubjSpanNER.tokenizer = if (TSProp.stanfordParser) new StanfordTokenizer(true) else new ArkTweetTokenizer(true)
    if (TSProp.srl) TargSubjSpanNER.srl =  new SemanticRoleLabeler
    //PolarityDomain.freeze()

    if (training) {
      startTrainProcess

      // as a sanity check during development, directly load model again and test with training data, as done during training
//      if (TSProp.modelFileName != "false") {
//        println("\n\n\n\n\n\n\n\n\n\n==================================================================")
//        println("Only during development of this software, do reload of model and try it.")
//        startPredictionProcess(TSProp.traincsv, "false")
//      }
    }
    else {
      startPredictionProcess(args(1),args(2))
    }
  }
}
