/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.data

import cc.factorie._
import sc.rk.targsubj.{NerSpan, TSRelation, SubjectiveLabel}
import org.jgrapht.alg.FloydWarshallShortestPaths
import edu.stanford.nlp.trees.{GrammaticalRelation, TreeGraphNode}
import collection.mutable.ArrayBuffer

class Sentence(doc:Document, initialStart:Int, initialLength:Int)(implicit d:DiffList = null) extends TokenSpan(doc, initialStart, initialLength)(d) {
  def this(doc:Document)(implicit d:DiffList = null) = this(doc, doc.length, 0)
  //def tokens: IndexedSeq[Token] = links
  def tokenAtCharIndex(charOffset:Int): Token = {
    require(charOffset >= tokens.head.stringStart && charOffset <= tokens.last.stringEnd)
    var i = 0 // TODO Implement as faster binary search
    while (i < this.length && tokens(i).stringStart <= charOffset) {
      val token = tokens(i)
      //if (token.stringStart <= charOffset && token.stringEnd <= charOffset) return token
      if (token.stringStart <= charOffset && token.stringEnd >= charOffset) return token
      i += 1
    }
    return null
  }
  // subjectivity
  def setGoldSubjectivity(subjective: Boolean) = {
    attr += new SubjectiveLabel(this,subjective) // sets target and value?
    this.subjective.set(false)(null)
  }
  def subjective = attr[SubjectiveLabel]

  def contains(elem: Token) = tokens.contains(elem)

  // common labels
  def posLabels = tokens.map(_.posLabel)
  def nerLabels = tokens.map(_.nerLabel)

  // for feature construction
  var fw: FloydWarshallShortestPaths[TreeGraphNode,(GrammaticalRelation,Boolean,Int)] = null

  // all targets and subjectives
  def nerSpans = tokens.map(t => t.startsSpansOfClass[NerSpan]).flatten // was this. instead of tokens.
  def subjectiveSpans = nerSpans.filter(s => s.label.categoryValue == "subjective")
  def targetSpans = nerSpans.filter(s => s.label.categoryValue == "target")
  def relations = subjectiveSpans.map(_.relations).flatten
}
