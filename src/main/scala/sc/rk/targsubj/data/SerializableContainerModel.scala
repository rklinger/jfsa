package sc.rk.targsubj.data

import cc.factorie.Model
import sc.rk.targsubj._
import sc.rk.targsubj.tools.WilsonDictionaryPriorPolarityChecker

import scala.collection.mutable

/**
 * Created by rklinger on 3/18/15.
 */
class SerializableContainerModel (
  var jointModel:Model=null,
  var subjectivitySpanModel:Model=null,
  var targetSpanModel:Model=null,
  var subjectivityClassModel:Model=null,
  var relationModel:Model=null,
  var polarityModel:Model=null,
  val wilson:WilsonDictionaryPriorPolarityChecker=null
) extends Serializable {
  val properties = TSProp.properties
  val bootstrappedLexicons = new mutable.HashMap[String,Dictionary]
  val externalLexicons = new mutable.HashMap[String,Dictionary]
  // TODO check if I got all
  var relationFeaturesDomain = RelationFeaturesDomain.dimensionDomain
  var tSSpanFeaturesDomain = TSSpanFeaturesDomain.dimensionDomain
  var tSSpanFeaturesJointDomain = TSSpanFeaturesJointDomain.dimensionDomain
  var subjectiveClassificationFeaturesDomain = SubjectiveClassificationFeaturesDomain.dimensionDomain
  var polarityClassificationFeaturesDomain = PolarityClassificationFeaturesDomain.dimensionDomain
}
