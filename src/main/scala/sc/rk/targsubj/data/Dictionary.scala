/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.data

import collection.mutable
import io.Source
import cc.factorie.app.strings.Stopwords
import collection.mutable.ArrayBuffer
import sc.rk.targsubj.TargSubjSpanNER

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 18.02.13
 * Time: 19:36
 */
class Dictionary(name: String) extends Serializable {
  var originalEntries = new mutable.HashSet[String]
  var tokenizedEntries = new mutable.HashSet[String] // type to be changed, maybe...
  lazy val expr = "[^A-Za-z]".r

  def readFromFile(filename : String) = {
    val dicSource = Source.fromFile(new java.io.File(filename))
    for (line <- dicSource.getLines()) {
      originalEntries += line
      TargSubjSpanNER.tokenizer.tokenize(line).foreach(x => tokenizedEntries += preprocessString(x.tokenText))
    }
  }

  def preprocessString(in:String) : String = {
    expr.replaceAllIn(in,"")
  }

  def readFromJanWiebeFile(filename : String) = {
    val dicSource = Source.fromFile(new java.io.File(filename))
    for (line <- dicSource.getLines()) {
      val entries = line.split(' ')
      val word = entries(2).replaceAll("word1=","")
      //println(word)
      originalEntries += word
      tokenizedEntries += preprocessString(word)
    }
    cleanFromStopWords
  }

  def +=(token: String) = {
    tokenizedEntries += preprocessString(token)
  }

  def ++=(fullEntry: String) = {
    originalEntries += fullEntry
    TargSubjSpanNER.tokenizer.tokenize(fullEntry).foreach(x => tokenizedEntries += preprocessString(x.tokenText))
  }

  def size = originalEntries.size

  def tokenSize = tokenizedEntries.size

  def contains(tokenString : String) = tokenizedEntries.contains(preprocessString(tokenString))
  def containsExactly(string : String) = originalEntries.contains(preprocessString(string))

  def cleanFromStopWords = {
    var r = new ArrayBuffer[String]
    tokenizedEntries.foreach(x=> if (Stopwords.contains(x)) r+= x)
    r.foreach(x=>tokenizedEntries.remove(x))
  }
}

object Dictionary {
  def main(args: Array[String]) {
    val d = new Dictionary("tmp")
    d.readFromJanWiebeFile("data/dic/subjclueslen1-HLTEMNLP05.tff")
  }
}
