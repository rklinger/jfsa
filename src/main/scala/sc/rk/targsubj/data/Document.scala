/* Copyright (C) 2008-2010 University of Massachusetts Amherst,
   Department of Computer Science.
   This file is part of "FACTORIE" (Factor graphs, Imperative, Extensible)
   http://factorie.cs.umass.edu, http://code.google.com/p/factorie/
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License. */

/**
 * This file has been changed for the use in JFSA
 */

package sc.rk.targsubj.data

import cc.factorie._
import cc.factorie.util._
import scala.collection.mutable.ArrayBuffer
import sc.rk.targsubj.{TSRelation, GoldNerSpan, NerSpan}

/** Value is the sequence of tokens */
// Consider Document(strValue:String, name:String = "")
// TODO: Must all Documents have a name?  Alexandre thinks not.
//class Document(val name:String, strValue:String = "", val originalText:String) extends Chain[Document,Token] with DocumentSubstring with Attr {
  class Document(val name:String, strValue:String = "", val originalText:String) extends ChainWithSpansVar[Document,TokenSpan,Token] with Attr {
  // One of the following two is always null, the other non-null
  private var _string: String = strValue
  private var _stringbuf: StringBuffer = null
  /** Append the string 's' to this Document.
      @return the length of the Document's string before string 's' was appended. */
  def appendString(s:String): Int = {
    if (_stringbuf eq null) _stringbuf = new StringBuffer(_string)
    val result = _stringbuf.length
    _stringbuf.append(s)
    _string = null
    result
  }
  def string: String = {
    this.synchronized {
      if (_string eq null) _string = _stringbuf.toString
      _stringbuf = null
    }
    _string
  }
  def stringLength: Int = if (_string ne null) _string.length else _stringbuf.length
  
  /** Just a clearly-named alias for Chain.links. */
  def tokens: IndexedSeq[Token] = links
  
  // Managing Sentences
  private var _sentences = new ArrayBuffer[Sentence]
  def sentences: Seq[Sentence] = _sentences
  // potentially very slow for large documents. // TODO Why isn't this simply using token.sentence??  Or even better, just remove this method. -akm
  def sentenceContaining(token: Token): Sentence = sentences.find(_.contains(token)).getOrElse(null)

  // Managing Spans, keeping Sentence-spans separate from all other TokenSpans
  override def +=(s:TokenSpan): Unit = s match {
    case s:Sentence => {
      if (_sentences.length == 0 || _sentences.last.end < s.start) _sentences += s
      else throw new Error("Sentences must be added in order and not overlap.")
      s._chain = this // not already done in += be cause += is not on ChainWithSpans
    }
    case s:TokenSpan => super.+=(s)
  }
  override def -=(s:TokenSpan): Unit = s match {
    case s:Sentence => _sentences -= s
    case s:TokenSpan => super.-=(s)
  }
  
  def sgmlString: String = {
    val buf = new StringBuffer
    for (token <- tokens) {
      if (token.isSentenceStart) buf.append("<sentence>")
      token.startsSpansOfClass[GoldNerSpan].foreach(span => {buf.append("<"+span.name+">")})
      buf.append(token.string)
      token.endsSpansOfClass[GoldNerSpan].foreach(span => {buf.append("</"+span.name+">")})
      if (token.isSentenceEnd) buf.append("</sentence>")
      buf.append(" ")
    }
    buf.toString
  }
    // rk
  def clearNerSpans(doSubjectivity:Boolean,doTargets:Boolean): Unit = {
//      println("Removing subjectives: "+doSubjectivity+", and targets: "+doTargets)
      for(s <- spansOfClass[NerSpan] ;
          if (doSubjectivity && s.label.categoryValue == "subjective") ||
          (doTargets && s.label.categoryValue == "target")
      ) {
        s.delete(null)
      }
  }
  def clearRelations(): Unit = {
    for(s <- spansOfClass[NerSpan].filter(_.label.categoryValue == "subjective")) {
//      s.relations.foreach(x => println("CHECK:"+x))
      val toberemoved = new ArrayBuffer[TSRelation] ++ s.relations
      for(r <- toberemoved) {
        s.removeRelation(r)(null)
      }
      if (s.relations.size > 0) {
        println("SIZE: "+s.relations.size+" on "+s)
        s.relations.foreach(x => println(x))
      }
    }
  }

  def clearAllNerSpans : Unit = {
    for(s <- spansOfClass[NerSpan]) {
      s.delete(null)
    }
  }

  def subjective = sentences.exists(s => s.subjective.booleanValue)

  def tsRelations = this.spansOfClass[NerSpan].filter(_.label.categoryValue=="subjective").map(_.relations).flatten
  def goldTsRelations = this.spansOfClass[GoldNerSpan].filter(_.label.categoryValue=="subjective").map(_.relations).flatten
  def goldNerSpansSubjective = this.spansOfClass[GoldNerSpan].filter(_.label.categoryValue=="subjective")
  def goldNerSpansTarget = this.spansOfClass[GoldNerSpan].filter(_.label.categoryValue=="target")
  def relationsWithTarget(targetSpan:NerSpan) = tsRelations.filter(x=>x.targetNerSpan===targetSpan)
}

//trait DocumentProcessor {
//  // NOTE: this method may mutate and return the same document that was passed in
//  def process(d: Document): Document
//}


//// TODO Consider moving this to file util/Attr.scala
//trait AttrCubbieSlots extends Cubbie {
//  val storeHooks = new cc.factorie.util.Hooks1[Attr]
//  val fetchHooks = new cc.factorie.util.Hooks1[AnyRef]
//  def storeAttr(a:Attr): this.type = { storeHooks(a); this }
//  def fetchAttr(a:Attr): Attr = { fetchHooks(a); a }
//}
//
//trait DateAttrCubbieSlot extends AttrCubbieSlots {
//  val date = DateSlot("date")
//  storeHooks += ((a:Attr) => date := a.attr[java.util.Date])
//  //fetchHooks += ((a:Attr) => a.attr += date.value)
//  fetchHooks += { case a:Attr => a.attr += date.value }
//}


