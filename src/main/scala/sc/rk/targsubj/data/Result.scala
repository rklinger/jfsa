/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.data

import cc.factorie.TemplateModel
import sc.rk.targsubj._
import collection.mutable
import collection.mutable.ArrayBuffer

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 20.03.13
 * Time: 16:25
 */
object Result {
  def evalList(documents:Seq[Document], sep: String) : String = {
    evalToResult("",-1,documents).toString(sep)
  }

  def evalToResult(id:String,iteration:Int,documents:Seq[Document]) : Result = {
    if (documents == null || documents.isEmpty) null else {
      val (trueCountTargetExact,predictedCountTargetExact,correctCountTargetExact,precisionTargetExact,recallTargetExact,f1TargetExact) = eval(documents,"target",false,false)
      val (trueCountTargetPart,predictedCountTargetPart,correctCountTargetPart,precisionTargetPart,recallTargetPart,f1TargetPart) = eval(documents,"target",true,false)
      val (trueCountSubjExact,predictedCountSubjExact,correctCountSubjExact,precisionSubjExact,recallSubjExact,f1SubjExact) = eval(documents,"subjective",false,false)
      val (trueCountSubjPart,predictedCountSubjPart,correctCountSubjPart,precisionSubjPart,recallSubjPart,f1SubjPart) = eval(documents,"subjective",true,false)
      val (trueCountExact,predictedCountExact,correctCountExact,precisionExact,recallExact,f1Exact) = eval(documents,"",false,true)
      val (trueCountPart,predictedCountPart,correctCountPart,precisionPart,recallPart,f1Part) = eval(documents,"",true,true)
      val (tsreltrueCount,tsrelPredCount,tsrelCorrCount,tsRelPrec,tsRelRec,tsRelF) = evalTSRelation(documents,false)
      val (partialtsreltrueCount,partialtsrelPredCount,partialtsrelCorrCount,partialtsRelPrec,partialtsRelRec,partialtsRelF) = evalTSRelation(documents,true)
      val (polarityTrueCountPos,polarityPredCountPos,polarityCorrCountPos,polarityPrecPos,polarityRecPos,polarityFPos) = evalPolarity(documents,true,true) // This might be not enough!
      val (polarityTrueCountNeg,polarityPredCountNeg,polarityCorrCountNeg,polarityPrecNeg,polarityRecNeg,polarityFNeg) = evalPolarity(documents,true,false) // This might be not enough!
      val polarityAcc = evalPolarityAccuracy(documents,true)
      new Result(id,iteration,
        precisionTargetExact,recallTargetExact,f1TargetExact,
        precisionSubjExact,recallSubjExact,f1SubjExact,
        precisionTargetPart,recallTargetPart,f1TargetPart,
        precisionSubjPart,recallSubjPart,f1SubjPart,
        precisionExact,recallExact,f1Exact,
        precisionPart,recallPart,f1Part,
        TargSubjSpanNER.subjectivityObjective.accuracy(documents.map(_.sentences).flatten.map(_.subjective)),
        tsRelPrec,tsRelRec,tsRelF,
        partialtsRelPrec,partialtsRelRec,partialtsRelF,
        polarityPrecPos,polarityRecPos,polarityFPos,polarityPrecNeg,polarityRecNeg,polarityFNeg,polarityAcc
      )
    }
  }

  def eval(documents: Seq[Document], labelString: String, acceptPartialMatchAsTrue:Boolean,allLabels:Boolean) : (Int,Int,Int,Double,Double,Double) = {
    var trueCount = 0
    var predictedCount = 0
    var correctCount = 0
    for(document <- documents) {
      val (tpfp,tp,fpfn,p,r,f) = eval(document,labelString,acceptPartialMatchAsTrue,allLabels)
      trueCount += tpfp
      predictedCount += tp
      correctCount += fpfn
    }
    def precision = if (predictedCount == 0) 1.0 else correctCount.toDouble / predictedCount
    def recall = if (trueCount == 0) 1.0 else correctCount.toDouble / trueCount
    def f1 = if (recall+precision == 0.0) 0.0 else (2.0 * recall * precision) / (recall + precision)
    (trueCount,predictedCount,correctCount,precision,recall,f1)
  }

  def evalTSRelation(documents: Seq[Document],partial:Boolean) : (Int,Int,Int,Double,Double,Double) = {
    var trueCount = 0
    var predictedCount = 0
    var correctCount = 0
    for(document <- documents) {
      val (tpfp,tp,fpfn,p,r,f) = evalTSRelation(document,partial)
      trueCount += tpfp
      predictedCount += tp
      correctCount += fpfn
    }
    def precision = if (predictedCount == 0) 1.0 else correctCount.toDouble / predictedCount
    def recall = if (trueCount == 0) 1.0 else correctCount.toDouble / trueCount
    def f1 = if (recall+precision == 0.0) 0.0 else (2.0 * recall * precision) / (recall + precision)
    (trueCount,predictedCount,correctCount,precision,recall,f1)
  }

  /**
   * This is just: Given the spans, how much of them have correctly predicted polarity? It does not measure anything about not-predicted spans!
   * @param documents
   * @param acceptPartialMatchAsTrue
   * @return
   */
  def evalPolarityAccuracy(documents:Seq[Document],acceptPartialMatchAsTrue:Boolean) : Double = {
    val subjectiveSpans = documents.map(_.spansOfClass[NerSpan]).flatten.filter(s => s.label.categoryValue == "subjective")
    val correctPolarity = subjectiveSpans.filter(s => s.hasCorrectPolarity(acceptPartialMatchAsTrue)).size.toDouble
    val allSpans = subjectiveSpans.size.toDouble
    def acc = correctPolarity / allSpans
    acc
  }

  /**
   * Evaluation of polarity. For finding the right gold subjective spans, I am not sure if I should use partial matching...
   * @param documents
   * @return
   */
  def evalPolarity(documents:Seq[Document],acceptPartialMatchAsTrue:Boolean,checkPositivePolarity:Boolean) : (Double,Double,Double,Double,Double,Double) = {
    var trueCount = 0
    var predictedCount = 0
    var correctCount = 0
    for(document <- documents) {
      val (tpfp,tp,fpfn,p,r,f) = evalPolarity(document,acceptPartialMatchAsTrue,checkPositivePolarity)
      trueCount += tpfp
      predictedCount += tp
      correctCount += fpfn
    }
    def precision = if (predictedCount == 0) 1.0 else correctCount.toDouble / predictedCount
    def recall = if (trueCount == 0) 1.0 else correctCount.toDouble / trueCount
    def f1 = if (recall+precision == 0.0) 0.0 else (2.0 * recall * precision) / (recall + precision)

    // now return
    (trueCount,predictedCount,correctCount,precision,recall,f1)
  }


  /**
   * This is an evaluation like for subjective, but taking into account the class-polarity!
   * @param doc
   * @param acceptPartialMatchAsTrue
   * @param checkPositivePolarity
   * @return
   */
  def evalPolarity(doc:Document,acceptPartialMatchAsTrue:Boolean,checkPositivePolarity:Boolean) : (Int,Int,Int,Double,Double,Double) = {
    var trueCount = 0       // How many (eg) positive spans are correct, should have been predicted?   --> TP+FN
    var predictedCount = 0  // How many (eg) positive spans are predicted?                             --> TP+FP
    var correctCount = 0    // How many (eg) positive spans are predicted and correct?                 --> TP
    val usedGoldNerSpans = new mutable.HashSet[GoldNerSpan]()
    val spans = for(s:NerSpan <- doc.spansOfClass[NerSpan] ; if (s.label.shortCategoryValue == "subjective" && ((checkPositivePolarity && s.isPositive) || (!checkPositivePolarity && s.isNegative)))) yield s
    val goldSpans = for(s:GoldNerSpan <- doc.spansOfClass[GoldNerSpan] ; if (s.label.shortCategoryValue == "subjective")) yield s

    predictedCount += spans.filter((s:NerSpan) => ((checkPositivePolarity && s.isPositive) || (!checkPositivePolarity && s.isNegative))).length // TP+FP, pessimistic for finding a long gold as multiple parts, corrected there: yyy
    if (acceptPartialMatchAsTrue) {
      for(span:NerSpan <- spans) {
        val goldSpan = span.bestGoldSpanWithPolarityCheck
        if (goldSpan != null) {
          if (!usedGoldNerSpans.contains(goldSpan)) {
            correctCount += 1
            usedGoldNerSpans += goldSpan
          } else { // yyy, but not a big deal
            predictedCount -= 1
          }
        }
      }
    } else {
      spans.foreach((span:NerSpan) => if (span.isCorrect) correctCount += 1) // TP
    }
    trueCount += goldSpans.filter((s:GoldNerSpan) => ((checkPositivePolarity && s.isPositive) || (!checkPositivePolarity && s.isNegative))).length // TP+FN

    def precision = if (predictedCount == 0) 1.0 else correctCount.toDouble / predictedCount
    def recall = if (trueCount == 0) 1.0 else correctCount.toDouble / trueCount
    def f1 = if (recall+precision == 0.0) 0.0 else (2.0 * recall * precision) / (recall + precision)

    //    var trueCount = 0
//    var predictedCount = 0
//    var correctCount = 0
//    val spans = for(s:NerSpan <- document.spansOfClass[NerSpan]) yield s
//    val goldSpans = for(s:GoldNerSpan <- document.spansOfClass[GoldNerSpan]) yield s
//    predictedCount = spans.length // TODO FILTER FOR POS OR NEG? this is tp + fp!
//    spans.foreach(span => if (/*span.isCorrect && */span.hasCorrectPolarity(acceptPartialMatchAsTrue) && ((checkPositivePolarity && span.isPositive) || (!checkPositivePolarity && span.isNegative))) correctCount += 1) // TP
//                            /* ^^^^^^^^^^^^^^^ => I think this is in hasCorrectPolarity included */
//    trueCount = goldSpans.filter(s => ((checkPositivePolarity && s.isPositive) || (!checkPositivePolarity && s.isNegative))).length // TP+FN // TODO FILTER FOR POS OR NEG? this is tp+fn
//
//    def precision = if (predictedCount == 0) 1.0 else correctCount.toDouble / predictedCount
//    def recall = if (trueCount == 0) 1.0 else correctCount.toDouble / trueCount
//    def f1 = if (recall+precision == 0.0) 0.0 else (2.0 * recall * precision) / (recall + precision)
//
    // debug output
//    System.err.println("Gold Spans")
//    goldSpans.foreach(s => System.err.println(s+":"+s.polarity))
//    System.err.println("Pred Spans")
//    spans.foreach(s => System.err.println(s+":"+s.polarity))
    // debug output end


//    System.err.println(">>>"+(trueCount,predictedCount,correctCount,precision,recall,f1))
//
    (trueCount,predictedCount,correctCount,precision,recall,f1)
  }

  // return tp+fp, TP, TP+FN, p, r, f1
  def eval(doc:Document, labelString : String, acceptPartialMatchAsTrue: Boolean, allLabels: Boolean) : (Int,Int,Int,Double,Double,Double) = {
    var trueCount = 0
    var predictedCount = 0
    var correctCount = 0
    var alternativeCorrectCount = 0 // just for debugging
    val usedGoldNerSpans = new mutable.HashSet[GoldNerSpan]()
    val spans = for(s:NerSpan <- doc.spansOfClass[NerSpan] ; if (s.label.shortCategoryValue == labelString || allLabels)) yield s
    val goldSpans = for(s:GoldNerSpan <- doc.spansOfClass[GoldNerSpan] ; if (s.label.shortCategoryValue == labelString || allLabels)) yield s
    predictedCount += spans.length // TP+FP, pessimistic for finding a long gold as multiple parts, corrected there: yyy
    if (acceptPartialMatchAsTrue) {
      for(span <- spans) {
        val goldSpan = span.bestGoldSpan
        if (goldSpan != null) {
          if (!usedGoldNerSpans.contains(goldSpan)) {
            correctCount += 1
            usedGoldNerSpans += goldSpan
          } else { // yyy, but not a big deal
            predictedCount -= 1
          }
        }
      }
    } else {
//      spans.foreach(span => if (span.isCorrect) alternativeCorrectCount += 1) // TP // original, optimistic
      // with checking that each gold span is only used once
      for(span <- spans) {
        if (span.isCorrect) {
          val goldSpan = span.bestGoldSpan
          if (!usedGoldNerSpans.contains(goldSpan)) {
            correctCount += 1
            usedGoldNerSpans += span.bestGoldSpan
          } else {
            predictedCount -= 1
          }
        }
      }
//      println("DEBUGGING LEVEL RK: "+alternativeCorrectCount+" =?= "+correctCount)
    }

    trueCount += goldSpans.length // TP+FN

    def precision = if (predictedCount == 0) 1.0 else correctCount.toDouble / predictedCount
    def recall = if (trueCount == 0) 1.0 else correctCount.toDouble / trueCount
    def f1 = if (recall+precision == 0.0) 0.0 else (2.0 * recall * precision) / (recall + precision)

    // debug
//        if (labelString != "") {
//          println("---------------------------------------------------")
//          println(doc.string)
//          println("Gold targets for "+labelString+" accepting partial matches: "+acceptPartialMatchAsTrue)
//          doc.spansOfClass[GoldNerSpan].filter(x => x.label.categoryValue == labelString).foreach(println(_))
//          println("Predicted targets for "+labelString+" accepting partial matches: "+acceptPartialMatchAsTrue)
//          doc.spansOfClass[NerSpan].filter(x => x.label.categoryValue == labelString).foreach(println(_))
//          println("result: trueCount=%d  predictedCount=%d  correctCount=%d  p=%-2f  r=%-2f  f1=%-2f".format(trueCount,predictedCount,correctCount,precision,recall,f1))
//          println("---------------------------------------------------")
//        }

    (trueCount,predictedCount,correctCount,precision,recall,f1)
  }

  def evalTSRelation(doc:Document,partial:Boolean) : (Int,Int,Int,Double,Double,Double) = {
    // check true relations
    val set = new mutable.HashSet[GoldTSRelation] ++ doc.goldTsRelations
    var tp = 0
    var fp = 0
    var fn = 0
    for (predictedRelation <- doc.tsRelations) {
      val grOpt = if (partial)
        set.find(r => r =~= predictedRelation)
      else
        set.find(r => r === predictedRelation)
      if (grOpt.isDefined) {
        tp += 1
        set.remove(grOpt.get)
      }
      else
        fp += 1
    }
    fn += set.size
    val trueCount = doc.goldTsRelations.size
    val predictedCount = doc.tsRelations.size
    val correctCount = tp

    def precision = if (predictedCount == 0) 1.0 else correctCount.toDouble / predictedCount.toDouble
    def recall = if (trueCount == 0) 1.0 else correctCount.toDouble / trueCount.toDouble
    def f1 = if (recall+precision == 0.0) 0.0 else (2.0 * recall * precision) / (recall + precision)

    (trueCount,predictedCount,correctCount,precision,recall,f1)
  }
}

class Result(val id:String, val iteration:Int,
             precisionTargetExact:Double,recallTargetExact:Double,val f1TargetExact:Double,precisionSubjExact:Double,recallSubjExact:Double,val f1SubjExact:Double,
             precisionTargetPart:Double,recallTargetPart:Double,f1TargetPart:Double,precisionSubjPart:Double,recallSubjPart:Double,f1SubjPart:Double,
             precisionExact:Double,recallExact:Double,val f1Exact:Double,precisionPart:Double,recallPart:Double,f1Part:Double,
             subjAcc:Double,tsrelPrec:Double,tsrelRec:Double,val rstrelF:Double,partialtsRelPrec:Double,partialtsRelRec:Double,partialtsRelF:Double,
             polarityPrecPos:Double,polarityRecPos:Double,polarityFPos:Double,polarityPrecNeg:Double,polarityRecNeg:Double,polarityFNeg:Double,val polarityAcc:Double
              ) {
  def this() = this("EMPTY", 0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
  def toString(sep:String) : String = {
  "%1.3f %1.3f %1.3f  ".format(precisionTargetExact,recallTargetExact,f1TargetExact)+sep+
    " %1.3f %1.3f %1.3f  ".format(precisionSubjExact,recallSubjExact,f1SubjExact)+sep+
    " %1.3f %1.3f %1.3f  ".format(precisionTargetPart,recallTargetPart,f1TargetPart)+sep+
    " %1.3f %1.3f %1.3f  ".format(precisionSubjPart,recallSubjPart,f1SubjPart)+sep+
    " %1.3f %1.3f %1.3f  ".format(precisionExact,recallExact,f1Exact)+sep+
    " %1.3f %1.3f %1.3f  ".format(precisionPart,recallPart,f1Part)+sep+sep+
    " %1.3f  ".format(subjAcc)+sep+sep+
    " %1.3f %1.3f %1.3f  ".format(tsrelPrec,tsrelRec,rstrelF)+sep+
    " %1.3f %1.3f %1.3f  ".format(partialtsRelPrec,partialtsRelRec,partialtsRelF)+sep+
    " %1.3f %1.3f %1.3f ".format(polarityPrecPos,polarityRecPos,polarityFPos)+sep+
    " %1.3f %1.3f %1.3f ".format(polarityPrecNeg,polarityRecNeg,polarityFNeg)+sep+
    " %1.3f  ".format(polarityAcc)+sep+sep
  }
}

// containing all results for train, test, and if available, validation
// In addition, this is also holding the model structure itself, to be able to store that
class ResultSet(val trainResult: Result, val testResult: Result, val validationResult: Result) {
  def print(prefix:String) :String = {
    val str = prefix+"\tTRAIN\t"+trainResult.toString("|")+"\n"+
      prefix+"\tTEST \t"+testResult.toString("|")+"\n"+
      (if (validationResult != null) prefix+"\tVAL  \t"+validationResult.toString("|")+"\n" else "")
    println(str)
    str
  }
}

// overs different iterations, each iteration contains one result set
class Results {
  val results = new ArrayBuffer[ResultSet]()
  def +=(r:ResultSet) = {results += r}
  def bestResultByValidation : ResultSet = {
    if (results.isEmpty) {println("val case 1");new ResultSet(new Result(),new Result(),new Result())}
    if (TSProp.polarityExp) {println("val case 2");results.maxBy(result => result.validationResult.polarityAcc)}
    else if (TSProp.pipelineRelation || TSProp.deepjointRelation) {println("val case 3");results.maxBy(result => result.validationResult.rstrelF+result.validationResult.f1Exact)} // Added +result.validationResult.f1Exact // not sure about that
    else if (TSProp.perfectSubjectivity) {println("val case 4");results.maxBy(result => result.validationResult.f1TargetExact)}
    else if (TSProp.perfectTargets) {println("val case 5");results.maxBy(result => result.validationResult.f1SubjExact)}
    else {println("val case 6");results.maxBy(result => result.validationResult.f1Exact)}
  }
  def bestResultByTest : ResultSet = {
    if (results.isEmpty) {println("test case 1");new ResultSet(new Result(),new Result(),new Result())}
    if (TSProp.polarityExp) {println("test case 2");results.maxBy(result => result.testResult.polarityAcc)}
    else if (TSProp.pipelineRelation || TSProp.deepjointRelation) {println("test case 3");results.maxBy(result => (result.testResult.rstrelF + 0.1*result.testResult.f1TargetExact + 0.1*result.testResult.f1SubjExact))} // these multiplications are for the case that all relations are perfect---happens currently in the sentence-wise case, which is probably a bug, but unsolved
    else if (TSProp.perfectSubjectivity) {println("test case 4");results.maxBy(result => result.testResult.f1TargetExact)}
    else if (TSProp.perfectTargets) {println("test case 5");results.maxBy(result => result.testResult.f1SubjExact)}
    else {println("test case 6");results.maxBy(result => result.testResult.f1Exact)}
  }
}
