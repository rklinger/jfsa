/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

// This file is responsable for loading data. It is really quite terrible code, 
// as it internally builds up an IOB format first and then converts it into a span representation.
// Should be rewritten, but it is tested and seems to work.
package sc.rk.targsubj.data

import collection.mutable.{ArrayBuffer, ListBuffer, HashMap}
import java.io.{PrintWriter, File}
import sc.rk.targsubj._
import io.{Codec, Source}
import java.nio.charset.{CodingErrorAction, Charset}


object LoadTargetSubjectivity {
  // Recursively descend directory, returning a list of files.
  def files(directory:File): Seq[File] = {
    if (!directory.exists) throw new Error("File "+directory+" does not exist")
    if (directory.isFile) return List(directory)
    val result = new scala.collection.mutable.ArrayBuffer[File]
    for (entry <- directory.listFiles) {
      if (entry.isFile) result += entry
      else if (entry.isDirectory) result ++= files(entry)
    }
    result
  }

  def fromFilename(csvFilename:String,withPOS:Boolean,loadingForVisualization:Boolean,readRelations:Boolean): Seq[Document] =
    fromFilename(csvFilename,withPOS,loadingForVisualization,readRelations,999999,TSProp.srl)

  def fromFilename(csvFilename:String,withPOS:Boolean,loadingForVisualization:Boolean,readRelations:Boolean,take:Int): Seq[Document] =
    fromFilename(csvFilename,withPOS,loadingForVisualization,readRelations,TSProp.srl)

  def fromFilename(csvFilename:String,withPOS:Boolean,loadingForVisualization:Boolean,readRelations:Boolean,srl:Boolean): Seq[Document] =
    fromFilename(csvFilename,withPOS,loadingForVisualization,readRelations,999999,srl)

  def readText(txtFilename:String) : HashMap[String,(Boolean,String)] = {
//    val txtsource = Source.fromFile(new java.io.File(txtFilename),"UTF8")
    val codec = new Codec(Charset.forName("UTF-8"))
    codec.decoder.onMalformedInput(CodingErrorAction.IGNORE)
    val txtsource = Source.fromFile(new java.io.File(txtFilename))(codec)
    val allTxt : HashMap[String,(Boolean,String)] = new HashMap[String,(Boolean,String)]
    for (line <- txtsource.getLines()) {
//      println("LINE:"+line)
      val elements = line.trim.split("\t")
      val id = elements(0)
      val subjective = elements(1).toBoolean
      val docTxt = elements(2)
      //if (verbose) println(id+"#TAB#"+subjective+"#TAB#"+docTxt)
      allTxt.put(id,(subjective,docTxt))
    }
    System.err.println("Loaded "+allTxt.size+" documents.")
    allTxt
  }

  def readRelations(relFilename:String) : HashMap[String,ArrayBuffer[(String,String)]] = {
    val allRel : HashMap[String,ArrayBuffer[(String,String)]] = new HashMap[String,ArrayBuffer[(String,String)]]
    val codec = new Codec(Charset.forName("UTF-8"))
    codec.decoder.onMalformedInput(CodingErrorAction.IGNORE)
    val relFile = new java.io.File(relFilename)
    if (relFile.exists()) {
      val relsource = Source.fromFile(relFile)(codec)
      for (line <- relsource.getLines()) {
        val elements = line.trim.split("\t")
        if (elements.length < 4) {
          System.err.println("Line in relation file " + relFilename + " has not enough elements. Ignoring:\n" + line)
        } else {
          val relationClass = elements(0)
          val docId = elements(1)
          val entityId1 = elements(2)
          val entityId2 = elements(3)
          if (relationClass == "TARG-SUBJ") {
            if (!allRel.contains(docId)) allRel.put(docId, new ArrayBuffer[(String, String)]())
            allRel(docId) += {
              (entityId1, entityId2)
            }
          }
        }
    }
    }
    println("Loaded "+allRel.values.flatten.size+" relations.")
    allRel
  }

  def readCSV(csvFilename:String) : (HashMap[String,ArrayBuffer[(Int,Int,String,String)]],HashMap[String,ArrayBuffer[(Int,Int,String,String)]]) = {
    var allTargets = new HashMap[String,ArrayBuffer[(Int,Int,String,String)]]
    var allSubj = new HashMap[String,ArrayBuffer[(Int,Int,String,String)]]
    val codec = new Codec(Charset.forName("UTF-8"))
    codec.decoder.onMalformedInput(CodingErrorAction.IGNORE)
    val csvFile = new java.io.File(csvFilename)
    if (csvFile.exists()) {
      val csvsource = Source.fromFile(csvFile)(codec)
      for (line <- csvsource.getLines(); if (line.nonEmpty)) {
        //      System.err.println(line)
        val fields = line.split('\t')
        assert(fields.length >= 5)
        val entityType = fields(0)
        val docId = fields(1)
        val left = fields(2).toInt
        val right = fields(3).toInt
        val entityText = fields(4)
        val entityId = fields(5)
        val polarity = fields(6)
        //      println(entityText + " '''' "+allTxt(docId).substring(left,right))
        if (entityType == "target") {
          if (!allTargets.contains(docId)) allTargets.put(docId, new ArrayBuffer[(Int, Int, String, String)]())
          allTargets(docId) += {
            (left, right, entityId, polarity)
          }
        }
        else if (entityType == "subj") {
          if (!allSubj.contains(docId)) allSubj.put(docId, new ArrayBuffer[(Int, Int, String, String)]())
          allSubj(docId) += {
            (left, right, entityId, polarity)
          }
        }
      }
    }
    (allTargets,allSubj)
  }

// TODO this method is really in a bad shape. Should be reimplemented from scratch.
  def fromFilename(filenameWithExtension:String,withPOS:Boolean,loadingForVisualization:Boolean,doReadRelations:Boolean,take:Int,srl:Boolean): Seq[Document] = {
  import scala.collection.mutable.ArrayBuffer
  // initializations
  val documents = new ArrayBuffer[Document]
  if (filenameWithExtension != "false") {
    val filenameStub = filenameWithExtension.substring(0, filenameWithExtension.lastIndexOf('.'))
    System.err.println("Loading from " + filenameStub)
    val csvFilename = filenameStub + ".csv"

    if (csvFilename.isEmpty || csvFilename == "false") return documents
    var document: Document = null //new Document("TargetSubjectivity-"+documents.length, "")

    // get filenames
    val txtFilename = csvFilename.replaceFirst(".csv", ".txt")
    val relFilename = csvFilename.replaceFirst(".csv", ".rel")
    println("Reading CSV from " + csvFilename)
    println("Reading TXT from " + txtFilename)
    println("Reading REL from " + relFilename + ": " + doReadRelations)


    // read whole text into memory
    val allTxt = readText(txtFilename)

    // read all targ-subj relations into memory
    // doc id -> pair of entity ids
    val allRel = if (doReadRelations) readRelations(relFilename) else new HashMap[String, ArrayBuffer[(String, String)]]

    // read whole csv information into memory (left,right,entityId,polarity)
    val (allTargets, allSubj) = readCSV(csvFilename)

    ////////////////////////////////////
    // now build the documents with spans
    //documents += document
    var sentence: Sentence = null // = new Sentence(document)(null)
    for (docId <- allTxt.keys) {
      val (subjective, docTxt) = allTxt(docId)

      document = new Document("SubjTarget-" + docId, "", docTxt)
      documents += document

      val (sentences, tokens) = TargSubjSpanNER.tokenizer.sentenceSplitAndTokenize(docTxt, new File(txtFilename).getName + "-" + docId)
      //      sentences.foreach(x => println("SENTENCE: "+x))
      //      println("TOKENS: "+tokens.mkString("_"))
      var targetentityStartTokenOffet = -3
      var targetentityEndTokenOffset = -3
      var subjentityStartTokenOffet = -3
      var subjentityEndTokenOffset = -3
      var lastTargetEntityId = ""
      var lastSubjecEntityId = ""
      var lastSubjecPolarity = "Unknown" // FIXME it would be better to have that as "" -- THIS IS A BUG! Reimplement file reader!
      var tokenNum = 0

      for (tmptoken <- tokens) {
        //(tokenText,leftOffset,rightOffset,postag)
        if (sentences.size > 0 && sentences.head.leftOffsetInDocument <= tmptoken.leftOffset) {
          sentence = new Sentence(document)(null)
          sentence.setGoldSubjectivity(subjective) // can be overwritten later by subjetive offset annotation
          sentence.fw = sentences(0).fw
          sentences.remove(0)
        }

        // IOB and spans
        var nerLabelText = "O"
        if (allTargets.contains(docId)) {
          val (l, entityId, polarity) = getIOBLabelForToken(tmptoken.leftOffset, tmptoken.rightOffset, allTargets(docId), "-TAR", docTxt)
          nerLabelText = l
          if (l == "B-TAR") {
            if (targetentityStartTokenOffet != -3) {
              // add gold span
              //println("A Generating with "+targetentityStartTokenOffet+","+(targetentityEndTokenOffset-targetentityStartTokenOffet+1))
              GoldNerSpan.createNewGoldNerSpan(document, "target", targetentityStartTokenOffet, targetentityEndTokenOffset - targetentityStartTokenOffet + 1, lastTargetEntityId)
              //              new GoldNerSpan(document, "target", targetentityStartTokenOffet, targetentityEndTokenOffset-targetentityStartTokenOffet+1, lastTargetEntityId)(null)
              if (entityId == "") println("A " + entityId)
              //println("\tmake gold span A "+targetentityStartTokenOffet+","+targetentityEndTokenOffset)
            }
            targetentityStartTokenOffet = tokenNum
            lastTargetEntityId = entityId
            targetentityEndTokenOffset = tokenNum
          } else if (l == "I-TAR") {
            if (targetentityStartTokenOffet == -3) targetentityStartTokenOffet = tokenNum // just for the case I missed a beginning
            targetentityEndTokenOffset = tokenNum
          } else if (l == "O" || l == "B-SUBJ") {
            if (targetentityStartTokenOffet != -3) {
              //println("B Generating with "+targetentityStartTokenOffet+","+(targetentityEndTokenOffset-targetentityStartTokenOffet+1))
              GoldNerSpan.createNewGoldNerSpan(document, "target", targetentityStartTokenOffet, targetentityEndTokenOffset - targetentityStartTokenOffet + 1, lastTargetEntityId)
              //              new GoldNerSpan(document, "target", targetentityStartTokenOffet, targetentityEndTokenOffset-targetentityStartTokenOffet+1, lastTargetEntityId)(null)
              if (lastTargetEntityId == "") println("B " + lastTargetEntityId)
              //println("\tmake gold span B "+targetentityStartTokenOffet+","+targetentityEndTokenOffset)
            }
            targetentityStartTokenOffet = -3
            targetentityEndTokenOffset = -3
          }
        }
        if (allSubj.contains(docId)) {
          val (l, entityId, polarity) = getIOBLabelForToken(tmptoken.leftOffset, tmptoken.rightOffset, allSubj(docId), "-SUBJ", docTxt)
          if (nerLabelText == "O") nerLabelText = l
          if (l == "B-SUBJ") {
            if (subjentityStartTokenOffet != -3) {
              // add gold span
              //println("C Generating with "+subjentityStartTokenOffet+","+(subjentityEndTokenOffset-targetentityStartTokenOffet+1))
              GoldNerSpan.createNewGoldNerSpan(document, "subjective", subjentityStartTokenOffet, subjentityEndTokenOffset - subjentityStartTokenOffet + 1, lastSubjecEntityId, lastSubjecPolarity)
              //              new GoldNerSpan(document, "subjective", subjentityStartTokenOffet, subjentityEndTokenOffset-subjentityStartTokenOffet+1, lastSubjecEntityId, lastSubjecPolarity)(null)
              if (entityId == "") println("C " + entityId)
              //println("\tmake gold span C "+subjentityStartTokenOffet+","+subjentityEndTokenOffset)
            }
            subjentityStartTokenOffet = tokenNum
            lastSubjecEntityId = entityId
            lastSubjecPolarity = polarity
            subjentityEndTokenOffset = tokenNum
          } else if (l == "I-SUBJ") {
            if (subjentityStartTokenOffet == -3) subjentityStartTokenOffet = tokenNum // just for the case I missed a beginning
            subjentityEndTokenOffset = tokenNum
          } else if (l == "O" || l == "B-TAR") {
            if (subjentityStartTokenOffet != -3) {
              //println("D Generating with "+subjentityStartTokenOffet+","+(subjentityEndTokenOffset-subjentityStartTokenOffet+1))
              //println("Trying to use "+lastSubjecPolarity+" at \n"+document.tokens.filter(t => t.position == subjentityStartTokenOffet || t.position == (subjentityEndTokenOffset-subjentityStartTokenOffet+1)).map(_.string)+"\nin\n"+document.tokens.map(_.string))
              //              if (lastSubjecPolarity == "XXX") System.err.println("Got an XXX")
              val s = GoldNerSpan.createNewGoldNerSpan(document, "subjective", subjentityStartTokenOffet, subjentityEndTokenOffset - subjentityStartTokenOffet + 1, lastSubjecEntityId, lastSubjecPolarity)
              //              val s = new GoldNerSpan(document, "subjective", subjentityStartTokenOffet, subjentityEndTokenOffset-subjentityStartTokenOffet+1, lastSubjecEntityId, lastSubjecPolarity)(null)
              if (lastSubjecEntityId == "") println("D " + entityId + ": " + s.phrase)
              //println("\tmake gold span D "+subjentityStartTokenOffet+","+subjentityEndTokenOffset)
            }
            subjentityStartTokenOffet = -3
            subjentityEndTokenOffset = -3
          }
        }
        if (sentence.length > 0) document.appendString(" ")
        val token = new Token(sentence, tmptoken.tokenText)
        token.setPos(tmptoken.posTag)
        token.txtOffset = (tmptoken.leftOffset, tmptoken.rightOffset)
        token.node = tmptoken.node
        token.setSentence(sentence)
        tokenNum += 1
      } // end for tokens
      // add something at the end if necessary
      if (subjentityStartTokenOffet != -3) {
        //println("E Generating with "+subjentityStartTokenOffet+","+(subjentityEndTokenOffset-subjentityStartTokenOffet+1))
        GoldNerSpan.createNewGoldNerSpan(document, "subjective", subjentityStartTokenOffet, subjentityEndTokenOffset - subjentityStartTokenOffet + 1, lastSubjecEntityId, lastSubjecPolarity)
        //        new GoldNerSpan(document, "subjective", subjentityStartTokenOffet, subjentityEndTokenOffset-subjentityStartTokenOffet+1,lastSubjecEntityId, lastSubjecPolarity)(null)
        if (lastSubjecEntityId == "") println("E " + lastSubjecEntityId)
      }
      if (targetentityStartTokenOffet != -3) {
        //println("F Generating with "+targetentityStartTokenOffet+","+(targetentityEndTokenOffset-targetentityStartTokenOffet+1))
        GoldNerSpan.createNewGoldNerSpan(document, "target", targetentityStartTokenOffet, targetentityEndTokenOffset - targetentityStartTokenOffet + 1, lastTargetEntityId)
        //        new GoldNerSpan(document, "target", targetentityStartTokenOffet, targetentityEndTokenOffset-targetentityStartTokenOffet+1,lastTargetEntityId)(null)
        if (lastTargetEntityId == "") println("F " + lastTargetEntityId) // recently changed the if clause here, not sure if it actually should have been lastSubjecEntityId. I assume it was a copy/paste error
      }
      // set subjectivity class from entities if requested, for each SENTENCE separately
      if (TSProp.subjectivityClassFromEntities) {
        for (sentence <- document.sentences) {
          sentence.setGoldSubjectivity(sentence.tokens.exists(t => {
            t.spansOfClass[GoldNerSpan].exists(s => s.label.categoryValue == "subjective")
          }))
        }
      }
      if ((!TSProp.subjectivityFirst && TSProp.perfectSubjectivity) || loadingForVisualization) {
        perfectionize(document, "subjective", false)
      }
      if ((!TSProp.targetFirst && TSProp.perfectTargets) || loadingForVisualization) {
        perfectionize(document, "target", false)
      }
      // add relations
      if (allRel.contains(docId)) {
        for (relation <- allRel(docId)) {
          // get both spans
          val entity1: Option[GoldNerSpan] = document.spansOfClass[GoldNerSpan].find(s => {
            /*println(s.entityId +" == "+ relation._1) ; */ s.entityId == relation._1
          })
          val entity2: Option[GoldNerSpan] = document.spansOfClass[GoldNerSpan].find(s => {
            /*println(s.entityId +" == "+ relation._1) ; */ s.entityId == relation._2
          })
          if (entity1.isDefined && entity2.isDefined) {
            val subjectiveEntity = if (entity1.get.labelString == "subjective") entity1.get else entity2.get
            val targetEntity = if (entity1.get.labelString == "target") entity1.get else entity2.get
            subjectiveEntity.relations += new GoldTSRelation(subjectiveEntity, targetEntity)
          } else {
            // FIXME this is a bug!
            //            System.err.println("Problems reading file/relations.")
            //            System.err.println(entity1.isDefined+": "+relation._1)
            //            System.err.println(entity2.isDefined+": "+relation._2)
            //            if (entity1.isDefined) System.err.println(entity1.get.sentence.phrase)
            //            if (entity2.isDefined) System.err.println(entity2.get.sentence.phrase)
          }
        }
      }
    } // end for documents
    // sanity check for spans
    val spansToDelete = new ArrayBuffer[GoldNerSpan]
    //    System.err.println()
    for (d <- documents; (span: GoldNerSpan) <- d.spansOfClass[GoldNerSpan]) {
      //      System.err.println(span)
      //      System.err.println(span.head+":"+span.last)
      //      System.err.println(span.head.sentence+":"+span.last.sentence)
      //      System.err.println(span.head)
      //      System.err.println(span.last)
      if (span.head.sentence != span.last.sentence) {
        //        System.err.println("Spanning over sentence borders: "+span)
        spansToDelete += span
        // span.delete(null) // now externalized
      }
    }
    spansToDelete.foreach(_.delete(null))
    // sanity check end
    var spanCount = 0;
    for (d <- documents) spanCount += d.spansOfClass[GoldNerSpan].size
    //    var subjCount = 0 ; for(d <- documents) subjCount += d.spansOfClass[NerSpan].filter(_.label.categoryValue == "subjective").size
    //    var correctSubjCount = 0 ; for(d <- documents) correctSubjCount += d.spansOfClass[NerSpan].filter(a => a.label.categoryValue == "subjective" && a.isCorrect).size
    //    var targCount = 0 ; for(d <- documents) targCount += d.spansOfClass[NerSpan].filter(_.label.categoryValue == "target").size
    //    var subjGoldCount = 0 ; for(d <- documents) subjGoldCount += d.spansOfClass[GoldNerSpan].filter(_.label.categoryValue == "subjective").size
    //    var targGoldCount = 0 ; for(d <- documents) targGoldCount += d.spansOfClass[GoldNerSpan].filter(_.label.categoryValue == "target").size
    println("Loaded " + documents.length + " documents with " + documents.map(_.sentences.size).sum + " sentences with " + documents.map(_.length).sum + " tokens and " + spanCount + " gold spans total from file " + csvFilename)
    //    println("Gold targets: "+targGoldCount+" ("+targCount+")")
    //    println("Gold subject: "+subjGoldCount+" ("+subjCount+","+correctSubjCount+")")
    //    println(Result.evalList(documents,"|"))
    //    val h = new mutable.HashSet[String]()
    //    for(d <- documents ; t <- d) h += t.posTag
    //    h.foreach(println(_))
    // init with gold as suggested by Mike Wick
    //    if (initSpansWithGold) {
    //      // done above!
    //    }
    //    if (initRelationsWithGold) {
    //
    //    }
    //    System.err.println(">"+PolarityDomain.toList) ; System.exit(1)

    // do semantic role labeling
    if (srl) {
      System.err.println("Doing SRL: " + srl + " " + TargSubjSpanNER.srl)
      TargSubjSpanNER.srl.labelDocuments(documents)
    }

    System.err.println("Number of gold relations in loaded documents:      " + documents.map(d => d.goldTsRelations).flatten.size) // always zero... TODO FIX BUG
    System.err.println("Number of predicted relations in loaded documents:      " + documents.map(d => d.goldTsRelations).flatten.size) // always zero... TODO FIX BUG
  }
  // return
  if (take > documents.size)
    documents
  else
    documents.take(take)
}

  def perfectionize(documents:Seq[Document],labelString:String,doPolarity:Boolean) : Unit = {
    System.err.println("Perfectionizing spans for "+documents.size+" documents.")
    documents.foreach((x:Document) => perfectionize(x,labelString,doPolarity))
  }

  /*
   * labelString can be "all"
   */
  def perfectionize(document:Document,labelString:String,doPolarity:Boolean) = {
    val goldSpansToUse = if (labelString == "all") document.spansOfClass[GoldNerSpan] else document.spansOfClass[GoldNerSpan].filter(s => s.label.categoryValue == labelString)
    for(goldSpan <- goldSpansToUse) {
      val newOne = new NerSpan(document, goldSpan.label.categoryValue, goldSpan.head.position, goldSpan.length)(null)
      if (doPolarity) {
        // as an experiment:
        //newOne.polarity.set(PolarityDomain.value("Negative"))(null)
        newOne.polarity.set(goldSpan.polarity.value)(null)
      }
    }
  }

  def perfectionizeRelation(documents:Seq[Document]) : Unit = {System.err.println("Perfectionizing relations for "+documents.size+" documents.") ; documents.foreach(x => perfectionizeRelation(x))}
  def perfectionizeRelation(document:Document) = {
    // assuming to have gold ner spans in already
    for(goldRelation <- document.goldTsRelations) {
      // is the target there already. if not add it
      val goldTarget = goldRelation.targetNerSpan
      val targetOption = document.sentences.map(s => s.targetSpans).flatten.find(target => target === goldTarget)
      val targetSpan = if (targetOption.isDefined) targetOption.get else new NerSpan(document, goldTarget.label.categoryValue, goldTarget.head.position, goldTarget.length)(null)
      // is the subjective there already? if not add it
      val goldSubjective = goldRelation.subjectiveNerSpan
      val subjectiveOption = document.sentences.map(s => s.subjectiveSpans).flatten.find(subjective => subjective === goldSubjective)
      val subjectiveSpan = if (subjectiveOption.isDefined) subjectiveOption.get else new NerSpan(document, goldSubjective.label.categoryValue, goldSubjective.head.position, goldSubjective.length)(null)
      // now actually add the relation
      subjectiveSpan.relations += new TSRelation(subjectiveSpan,targetSpan)(null)
    }
  }


  def getIOBLabelForToken(leftOffset : Int,rightOffset : Int, docMap : ArrayBuffer[(Int,Int,String,String)], prefix : String, text: String) : (String,String,String) = {
//    println(text)
//    println(prefix+"-CHECK "+text.substring(leftOffset,rightOffset)+": ("+leftOffset+","+rightOffset+")")
//    for(x <- docMap) {
//      println("\tcompare to "+": ("+x._1+","+x._2+")")
//    }
    val b = docMap.find(x => {x._1 == leftOffset || (text.charAt(x._1)==' ' && x._1 +1 == leftOffset)})
    val i = docMap.find(x => {leftOffset > x._1 && rightOffset <= x._2 && rightOffset < text.length}) // TODO check if good: && rightOffset < text.length
    val rv =
      if (b.isDefined) ("B"+prefix,b.get._3,b.get._4) // FIXME this fuzzy match is preliminary
      else if (i.isDefined) ("I"+prefix,i.get._3,i.get._4)
      else ("O","","")
//    if (rv._2 == "camera-002-075-target-41") println("GOTIT!")
    rv
  }

  def printthings(documents : Seq[Document]) {
    for(doc <- documents) {
      for(sentence <- doc.sentences) {
        for(token <- sentence.tokens) {
          val ner = token.nerLabel
          print(token + " -> " + ner.categoryValue + "\t");
        }
      }
      println(" ")
    }
  }



  def splitLabel(token : Token) : Array[String] = {
    if(token.nerLabel.categoryValue.contains("-"))
      token.nerLabel.categoryValue.split("-")
    else
      Array("", "O")
  }
}

object Localtest {
  def main(args:Array[String]) = {
    println("Loading from "+args(0))
    val csvFilename = args(0)
    val withPOS = true
    val loadingForVisualization = false
    val readRelations = true
    val d:Seq[Document] = LoadTargetSubjectivity.fromFilename(csvFilename,withPOS,loadingForVisualization,readRelations,99999,true)
    for(doc <- d) {
      println(doc.name)
      for(s <- doc.sentences) {
        println(s.string)
        val goldSpansOnSentence = for(token <- s.tokens ; goldSpan <- token.startsSpansOfClass[GoldNerSpan]) yield goldSpan
        for(goldSpan <- goldSpansOnSentence) {
          println("\t"+goldSpan)
          for(relation <- goldSpan.relations) {
            println("\t\t"+relation)
          }
        }
      }
      println
      println
      relStatPrint(d)
    }
  }
  
  def relStatPrint(d:Seq[Document]) = {
    val scounter = new HashMap[Int,Int]()
    val tcounter = new HashMap[Int,Int]()
    for(doc <- d.take(1) ; gs <- doc.spansOfClass[GoldNerSpan]){
      val c = gs.relations.size
      if (gs.labelString == "target") {
        if (!tcounter.contains(c)) tcounter += c -> 0
        tcounter += {c -> {tcounter(c)+1}}
      }
      if (gs.labelString == "subjective") {
        if (!scounter.contains(c)) scounter += c -> 0
        scounter += {c -> {scounter(c)+1}}
      }
    }
    println("Target relations:")
    tcounter.foreach(a => println(a._1+"\t->\t"+a._2))
    println("Subjective relations:")
    scounter.foreach(a => println(a._1+"\t->\t"+a._2))
  }
}

object PrintIOB {
  def main(args: Array[String]) = {
    println("Loading from " + args(0))
    println("Writing to" + args(1))
    val csvFilename = args(0)
    val outPrefix = args(1)
    loadAndPrintIOB(csvFilename,outPrefix)
  }

  def loadAndPrintIOB(filenameWithExtension:String,outputprefix:String) = {
    import scala.collection.mutable.ArrayBuffer
    // initializations
    val targetwriter = new PrintWriter(outputprefix+"-target.iob")
    val subjwriter = new PrintWriter(outputprefix+"-subj.iob")
    
    if (filenameWithExtension != "false") {
      val filenameStub = filenameWithExtension.substring(0, filenameWithExtension.lastIndexOf('.'))
      System.err.println("Loading from " + filenameStub)
      val csvFilename = filenameStub + ".csv"

      // get filenames
      val txtFilename = csvFilename.replaceFirst(".csv", ".txt")
      val relFilename = csvFilename.replaceFirst(".csv", ".rel")
      System.err.println("Reading CSV from " + csvFilename)
      System.err.println("Reading TXT from " + txtFilename)

      // read whole text into memory
      val allTxt = LoadTargetSubjectivity.readText(txtFilename)

      // read whole csv information into memory (left,right,entityId,polarity)
      val (allTargets, allSubj) = LoadTargetSubjectivity.readCSV(csvFilename)

     
      ////////////////////////////////////
      // now build the documents with spans
      //documents += document
      var sentence: Sentence = null // = new Sentence(document)(null)
      for (docId <- allTxt.keys) {
        val (subjective, docTxt) = allTxt(docId)
        val (sentences, tokens) = TargSubjSpanNER.tokenizer.sentenceSplitAndTokenize(docTxt, new File(txtFilename).getName + "-" + docId)
        var tokenNum = 0

        // print target-iob
        if (allTargets.contains(docId)) {
          for (tmptoken <- tokens) {
            if (allTargets.contains(docId)) {
              val tmplabel = LoadTargetSubjectivity.getIOBLabelForToken(tmptoken.leftOffset, tmptoken.rightOffset, allTargets(docId), "-TAR", docTxt)
              targetwriter.println(tmptoken.tokenText + "\t" + tmplabel._1)
            }
          }
          targetwriter.println()
        }


        // print subj-iob
        if (allSubj.contains(docId)) {
          for (tmptoken <- tokens) {
            if (allSubj.contains(docId)) {
              val tmplabel = LoadTargetSubjectivity.getIOBLabelForToken(tmptoken.leftOffset, tmptoken.rightOffset, allSubj(docId), "-SUBJ", docTxt)
              subjwriter.println(tmptoken.tokenText + "\t" + tmplabel._1)
            }
          }
          // empty line at the end of an entry
          subjwriter.println()
        }
      }
    }
    targetwriter.flush()
    targetwriter.close()
    subjwriter.flush()
    subjwriter.close()
  }
}