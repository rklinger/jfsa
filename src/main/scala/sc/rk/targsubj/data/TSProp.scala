/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */


package sc.rk.targsubj.data

import java.util.Properties
import java.io.{FileInputStream, BufferedInputStream}

import sc.rk.targsubj.TargSubjSpanNER

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 13.03.13
 * Time: 12:01
 */

// This file handles the ini-file.

object TSProp extends Serializable {
  var properties = new Properties

  // default values
  var traincsv = ""
  var testcsv = ""
  var outcsv = ""
  var treetmp = ""
  var take = 99999999 // new compared to ACL2013 version
  var takeFromTrain1fold = 99999999
  var cv = "false"
  var onlyfold = -1
  var saveWeightsToFile = "false"
  var initSpansWithGold = false
  var initRelationsWithGold = false
  var perfectSubjectivity = false
  var subjectivityFirst = false
  var perfectTargets = false
  var targetFirst = false
  var justjoint = false // new compared to ACL2013 version
  var guessrelation = false // new compared to ACL2013 version
  var polarityExp = false
  var doPolarity = false
  var srl = false
  var forwardSampleRank = false
  var deepjointRelation = false // new compared to ACL2013 version
  var pipelineRelation = false // new compared to ACL2013 version
  var sentencesplitting = false
  var stanfordParser = false
  var lang = "en"
  var randomizeTokens = false
  var subjectivityClassFromEntities = false
  var iobwordfeature = true
  var prevwindow = true
  var prefix3 = true
  var suffix3 = true
  var dictionaries = false
  var offsetConjunction = 0
  var jointTargetSubjectivity = true // adds additional templates and changes the order of training
  var distancejoint = false
  var directjoint = false
  var booleanjoint = false
  var phrasebetweenjoint = false
  var wordsbetweenjoint = false
  var parsingShortestEdgePath = false
  var parsingOneEdge = false
  var onlyImportantEdges = false
  var identifyNounCloseToSubjective = false
  var simpleTokenBasedFeatureOnlyWithJoint = false
  var mergeJointIdentifierToOne = false
  var jointClassificationSpanPrediction = false // adds additional templates and changes the order of training
  var classificationtrainiterations = 12
  var jointClassificationtrainiterations = 2
  var trainiterations = 30
  var removeSpansEvery = 5
  var predictionIterations = 5 // no removal of spans!
  var iterationNumBeforePredict = 5 // influences joint classification and prediction on test data
  var modelFileName = "empty"

  var useRelationForSpanDetection = false // not in config file in the moment // new compared to ACL2013 version

  def storeProperties = {
    println(properties)
    traincsv = properties.getProperty("traincsv")
    testcsv = properties.getProperty("testcsv")
    outcsv = properties.getProperty("outcsv")
    treetmp = properties.getProperty("treetmp")
    take = properties.getProperty("take","999999").toInt
    takeFromTrain1fold = properties.getProperty("takeFromTrain1fold","999999").toInt
    cv = properties.getProperty("cv")
    onlyfold = properties.getProperty("onlyfold","-1").toInt
    saveWeightsToFile = properties.getProperty("saveWeightsToFile")

    initSpansWithGold = properties.getProperty("initSpansWithGold","false").toBoolean
    initRelationsWithGold = properties.getProperty("initRelationsWithGold","false").toBoolean
    perfectSubjectivity = properties.getProperty("perfectSubjectivity","false").toBoolean
    subjectivityFirst = properties.getProperty("subjectivityFirst","false").toBoolean
    perfectTargets = properties.getProperty("perfectTargets","false").toBoolean
    targetFirst = properties.getProperty("targetFirst","false").toBoolean
    justjoint = properties.getProperty("justjoint","false").toBoolean
    deepjointRelation = properties.getProperty("deepjointRelation","false").toBoolean
    pipelineRelation = properties.getProperty("pipelineRelation","false").toBoolean
    guessrelation = properties.getProperty("guessrelation","false").toBoolean
    polarityExp = properties.getProperty("polarityExp","false").toBoolean
    srl = properties.getProperty("srl","false").toBoolean
    doPolarity = properties.getProperty("doPolarity","false").toBoolean
    forwardSampleRank = properties.getProperty("forwardSampleRank","false").toBoolean

    sentencesplitting = properties.getProperty("sentencesplitting","false").toBoolean
    stanfordParser = properties.getProperty("stanfordParser","false").toBoolean
    lang = properties.getProperty("LANG","en")
    randomizeTokens = properties.getProperty("randomizeTokens","false").toBoolean
    subjectivityClassFromEntities = properties.getProperty("subjectivityClassFromEntities","false").toBoolean

    iobwordfeature = properties.getProperty("iobwordfeature").toBoolean
    prevwindow = properties.getProperty("prevwindow").toBoolean
    prefix3 = properties.getProperty("prefix3").toBoolean
    suffix3 = properties.getProperty("suffix3").toBoolean
    dictionaries = properties.getProperty("dictionaries").toBoolean
    offsetConjunction = properties.getProperty("offsetConjunction").toInt

    jointTargetSubjectivity = properties.getProperty("jointTargetSubjectivity").toBoolean
    distancejoint = properties.getProperty("distancejoint").toBoolean
    directjoint = properties.getProperty("directjoint").toBoolean
    booleanjoint = properties.getProperty("booleanjoint").toBoolean
    phrasebetweenjoint = properties.getProperty("phrasebetweenjoint").toBoolean
    wordsbetweenjoint = properties.getProperty("wordsbetweenjoint").toBoolean
    parsingShortestEdgePath = properties.getProperty("parsingShortestEdgePath").toBoolean
    parsingOneEdge = properties.getProperty("parsingOneEdge").toBoolean
    onlyImportantEdges = properties.getProperty("onlyImportantEdges").toBoolean
    identifyNounCloseToSubjective = properties.getProperty("identifyNounCloseToSubjective").toBoolean
    simpleTokenBasedFeatureOnlyWithJoint = properties.getProperty("simpleTokenBasedFeatureOnlyWithJoint").toBoolean
    mergeJointIdentifierToOne = properties.getProperty("mergeJointIdentifierToOne").toBoolean

    useRelationForSpanDetection =  properties.getProperty("useRelationForSpanDetection","false").toBoolean

    jointClassificationSpanPrediction = properties.getProperty("jointClassificationSpanPrediction").toBoolean
    classificationtrainiterations = properties.getProperty("classificationtrainiterations").toInt
    jointClassificationtrainiterations = properties.getProperty("jointClassificationtrainiterations").toInt
    trainiterations = properties.getProperty("trainiterations").toInt
    //    trainiterations = 5 // TMP!
    removeSpansEvery = properties.getProperty("removeSpansEvery").toInt
    predictionIterations = properties.getProperty("predictionIterations").toInt
    iterationNumBeforePredict = properties.getProperty("iterationNumBeforePredict").toInt
    modelFileName = properties.getProperty("modelFileName","empty")

    // SHORTCUT, not good, should be changed when a German dependency parser is included!
    if (lang == "de") {
      parsingOneEdge = false
      parsingShortestEdgePath = false
      stanfordParser = false
      //xxx
    }

    // make clear
    if (initRelationsWithGold) initSpansWithGold = false


  }
  
  def read(propfilename: String) = {
    val stream = new BufferedInputStream(new FileInputStream(propfilename))
    properties.load(stream)
    storeProperties
  }
}

