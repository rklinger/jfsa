/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.data

import cc.factorie.{Var, Diff}

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 24.01.13
 * Time: 14:18
 */
case class FlagDiff(v: Var) extends Diff {
  def undo = {}
  def redo = {}
  def variable = v
}
