/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.data

import cc.factorie.{CategoricalDomain, Diff, Var, DiffList}

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 24.01.13
 * Time: 13:38
 */

object AspectDomain extends CategoricalDomain[String]

class Aspect(var id: String, val clue: TokenSpan)(implicit d: DiffList) extends EntityRK(clue: TokenSpan) with Var {

  // Notify about the change
  if (d != null) {
    val diff = new NewAspectDiff
    diff.redo
    d += diff
    d += FlagDiff(clue.sentence)
  }

  def value = "" // FIXME do better
  def domain = AspectDomain // FIXME do better

  // shortcuts
  override def toString(): String = {id + ":" + clue.start + ":" + clue.length + ":" /*+ clue.map(_.string).mkString(" ")*/}
  def sentence = span.sentence
  var present = true

  def delete(implicit d: DiffList) = {
    val diff = new DeleteAspectDiff
    diff.redo
    if (d != null) d += diff
    if (d != null) d += new FlagDiff(clue.sentence)
  }

  case class FlagDiff(v: Var) extends Diff {
    def undo = {}

    def redo = {}

    def variable = v
  }

  case class DeleteAspectDiff() extends Diff {
    def undo = {
      present = true
    }

    def redo = {
      present = false
    }

    def variable = Aspect.this
  }

  case class NewAspectDiff() extends Diff {
    def undo = {
      present = false
    }

    def redo = {
      present = true
    }

    def variable = Aspect.this
  }
}
