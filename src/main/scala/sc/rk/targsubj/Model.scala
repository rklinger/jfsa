/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

// This file contains the main model structures and is therefore
// of central importance.

package sc.rk.targsubj

import cc.factorie._
import data._
import data.FlagDiff
import java.io.File
import scala.util.Random
import cc.factorie.util.{CubbieConversions, BinarySerializer}

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 04.02.13
 * Time: 15:44
 */

/**
 * The model which represents a relation between a subjective phrase and an aspect. This is a 
 * DotTemplate1 as we have one variable representing both parts.
 */
class TargSubjRelationModel extends TemplateModel with Parameters {
  self =>
  addTemplates(
    // think about bias term
    // Template for prediction of targets for subjective terms (direction: subj -> targ)
    new DotTemplate1[TSRelation] {
      println("maxSize = "+RelationFeaturesDomain.dimensionDomain.maxSize)
      val weights = Weights(new la.DenseTensor1(RelationFeaturesDomain.dimensionDomain.maxSize))
      override def unroll1(tsRelation:TSRelation) = if (tsRelation.present) Factor(tsRelation) else Nil

      override def statistics(v1:TSRelation#Value) = {
        val subjSeq = v1.asInstanceOf[Tuple2[_,_]]._1.asInstanceOf[NerSpan].toSeq // subjective phrase
        val tarSeq = v1.asInstanceOf[Tuple2[_,_]]._2.asInstanceOf[NerSpan].toSeq  // aspect
        val features1 = new RelationFeatures(subjSeq,tarSeq)                      // get features
        val returnvalue = features1.value
        returnvalue
      }
    }
  )
}

/**
 * This model is estimating three things: the polarity of a span, based on the span model, the relation model, and the polarity model itself.
 */
class SpanRelationPolarityModel extends TemplateModel with Parameters { // Serializable here not needed, it is in TemplateModel
  self =>
  {
    (new SpanNerModel).templates.foreach(addTemplates(_))
    (new TargSubjRelationModel).templates.foreach(addTemplates(_))
    if (TSProp.doPolarity) (new PolaritySpanRelationModel).templates.foreach(addTemplates(_))
  }
}

/**
 * The polarity model itself, it measures the polarity of a span. Features are span based, dependency-graph based, negation based, amongst others. *
 */
class PolaritySpanRelationModel extends TemplateModel with Parameters {
  self =>
  addTemplates(
    new DotTemplateWithStatistics1[Polarity] {
//      lazy val weights = new la.DenseTensor1(PolarityDomain.size)
      val weights = Weights(new la.DenseTensor1(PolarityDomain.size))
    },
    new DotTemplate2[NerSpan,Polarity] {
      val weights = Weights(new la.DenseTensor2(TSSpanFeaturesDomain.dimensionDomain.maxSize, PolarityDomain.size))
      def unroll1(span:NerSpan) = {Factor(span, span.polarity)}
      def unroll2(polarity:Polarity) = {Factor(polarity.span, polarity)}
      override def statistics(v1:NerSpan#Value, v2:Polarity#Value) = {
        val features = new PolarityClassificationFeatures(v1,v2) // v2 only for debugging gold features
        val returnvalue = features.value outer v2
        returnvalue
      }
    }
  )
  def this(file:File) = {
    this()
    BinarySerializer.deserialize(RelationFeaturesDomain, RelationFeaturesDomain, this, file)
  }
}

/**
 * This model measures if a subjective span or an aspect/target span is probable.*
 */
class SpanNerModel extends TemplateModel with Parameters {
  self =>
  addTemplates(
    // Bias term on each individual label
    new DotTemplateWithStatistics1[TSSpanNerLabel] {
      val weights = Weights(new la.DenseTensor1(TSNerDomain.size)) // dense because its only weights, not features
    },
    // Complex Features on the span itself, all joint with the label (of course)
    new DotTemplate2[NerSpan,TSSpanNerLabel] { // Growable and maxSize instead of dimensionSize
    val weights = Weights(new la.DenseTensor2(TSSpanFeaturesDomain.dimensionDomain.maxSize, TSNerDomain.size))
      def unroll1(span:NerSpan) = if (span.label.categoryValue == "subjective" || !TSProp.justjoint ) Factor(span, span.label) else Nil
      def unroll2(label:TSSpanNerLabel) = if (label.categoryValue == "subjective" || !TSProp.justjoint ) Factor(label.span, label) else Nil

      override def statistics(v1:NerSpan#Value, v2:TSSpanNerLabel#Value) = {
        val features = new TSSpanFeatures(v1.toSeq,v2.category,false,TSProp.useRelationForSpanDetection)
        val returnvalue = features.value outer v2
        returnvalue
      }
    },
    new DotTemplate2[NerSpan,TSSpanNerLabel] { // Growable and maxSize instead of dimensionSize
    val weights = Weights(new la.DenseTensor2(TSSpanFeaturesJointDomain.dimensionDomain.maxSize, TSNerDomain.size))
      def unroll1(span:NerSpan) = if (span.label.categoryValue == "target" || !TSProp.justjoint ) {
        span.head.startsSpansOfClass[NerSpan].head
        Factor(span, span.label)
      } else Nil
      def unroll2(label:TSSpanNerLabel) = if (label.categoryValue == "target" || !TSProp.justjoint ) {
        label.span.head.startsSpansOfClass[NerSpan].head
        Factor(label.span, label)
      } else Nil

      override def statistics(v1:NerSpan#Value, v2:TSSpanNerLabel#Value) = {
        val features = new TSSpanFeaturesJoint(v1.toSeq,v2.category)
        val returnvalue = features.value outer v2
        returnvalue
      }
    }
  )
  def this(file:File) = { this(); BinarySerializer.deserialize(this, file) }
}

/**
 * This model measures the subjectivity of a sentence, not a span. 
 * The idea was, that this might have some interactions with the predictions of subjectivities for spans. It does for some corpora,
 * but for product reviews, nearly every sentence is subjective.
 */
class SubjectivityModel extends TemplateModel with Parameters {
  self =>
  addTemplates(
    // Bias term on each individual label
    new DotTemplateWithStatistics1[SubjectiveLabel] {
      val weights = Weights(new la.DenseTensor1(2)) // should be boolean, actually the label domain size, which I did not explicitly define.
//      lazy val weights = new la.DenseTensor1(2) // should be boolean, actually the label domain size, which I did not explicitly define.
    },
    new DotTemplate2[Sentence,SubjectiveLabel] { // Growable and maxSize instead of dimensionSize
    val weights = Weights(new la.DenseTensor2(SubjectiveClassificationFeaturesDomain.dimensionDomain.maxSize, 2)) // second is boolean ;-)
//    lazy val weights = new la.DenseTensor2(SubjectiveClassificationFeaturesDomain.dimensionDomain.maxSize, 2) // second is boolean ;-)
    def unroll1(sentence:Sentence) = {println("COMING FROM SENTENCE, TYPICALLY NOT CALLED IN WORKFLOW, CHECK, IF YOU SEE THIS MESSAGE!") ; Factor(sentence, sentence.subjective)}
      def unroll2(subjective:SubjectiveLabel) = {/*println("COMING FROM SUBJ-LABEL") ; */Factor(subjective.sentence, subjective)}
      override def statistics(v1:Sentence#Value, v2:SubjectiveLabel#Value) = {
        val features = new SubjectiveClassificationFeatures(v1)
        val returnvalue = features.value outer v2
        returnvalue
      }
    }
  )
  def this(file:File) = { this(); BinarySerializer.deserialize(TSNerDomain, TSSpanFeaturesDomain, this, file) }
}

/**
 * Simple accuracy based objective for sentence based subjectivity prediction. *
 */
class SubjectivityObjective extends HammingTemplate[SubjectiveLabel]

/**
 * The objective of the polarity detection of a span corresponds to accuracy of the existance of a span with that polarity. The span is only measured by overlap.
 * Therefore, this method is measuring more than one thing.
 */
class PolarityObjective extends TemplateModel(
  // For each span
  new TupleTemplateWithStatistics2[NerSpan,Polarity] {
    def unroll1(span:NerSpan) = { if (span.label.categoryValue == "subjective") Factor(span, span.polarity) else Nil}
    def unroll2(polarity:Polarity) = { if (polarity.span.label.categoryValue == "subjective") Factor(polarity.span, polarity) else Nil}
    def score(spanValue:NerSpan#Value,polarityValue:Polarity#Value) = {
      // get exact gold span
      var result = 0.0
      val goldNerSpansOnDocument = spanValue.head.document.spansOfClass[GoldNerSpan].filter((s:GoldNerSpan) => s.label.categoryValue == "subjective")
      if (goldNerSpansOnDocument.exists((s:GoldNerSpan) => s =~= spanValue && s.polarity.value == polarityValue) ) result += 1 else result -= 1
      result
    }
  }
)

/**
 * One of the main training objectives, making use of the span and the label (subjective, aspect/target) of the span. *
 * The general idea is: More tokens fitting are better. Too many tokens are punished. *
 */
class SpanNerObjective extends TemplateModel(
  // For each span
  new TupleTemplateWithStatistics2[NerSpan,TSSpanNerLabel] {
    def unroll1(span:NerSpan) = { Factor(span, span.label)}
    def unroll2(label:TSSpanNerLabel) = { Factor(label.span, label) }
    def score(spanValue:NerSpan#Value,spanLabelValue:TSSpanNerLabel#Value) = {
      // preparations
      var result = 0.0
      var allTokensCorrect = true
      val goldNerSpansOnDocument = spanValue.head.document.spansOfClass[GoldNerSpan]
      val predictedSpansOnDocument = spanValue.head.document.spansOfClass[NerSpan]

      val correctSpanStart = goldNerSpansOnDocument.exists((sampledSpan:GoldNerSpan) => sampledSpan.head == spanValue.head && sampledSpan.label.value == spanLabelValue)
      val correctSpanEnd = goldNerSpansOnDocument.exists(sampledSpan => sampledSpan.last == spanValue.last && sampledSpan.label.value == spanLabelValue)
      val correctSpanStartAndEnd = goldNerSpansOnDocument.exists(sampledSpan => sampledSpan.head == spanValue.head && sampledSpan.last == spanValue.last && sampledSpan.label.value == spanLabelValue)

      var (numOfTrueTokens,maxGoldLength) = (NerSpan.maxNumOfSameTokens(spanValue,spanLabelValue,goldNerSpansOnDocument))
      var (numOfTrueSuccTokens,maxGoldLength2) = NerSpan.maxNumOfSameSucceedingTokens(spanValue,spanLabelValue,goldNerSpansOnDocument)
      var fractionOfTrueTokens = if (maxGoldLength == 0) 0 else numOfTrueTokens/maxGoldLength
      var fractionOfTrueSuccTokens = if (maxGoldLength == 0) 0 else numOfTrueSuccTokens/maxGoldLength
      //if (fractionOfTrueTokens > 0 || correctSpanStartAndEnd || correctSpanStart || correctSpanEnd) println("fraction "+fractionOfTrueTokens+"\t"+numOfTrueTokens+"\t"+maxGoldLength+"\t"+correctSpanStartAndEnd+"\t"+correctSpanStart+"\t"+correctSpanEnd+"\t")
      //result += fractionOfTrueTokens // also good ;-)
      result += fractionOfTrueSuccTokens
      if (maxGoldLength < spanValue.size && fractionOfTrueSuccTokens == 1) { // this should be weighted by number of superfluous tokens
        result -= 0.01
      }
      // new exp
      if (fractionOfTrueTokens == 0) result -= 0.01
      // end new exp
      // to have longer spans which are correct supported:
      //if (correctSpanStartAndEnd) result += 100.0
      //println("COMPARING "+spanValue+" ("+spanLabelValue+") to "+goldNerSpansOnDocument+" (num of gold spans="+goldNerSpansOnDocument.size+") "+correctSpanStart+","+correctSpanEnd+","+correctSpanStartAndEnd+" with result = "+result)
      result
    }
  }
)

/**
 * The relation subjective takes into account the same scores as the span detection, but combines them based on the existence of a relation. *
 */
class SpanRelationObjective extends TemplateModel(
  new TupleTemplateWithStatistics1[TSRelation] {
    //val r = new Random
    override def unroll1(tsRelation:TSRelation) = if (tsRelation.present) Factor(tsRelation) else Nil
    def score(tsRelation:TSRelation#Value) = {
      val subjSeq = tsRelation.asInstanceOf[Tuple2[_,_]]._1.asInstanceOf[NerSpan]
      val tarSeq = tsRelation.asInstanceOf[Tuple2[_,_]]._2.asInstanceOf[NerSpan]
      var result = 0.0
      val getAGold = subjSeq.head.document.goldTsRelations.find(goldRel => subjSeq.partialScore(goldRel.subjectiveNerSpan) > 0 && tarSeq.partialScore(goldRel.targetNerSpan) > 0) // TODO do max here!
      if (getAGold.isDefined) {
        val score = 0.5*subjSeq.partialScore(getAGold.get.subjectiveNerSpan) + 0.5*tarSeq.partialScore(getAGold.get.targetNerSpan)
        // mit CS xxx
        result = if (score < 0.99) score-1 else score
        //
        //result = score
      } else {
        result = -0.01
      }
//      result = 0
      result
    }
  },
  new TupleTemplateWithStatistics2[NerSpan,TSSpanNerLabel] {
    def unroll1(span:NerSpan) = { Factor(span, span.label)}
    def unroll2(label:TSSpanNerLabel) = { Factor(label.span, label) }
    def score(spanValue:NerSpan#Value,spanLabelValue:TSSpanNerLabel#Value) = {
      // preparations
      var result = 0.0
      var allTokensCorrect = true
      val goldNerSpansOnDocument = spanValue.head.document.spansOfClass[GoldNerSpan]
      val predictedSpansOnDocument = spanValue.head.document.spansOfClass[NerSpan]

      val correctSpanStart = goldNerSpansOnDocument.exists(sampledSpan => sampledSpan.head == spanValue.head && sampledSpan.label.value == spanLabelValue)
      val correctSpanEnd = goldNerSpansOnDocument.exists(sampledSpan => sampledSpan.last == spanValue.last && sampledSpan.label.value == spanLabelValue)
      val correctSpanStartAndEnd = goldNerSpansOnDocument.exists(sampledSpan => sampledSpan.head == spanValue.head && sampledSpan.last == spanValue.last && sampledSpan.label.value == spanLabelValue)

      var (numOfTrueTokens,maxGoldLength) = (NerSpan.maxNumOfSameTokens(spanValue,spanLabelValue,goldNerSpansOnDocument))
      var (numOfTrueSuccTokens,maxGoldLength2) = NerSpan.maxNumOfSameSucceedingTokens(spanValue,spanLabelValue,goldNerSpansOnDocument)
      var fractionOfTrueTokens = if (maxGoldLength == 0) 0 else numOfTrueTokens/maxGoldLength
      var fractionOfTrueSuccTokens = if (maxGoldLength == 0) 0 else numOfTrueSuccTokens/maxGoldLength
      result += 0.1*fractionOfTrueSuccTokens
      if (maxGoldLength < spanValue.size && fractionOfTrueSuccTokens == 1) {
        result -= 0.01
      }
      if (maxGoldLength == spanValue.size && fractionOfTrueSuccTokens==1)
        result += 1
      if (fractionOfTrueSuccTokens <= 0) result = -1
      result
    }
  },
  new TupleTemplateWithStatistics2[NerSpan,Polarity] {
    def unroll1(span:NerSpan) = { if (span.label.categoryValue == "subjective") Factor(span, span.polarity) else Nil}
    def unroll2(polarity:Polarity) = { if (polarity.span.label.categoryValue == "subjective") Factor(polarity.span, polarity) else Nil}
    def score(spanValue:NerSpan#Value,polarityValue:Polarity#Value) = {
      // get exact gold span
      var result = 0.0
      val goldNerSpansOnDocument = spanValue.head.document.spansOfClass[GoldNerSpan].filter((s:GoldNerSpan) => s.label.categoryValue == "subjective")
      if (goldNerSpansOnDocument.exists((s:GoldNerSpan) => s =~= spanValue && s.polarity.value == polarityValue) ) result += 0.1 else result -= 0.1
      result
    }
  }
)

/**
 * Relation objective *
 */
class RelationObjective extends TemplateModel(
  // For each span
  new TupleTemplateWithStatistics1[TSRelation] {
    //val r = new Random
    override def unroll1(tsRelation:TSRelation) = if (tsRelation.present) Factor(tsRelation) else Nil
    def score(tsRelation:TSRelation#Value) = {
      val subjSeq = tsRelation.asInstanceOf[Tuple2[_,_]]._1.asInstanceOf[NerSpan]
      val tarSeq = tsRelation.asInstanceOf[Tuple2[_,_]]._2.asInstanceOf[NerSpan]
      var result = 0.0
      if (subjSeq.head.document.goldTsRelations.exists((goldRel:GoldTSRelation) => goldRel.subjectiveNerSpan === subjSeq && goldRel.targetNerSpan === tarSeq)) {
        result += 1
      } else
      {
        result -= 1
      }
      result
    }
  }
)


/**
 * Proposes relations between subjective phrases and aspects. Has some hacks in it. *
 * @param model
 * @param objective
 * @param onPerfect
 * @param random
 */
class RelationSampler(model:Model, objective:Model, onPerfect:Boolean = false)(override implicit val random: scala.util.Random) extends SettingsSampler[NerSpan](model, objective) { // added (override implicit val random: scala.util.Random)
  this.proposalsHooks += {
    (proposals:Seq[cc.factorie.Proposal]) => {
      proposals.foreach((x:cc.factorie.Proposal) =>
        if (false) {
          System.err.println(
            "Diff="+x.diff+
              "; AccScore="+x.acceptanceScore+/*"; bfRatio=N/A"+()+*/
              "; ModScore="+x.modelScore+
              "; ObjScore="+x.objectiveScore/*+"; Temp=N/A"+()*/)
          //  System.err.println(proposals.mkString("\n"))
        })
    }}

  def settings(s:NerSpan) = {
    if (s.label.categoryValue == "target") {
      //      println("CALLED")
      new SettingIterator {
        //override def newDiffList = new DebugDiffList this won't compile
        val subjectiveSpansOnSameSentence = s.sentence.tokens.map(_.startsSpansOfClass[NerSpan]).flatten.filter(_.label.categoryValue=="subjective")
        //        println("\n\nExisting relations:")
        val existingRelationsWithThisTarget = subjectiveSpansOnSameSentence.map(_.relations).flatten.filter(x => x.targetNerSpan === s)
        //        existingRelationsWithThisTarget.foreach(x => println(x+"\t"+s.head.document.goldTsRelations.exists(g => g === x)))
        val changes = new scala.collection.mutable.ArrayBuffer[(DiffList)=>Unit]
        if (existingRelationsWithThisTarget.size < 3) { // doesn't make a big different, seems so, but may speed up
          for(subjectiveSpan <- subjectiveSpansOnSameSentence ; if (subjectiveSpan.relations.length < 4) ; if (!subjectiveSpan.relations.exists(x => subjectiveSpan === x.subjectiveNerSpan && s === x.targetNerSpan))) {
            changes += {(d:DiffList) => subjectiveSpan.addNerSpanAsRelation(s)(d)}
          }
        }
        for(er <- existingRelationsWithThisTarget) {
          changes += {(d:DiffList) => er.subjectiveNerSpan.removeRelation(er)(d)}
        }
        // do nothing
        changes += {(d:DiffList) => {}} // The no-op action
        changes += {(d:DiffList) => {}} // The no-op action // instead of doing that twice, only call the sampler on spans on which there is something to be proposed!
        var i = 0
        def hasNext = i < changes.length
        def next(d:DiffList) = { val d = new DiffList; changes(i).apply(d); i += 1; d }
        def reset = i = 0
      }
    }
    else { // not an aspect, do nothing
      new SettingIterator {
        //        println("\n\nExisting relations:")
        //        s.relations.foreach(x => println(x+"\t"+s.head.document.goldTsRelations.exists(g => g === x)))
        val changes = new scala.collection.mutable.ArrayBuffer[(DiffList)=>Unit]
        // for now, only propose the perfect one, debugging setting
        //          val targetSpansToBeProposed = s.document.spansOfClass[NerSpan].filter(_.label.categoryValue=="target")
        //          val targetSpansToBeProposed = s.sentence.map(_.startsSpansOfClass[NerSpan]).flatten.filter(_.label.categoryValue=="target")
        //          targetSpansToBeProposed.foreach(x => println("Target: "+x))
        //          if (s.relations.length < 10) {
        //            for(targetSpan <- targetSpansToBeProposed ; if (!s.relations.exists(x => targetSpan === x.targetNerSpan && s === x.subjectiveNerSpan))) {
        //              //          println("CAlled")
        //              changes += {(d:DiffList) => s.addNerSpanAsRelation(targetSpan)(d)}
        //              // remove each of the others and add this one
        //              for(relation <- s.relations) {
        //                changes += {(d:DiffList) => {
        //                  s.addNerSpanAsRelation(targetSpan)(d)
        //                  s.removeRelation(relation)(d)
        //                }}
        //              }
        //            }
        //          }
        // removal
        //for(relation <- s.relations) {
        //            changes += {(d:DiffList) => s.removeRelation(relation)(d)}
        //}
        // do nothing
        changes += {(d:DiffList) => {}} // The no-op action
        changes += {(d:DiffList) => {}} // The no-op action // instead of doing that twice, only call the sampler on spans on which there is something to be proposed!
        //System.err.println("Changes.length:"+changes.length)
        var i = 0
        def hasNext = i < changes.length
        def next(d:DiffList) = { val d = new DiffList; changes(i).apply(d); i += 1; d }
        def reset = i = 0
      }
    }
  }
}

/**
 * Propose the change of the polarity of a span. Should only be called on subjective phrases. *
 * @param model
 * @param objective
 * @param random
 */
class PolarityOnlySampler(model:Model, objective:Model)(override implicit val random: scala.util.Random) extends SettingsSampler[NerSpan](model, objective) {
  def settings(s:NerSpan) = new SettingIterator {
    assert (s.label.categoryValue == "subjective")
    val changes = new scala.collection.mutable.ArrayBuffer[(DiffList)=>Unit]
    for(polarity <- PolarityDomain
        //Na,;
//        if
//        (polarity.category == "Negative")
//          || (polarity.category == "Mixed")
//          || (polarity.category == "Neutral")
//          || (polarity.category == "Unknown")
//          || (polarity.category == "Positive")
    ) { // this commented stuff was just for debugging
//      System.err.println("POLARITY:"+polarity)
      changes += {(d:DiffList) => s.polarity.set(polarity)(d)}
    }
    var i = 0
    def hasNext = i < changes.length
    def next(d:DiffList) = { val d = new DiffList; changes(i).apply(d); i += 1; d }
    def reset = i = 0
  }
}

/**
 * Subjectivty sampler for sentences. Very simple. *
 * @param model
 * @param objective
 * @param random
 */
class SubjectivityClassificationSampler(model:Model, objective:Model)(override implicit val random: scala.util.Random) extends SettingsSampler[Sentence](model, objective) {
  def settings(sentence:Sentence) = new SettingIterator {
    val changes = new scala.collection.mutable.ArrayBuffer[(DiffList)=>Unit]
    changes += {(d:DiffList) => sentence.subjective.set(false)(d)}
    changes += {(d:DiffList) => sentence.subjective.set(true)(d)}
    var i = 0
    def hasNext = i < changes.length
    //    println("SAMPLER CALLED WITH "+changes.length+" PROPOSALS")
    def next(d:DiffList) = { val d = new DiffList; changes(i).apply(d); i += 1; d }
    def reset = i = 0
  }
  //  override def proposalsHook(proposals:Seq[Proposal]): Unit = {
  //    println("General (train/test) proposal")
  //    proposals.foreach(p =>
  //      println(p+"  "+(if (p.modelScore > 0.0) "NON-NULL" else "NULL")))
  //    println
  //    super.proposalsHook(proposals)
  //  }
}

/**
 * A joint sampler predicting span relations and polarities. This is one of the core samplers for the joint inference model. *
 * @param model
 * @param objective
 * @param compareToGoldAlways
 * @param random
 */
class SpanRelationPolaritySampler(model:Model, objective:Model,compareToGoldAlways:Boolean)(override implicit val random: scala.util.Random) extends SettingsSampler[Token](model, objective) {
  val debugProposals = false
  var numberOfProposals = 0
  var numberOfProposalChecks = 0
  def printNumberProposals = System.err.println("numberOfProposals\t"+numberOfProposals+"\t"+numberOfProposalChecks)
  def resetNumberOfProposals = {numberOfProposals=0;numberOfProposalChecks=0}
  this.proposalsHooks += {
    (proposals:Seq[cc.factorie.Proposal]) => {
      numberOfProposalChecks += 1
      proposals.foreach((x:cc.factorie.Proposal) => {
        numberOfProposals += 1
        if (debugProposals) // xxx
          System.err.println(
            "Diff="+x.diff+
              "; AccScore="+x.acceptanceScore+/*"; bfRatio=N/A"+()+*/
              "; ModScore="+x.modelScore+
              "; ObjScore="+x.objectiveScore/*+"; Temp=N/A"+()*/)
      }
      )
      //  System.err.println(proposals.mkString("\n"))
    }}

  override def pickProposal(proposals:Seq[Proposal]): Proposal = { // assume the last is the gold which is not necessarily taken into account
    val pickedProposal = proposals.take(if (compareToGoldAlways) proposals.size-1 else proposals.size).sampleExpProportionally((p:Proposal) => p.acceptanceScore / temperature)
//    println("Picked: "+proposals.indexOf(pickedProposal)+" from "+proposals.size)
//    val goldProposal = proposals.find((p:Proposal) => {p.diff.exists((d) => d.variable.isInstanceOf[GoldDenoter])})
//    if (goldProposal.isDefined) System.err.println("GOLD EXISTS: "+proposals.indexOf(goldProposal.get)+" of "+proposals.size)

//    if (pickedProposal.diff.variables.contains((v:Var) => v.isInstanceOf[GoldDenoter])) {System.err.println("PICKED GOLD!")}
    pickedProposal
  }

  // The proposer for changes to Spans touching this Token
  def settings(token:Token) = new SettingIterator {
    val changes = new scala.collection.mutable.ArrayBuffer[(DiffList)=>Unit]; // list of possible changes
    val existingSpans = token.spansOfClass[NerSpan](classOf[NerSpan])
    if (debugProposals) System.err.println("-----------------------------------------------------------------------------------")
    if (debugProposals) System.err.println("Proposal for "+token)
    for ((span:NerSpan) <- existingSpans) { // for each span on the token
      // Change label without changing boundaries
      for (labelValue <- TSNerDomain)  {
        // subjective label -> target label
        if (span.label.categoryValue == "subjective") { // relevant when being joint
          changes += {(d:DiffList) => {
            span.relations.foreach((r:TSRelation) => {
              span.removeRelation(r)(d)
            })
            span.label.set(labelValue)(d)
          }
          }
        }
        // target label -> subjective label
        if (span.label.categoryValue == "target") { // relevant when being joint
          changes += {(d:DiffList) => {
            span.deleteMeAsTargetFromAllRelations(d)
            span.label.set(labelValue)(d)
          }
          }
        }
      }
      // remove subjective span
      if (span.label.categoryValue == "subjective") {
        changes += {(d:DiffList) => {
          span.delete(d)
          span.removeAllRelations()(d)
        }}
      }
      // remove target span
      if (span.label.categoryValue == "target") {
        changes += {(d:DiffList) => {
          span.deleteMeAsTargetFromAllRelations(d)
          span.delete(d)
        }}
      }

      // shorten // removed for test
      if (span.length > 1) {
        // Trim last word, without changing label
        changes += {(d:DiffList) =>
          span.trimEnd(1)(d)
          (span.relations++span.document.relationsWithTarget(span)).foreach(r => d += new FlagDiff(r))
        }
        // Trim first word, without changing label
        changes += {(d:DiffList) =>
          span.trimStart(1)(d)
          (span.relations++span.document.relationsWithTarget(span)).foreach(r => d += new FlagDiff(r))
        }
      }
      // Add a new word to beginning, and change label
      if (span.canPrepend(1)) {
        changes += {(d:DiffList) => {
          span.prepend(1)(d)
          span.head.spansOfClass[NerSpan].filter(x => x != span).foreach(_.trimEnd(1)(d))
          (span.relations++span.document.relationsWithTarget(span)).foreach(r => d += new FlagDiff(r))
        }
        }
      }
      // Add a new word to the end, and change label
      if (span.canAppend(1)) {
        //for (labelValue <- TSNerDomain)
        if (debugProposals) System.err.println("Append for "+token+" as "+span)
        changes += {(d:DiffList) => {
          span.append(1)(d)
          span.last.spansOfClass[NerSpan].filter(x => x != span).foreach(_.trimStart(1)(d))
          (span.relations++span.document.relationsWithTarget(span)).foreach(r => d += new FlagDiff(r))
        }
        }
      }
      // Merge two neighboring spans having the same label // removed for test
      if (span.hasPredecessor(1)) {
        val prevSpans = span.predecessor(1).endsSpansOfClass[NerSpan]
        val prevSpan: NerSpan = if (prevSpans.size > 0) prevSpans.head else null
        if (prevSpan != null && prevSpan.label.intValue == span.label.intValue) {
          //System.err.println("Genering Diff "+prevSpan+" and "+prevSpan)
          changes += {(d:DiffList) => {
            span.prepend(prevSpan.length)(d)
            prevSpan.delete(d) // BACKIN
            prevSpan.deleteMeAsTargetFromAllRelations(d)
            prevSpan.removeAllRelations()(d)
            (span.relations++span.document.relationsWithTarget(span)).foreach(r => d += new FlagDiff(r))
          }}
        }
      }
      if (span.hasSuccessor(1)) {
        val nextSpans = span.successor(1).startsSpansOfClass[NerSpan]
        val nextSpan: NerSpan = if (nextSpans.size > 0) nextSpans.head else null
        if (nextSpan != null && nextSpan.label.intValue == span.label.intValue) {
          changes += {(d:DiffList) => {
            nextSpan.delete(d)
            nextSpan.deleteMeAsTargetFromAllRelations(d)
            nextSpan.removeAllRelations()(d)
            span.append(nextSpan.length)(d)
            (span.relations++span.document.relationsWithTarget(span)).foreach(r => d += new FlagDiff(r))
          }}
        }
      }
      // NEW!!!
      // TODO: Add FlagDiffs in common places!
      if (TSProp.doPolarity && span.label.categoryValue == "subjective") {
        for(polarity <- PolarityDomain)
          changes += {(d:DiffList) => span.polarity.set(polarity)(d)}
      }
    }
    changes += {(d:DiffList) => {}} // The no-op action
    if (existingSpans.isEmpty) {
      // attach each proposed target to existing subjectives
      if (true) { // switch
        for (labelValue <- TSNerDomain) {
          if (labelValue.category == "target") {
            changes += {(d:DiffList) => {
              val newSpan = new NerSpan(token.document, labelValue.category, token.position, 1)(d)
            }}
            for(subjectiveSpan <- token.sentence.subjectiveSpans) {
              changes += {(d:DiffList) => {
                val newSpan = new NerSpan(token.document, labelValue.category, token.position, 1)(d)
                subjectiveSpan.addNerSpanAsRelation(newSpan)(d)
              }}
            }
          }
          if (labelValue.category == "subjective") {
            changes += {(d:DiffList) => {
              val newSpan = new NerSpan(token.document, labelValue.category, token.position, 1)(d)
            }}
          }
        }
      } else { // TOO SLOW!
        // attach each proposed subj to existing targets
        for (labelValue <- TSNerDomain) {
          if (labelValue.category == "target") {
            changes += {(d:DiffList) => {
              val newSpan = new NerSpan(token.document, labelValue.category, token.position, 1)(d)
            }}
          }
          if (labelValue.category == "subjective") {
            changes += {(d:DiffList) => {
              val newSpan = new NerSpan(token.document, labelValue.category, token.position, 1)(d)
            }}
            for(targetSpan <- token.sentence.targetSpans) {
              changes += {(d:DiffList) => {
                val newSpan = new NerSpan(token.document, labelValue.category, token.position, 1)(d)
                newSpan.addNerSpanAsRelation(targetSpan)(d)
              }}
            }
          }
        }
      }
    }

    // if we do the perceptron-like compare-to-gold, add the gold:
    // actually, that needed to remove everything else...
    // this a debugging setting, typically fixed to false
    if (compareToGoldAlways) {
      changes += {(d:DiffList) => {
        token.startsSpansOfClass[GoldNerSpan].foreach((goldSpan:GoldNerSpan) => {
          val newSpan = new NerSpan(token.document, goldSpan.label.value.category, token.position, goldSpan.length)(d)
        })
        // now the spans are alright, let's deal with relations
        token.startsSpansOfClass[GoldNerSpan].filter(_.isSubjective).foreach((goldSpan:GoldNerSpan) => {
          goldSpan.relations.foreach((goldRelation:GoldTSRelation) => {
            val goldSubjSpan = goldRelation.subjectiveNerSpan
            val goldTargSpan = goldRelation.targetNerSpan
            // are they there? Otherwise propose them as well
            // TODO correct: for now, we just propose them always
            val newSubjSpan = new NerSpan(token.document, goldSubjSpan.label.value.category, goldSubjSpan.head.position, goldSubjSpan.length)(d)
            val newTargSpan = new NerSpan(token.document, goldTargSpan.label.value.category, goldTargSpan.head.position, goldTargSpan.length)(d)
            newSubjSpan.addNerSpanAsRelation(newTargSpan)(d)
          })
        })
        new GoldDenoter()(d)
      }}
    }
    if (debugProposals) System.err.println("-----------------------------------------------------------------------------------")
    var i = 0
    def hasNext = i < changes.length
    def next(d:DiffList) = { val d = new DiffList; changes(i).apply(d); i += 1; d }
    def reset = i = 0
  }
}


/**
 * Central sampler: Propose token spans for subjective and aspect phrases. *
 * @param model
 * @param objective
 * @param doSubjectiveSpans
 * @param doTargetSpans
 * @param allOperations
 * @param random
 */
class TokenSpanSampler(model:Model, objective:Model,doSubjectiveSpans:Boolean,doTargetSpans:Boolean,allOperations:Boolean
                        )(override implicit val random: scala.util.Random) extends SettingsSampler[Token](model, objective) {
  var numberOfProposals = 0
  var numberOfProposalChecks = 0
  def printNumberProposals = System.err.println("numberOfProposals\t"+numberOfProposals+"\t"+numberOfProposalChecks)
  def resetNumberOfProposals = {numberOfProposals=0;numberOfProposalChecks=0}
  this.proposalsHooks += {
    (proposals:Seq[cc.factorie.Proposal]) => {
      numberOfProposalChecks += 1
      proposals.foreach((x:cc.factorie.Proposal) => {
        numberOfProposals += 1
        if (false) // xxx
          System.err.println(
            "Diff="+x.diff+
              "; AccScore="+x.acceptanceScore+/*"; bfRatio=N/A"+()+*/
              "; ModScore="+x.modelScore+
              "; ObjScore="+x.objectiveScore/*+"; Temp=N/A"+()*/)
      }
      )
      //  System.err.println(proposals.mkString("\n"))
    }}

  def spanOfInterest(s:NerSpan) : Boolean = { (s.label.categoryValue == "subjective" && doSubjectiveSpans) || (s.label.categoryValue == "target" && doTargetSpans) }
  def labelOfInterest(l:String) : Boolean = { (l == "subjective" && doSubjectiveSpans) || (l == "target" && doTargetSpans) }

  // The proposer for changes to Spans touching this Token
  def settings(token:Token) = new SettingIterator {
    val changes = new scala.collection.mutable.ArrayBuffer[(DiffList)=>Unit]; // list of possible changes
    val existingSpans =
      token.spansOfClass[NerSpan](classOf[NerSpan]).filter(s => spanOfInterest(s))

    for (span <- existingSpans) { // for each span on the token
      // Change label without changing boundaries
      // new
      for (labelValue <- TSNerDomain  ; if (labelOfInterest(labelValue.category))) {
        if (span.label.categoryValue == "subjective") { // relevant when being joint
          changes += {(d:DiffList) => {
            span.removeAllRelations()(d)
            span.label.set(labelValue)(d)
          }
          }
        }
        if (span.label.categoryValue == "target") { // relevant when being joint
          changes += {(d:DiffList) => {
            span.deleteMeAsTargetFromAllRelations(d)
            span.label.set(labelValue)(d)
          }
          }
        }
      }

      // Delete the span !!!
      if (spanOfInterest(span)) {
        if (span.label.categoryValue == "subjective") {
          changes += {(d:DiffList) => {
            span.removeAllRelations()(d)
            span.delete(d)
          }}
        }
        if (span.label.categoryValue == "target") {
          changes += {(d:DiffList) => {
            span.deleteMeAsTargetFromAllRelations(d)
            span.delete(d)
          }}
        }
      }

      if (allOperations) {
        // shorten
        if (span.length > 1) {
          // Trim last word, without changing label
          changes += {(d:DiffList) => span.trimEnd(1)(d) ; span.relations.foreach(r => d += new FlagDiff(r))}
          // Trim first word, without changing label
          changes += {(d:DiffList) => span.trimStart(1)(d) ; span.relations.foreach(r => d += new FlagDiff(r))}
        }
        // Add a new word to beginning, and change label
        if (span.canPrepend(1)) {
          // for (labelValue <- TSNerDomain)
          changes += {(d:DiffList) => {
            span.prepend(1)(d)
            span.head.spansOfClass[NerSpan].filter(x => x != span && spanOfInterest(x)).foreach(_.trimEnd(1)(d))
            span.relations.foreach(r => d += new FlagDiff(r))
          }
          }
        }
        // Add a new word to the end, and change label
        if (span.canAppend(1)) {
          //for (labelValue <- TSNerDomain)
          changes += {(d:DiffList) => {
            span.append(1)(d)
            span.last.spansOfClass[NerSpan].filter(x => x != span && spanOfInterest(x)).foreach(_.trimStart(1)(d))
            span.relations.foreach(r => d += new FlagDiff(r)) // NOT SUFFICIENT?
          }
          }
        }
        // Merge two neighboring spans having the same label WAS IN, SHOULD BE IN AGAIN!
        if (span.hasPredecessor(1)) {
          val prevSpans = span.predecessor(1).endsSpansOfClass[NerSpan]
          val prevSpan: NerSpan = if (prevSpans.size > 0) prevSpans.head else null
          if (prevSpan != null && prevSpan.label.intValue == span.label.intValue) {
            changes += {(d:DiffList) => {
              val newSpan = new NerSpan(token.document, span.label.categoryValue, prevSpan.start, prevSpan.length + span.length)(d)
              span.delete(d)
              prevSpan.delete(d)
              if (span.label.categoryValue == "target") {
                val subjectiveSpansWithThisTarget = (span.head.sentence.relations++prevSpan.head.sentence.relations).filter(r => r.targetNerSpan == span).map(r => r.subjectiveNerSpan)
                span.deleteMeAsTargetFromAllRelations(d)
                prevSpan.deleteMeAsTargetFromAllRelations(d)
                subjectiveSpansWithThisTarget.foreach(s => s.addNerSpanAsRelation(newSpan)(d))
              }
              if (span.label.categoryValue == "subjective") {
                val targetsOfThisSubjectiveSpan = (span.head.sentence.relations++prevSpan.head.sentence.relations).filter(r => r.subjectiveNerSpan == span).map(r => r.targetNerSpan)
                span.removeAllRelations()(d)
                prevSpan.removeAllRelations()(d)
                targetsOfThisSubjectiveSpan.foreach(s => newSpan.addNerSpanAsRelation(s)(d))
              }
            }}
          }
        }
        if (span.hasSuccessor(1)) {
          val nextSpans = span.successor(1).startsSpansOfClass[NerSpan]
          val nextSpan: NerSpan = if (nextSpans.size > 0) nextSpans.head else null
          if (nextSpan != null && nextSpan.label.intValue == span.label.intValue) {
            changes += {(d:DiffList) => {
              val newSpan = new NerSpan(token.document, span.label.categoryValue, span.start, span.length + nextSpan.length)(d)
              span.delete(d)
              nextSpan.delete(d)
              if (span.label.categoryValue == "target") {
                val subjectiveSpansWithThisTarget = (span.head.sentence.relations++nextSpan.head.sentence.relations).filter(r => r.targetNerSpan == span).map(r => r.subjectiveNerSpan)
                span.deleteMeAsTargetFromAllRelations(d)
                nextSpan.deleteMeAsTargetFromAllRelations(d)
                subjectiveSpansWithThisTarget.foreach(s => s.addNerSpanAsRelation(newSpan)(d))
              }
              if (span.label.categoryValue == "subjective") {
                val targetsOfThisSubjectiveSpan = (span.head.sentence.relations++nextSpan.head.sentence.relations).filter(r => r.subjectiveNerSpan == span).map(r => r.targetNerSpan)
                span.removeAllRelations()(d)
                nextSpan.removeAllRelations()(d)
                targetsOfThisSubjectiveSpan.foreach(s => newSpan.addNerSpanAsRelation(s)(d))
              }
            }}
          }
        }
        // split off last position // REDUNDANT?
        if (span.length > 1) changes += {(d:DiffList) => {
          span.trimEnd(1)(d);
          new NerSpan(token.document, span.label.categoryValue, span.last.sentencePosition+1, 1)(d)
          span.relations.foreach(r => d += new FlagDiff(r))
        }
        }
      }
    }
    changes += {(d:DiffList) => {}} // The no-op action
    if (existingSpans.isEmpty) {
      for (labelValue <- TSNerDomain ; if (labelOfInterest(labelValue.category))) {
        // Add new length=1 span, for each label value
        changes += {(d:DiffList) => new NerSpan(token.document, labelValue.category, token.position, 1)(d)}
      }
    }
//    println("Token.settings length="+changes.length)
    var i = 0
    def hasNext = i < changes.length
    def next(d:DiffList) = { val d = new DiffList; changes(i).apply(d); i += 1; d }
    def reset = i = 0
  }
}

// The predictor for test data
class SpanNerPredictor(model:Model,doSubjective:Boolean,doTarget:Boolean,allOperations:Boolean)(override implicit val random: scala.util.Random) extends TokenSpanSampler(model, null,doSubjective,doTarget,allOperations) {
  def this(file:File,doSubjective:Boolean,doTarget:Boolean,allOperations:Boolean)(implicit random: scala.util.Random) = this(new SpanNerModel(file),doSubjective,doTarget,allOperations)
  var verbose = false
  //temperature = 0.0001
  //  override def preProcessHook(t:Token): Token = { // only do for capitalized tokens...
  //    super.preProcessHook(t)
  //    if (t.isCapitalized) {
  //      //if (verbose) t.spansOfClass(classOf[NerSpan]).foreach(s => println({if (s.isCorrect) "CORRECT " else "INCORRECT "}+s))
  //      t
  //    } else null.asInstanceOf[Token]
  //  }
  //  override def proposalsHook(proposals:Seq[Proposal]): Unit = {
  //    if (verbose && false) println("Test proposal")
  //    //proposals.foreach(println(_)); println
  //    if (verbose && false) { proposals.foreach(p => println(p+"  "+(if (p.modelScore > 0.0) "MM" else ""))); println }
  //    super.proposalsHook(proposals)
  //  }
  //  override def proposalHook(proposal:Proposal): Unit = {
  //    super.proposalHook(proposal)
  //    // If we changed the possible world last time, try sampling it again right away to see if we can make more changes
  //    // TODO Disabled for now, but this should be re-enabled
  //    if (false && proposal.diff.size > 0) {
  //      val spanDiffs = proposal.diff.filter(d => d.variable match { case s:NerSpan => s.present; case _ => false })
  //      spanDiffs.foreach(_.variable match {
  //        case span:NerSpan => if (span.present) { if (verbose) println("RECURSIVE PROPOSAL"); this.process(span.last) }
  //        case _ => {}
  //      })
  //    }
  //  }
}

class SpanRelationPolarityPredictor(model:SpanRelationPolarityModel)(implicit random: scala.util.Random) extends SpanRelationPolaritySampler(model,null,false) {
//  def this(file:File)(implicit random: scala.util.Random) = this(new SpanRelationPolarityModel(file))
  var verbose = false
}


class RelationPredictor(model:Model)(implicit random: scala.util.Random) extends RelationSampler(model, null) {
//  def this(file:File)(implicit random: scala.util.Random) = this(new TargSubjRelationModel(file))
  temperature = 0.0001
}

class SubjectivityPredictor(model:Model)(implicit random: scala.util.Random) extends SubjectivityClassificationSampler(model, null) {
  def this(file:File)(implicit random: scala.util.Random) = this(new SubjectivityModel(file))
  temperature = 0.0001
  //  override def proposalsHook(proposals:Seq[Proposal]): Unit = {
  //    println("Test proposal")
  //    proposals.foreach(p =>
  //      println(p+"  "+(if (p.modelScore > 0.0) "NON-NULL" else "NULL")))
  //    println
  //    super.proposalsHook(proposals)
  //  }
}

class PolarityPredictor(model:Model)(implicit random: scala.util.Random) extends PolarityOnlySampler(model, null) {
  def this(file:File)(implicit random: scala.util.Random) = this(new PolaritySpanRelationModel(file))
  temperature = 0.0001
  //  override def proposalsHook(proposals:Seq[Proposal]): Unit = {
  //    println("Test proposal")
  //    proposals.foreach(p =>
  //      println(p+"  "+(if (p.modelScore > 0.0) "NON-NULL" else "NULL")))
  //    println
  //    super.proposalsHook(proposals)
  //  }
}