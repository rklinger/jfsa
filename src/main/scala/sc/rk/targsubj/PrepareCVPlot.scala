/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj

import io.Source
import java.io.File
import collection.mutable.{ArrayBuffer, ListBuffer}

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 22.03.13
 * Time: 10:17
 */

// This file takes a log file from a 10fold-CV experiments and calculates the relevant measures.

object PrepareCVPlot {
  def main(args:Array[String]) = {
    for(filename <- args) {
      val file = new File(filename)
      val source = Source.fromFile(file)
      val name = file.getName.replaceAll(".log","").replaceAll(".ini","")
      val shortname = name.replaceAll("","").replaceAll("camera","c")
      val cvLines = source.getLines().filter(l => l.startsWith("FOLD") && l.contains("TEST")).map(l => l.replaceAll(",","."))
      //cvLines.foreach(println(_))
      val targetExactTripes = new ListBuffer[String]
      val subjExactTripes = new ListBuffer[String]
      val targetApproxTripes = new ListBuffer[String]
      val subjApproxTripes = new ListBuffer[String]
      val allExactTripes = new ListBuffer[String]
      val allApproxTripes = new ListBuffer[String]
      val allExactRelTripes = new ListBuffer[String]
      val allApproxRelTripes = new ListBuffer[String]
      for(line <- cvLines) {
        // part of the string with results of spans
        val a = line.substring(13,line.indexOf("||")-2)
        val b = a.split("\\|")
        targetExactTripes += b(0).trim
        subjExactTripes += b(1).trim
        targetApproxTripes += b(2).trim
        subjApproxTripes += b(3).trim
        allExactTripes += b(4).trim
        allApproxTripes += b(5).trim
        // relation results,
        // new version remove the last two || then it should work again, in old version just val lineTmp = line
        val lineTmp = line.substring(0,line.lastIndexOf("||"))
        val beginningOfRelationResults = lineTmp.lastIndexOf("||")
        val endOfRelationResults = lineTmp.lastIndexOf("|")
//        System.err.println("0>"+lineTmp)
//        System.err.println("1>"+beginningOfRelationResults)
//        System.err.println("2>"+endOfRelationResults)
        val relRes = lineTmp.substring(beginningOfRelationResults+2,endOfRelationResults-2).split("\\|").map(x => x.trim)
        allExactRelTripes += relRes(0)
        allApproxRelTripes += relRes(1)
      }
      val skipSD = true
      val te = prftripleToAverage(targetExactTripes,skipSD).map(_.formatted("%1.3f")).mkString("\t")
      val se = prftripleToAverage(subjExactTripes,skipSD).map(_.formatted("%1.3f")).mkString("\t")
      val ta = prftripleToAverage(targetApproxTripes,skipSD).map(_.formatted("%1.3f")).mkString("\t")
      val sa = prftripleToAverage(subjApproxTripes,skipSD).map(_.formatted("%1.3f")).mkString("\t")
      val ae = prftripleToAverage(allExactTripes,skipSD).map(_.formatted("%1.3f")).mkString("\t")
      val aa = prftripleToAverage(allApproxTripes,skipSD).map(_.formatted("%1.3f")).mkString("\t")
      val relE = prftripleToAverage(allExactRelTripes,skipSD).map(_.formatted("%1.3f")).mkString("\t")
      val relA = prftripleToAverage(allApproxRelTripes,skipSD).map(_.formatted("%1.3f")).mkString("\t")
      println("'%-10s'".format(shortname)+"\t"+te+"\t"+se+"\t"+ta+"\t"+sa+"\t"+ae+"\t"+aa+"\t"+relE+"\t"+relA)
      // println(name+"\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f\t%1.3f".format(p,r,f,pdev,rdev,fdev))
    }
  }

  def prftripleToAverage(triples : ListBuffer[String],skipSD:Boolean) : List[Double] = {
    // convert the strings to numbers
    val ps = new ArrayBuffer[Double]
    val rs = new ArrayBuffer[Double]
    val fs = new ArrayBuffer[Double]
    triples.foreach(t => {val a = t.split("[ \t]") ; ps += a(0).toDouble ; rs += a(1).toDouble ; fs += a(2).toDouble ; })
    val p = ps.sum / ps.length
    val r = rs.sum / rs.length
    val f = fs.sum / fs.length
    var psd = stddev(ps,p)
    var rsd = stddev(rs,r)
    var fsd = stddev(fs,f)
    if (skipSD) List(p,r,f) else List(p,r,f,psd,rsd,fsd)
  }

  def stddev(values:ArrayBuffer[Double],average:Double) : Double = {
    var sd = 0.0
    values.foreach(x => sd += scala.math.pow(x-average,2))
    sd = scala.math.sqrt(sd/values.length)
    sd
  }
}
