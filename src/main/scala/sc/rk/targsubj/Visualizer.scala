/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

// this is outdated, I think. The original idea was that this program takes a gold
// standard file and a file with predictions (the output) and visualizes both in HTML
// It might still work, but is untested. I (rk) did not use it for quite some time.

package sc.rk.targsubj

import cc.factorie.util.DefaultCmdOptions
import data.{TSProp, LoadTargetSubjectivity}
import tools.{ArkTweetTokenizer, StanfordTokenizer, Printer}

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 08.03.13
 * Time: 15:08
 */
class Visualizer {
  def visualize(goldFile:String, predictFile:String, outFile:String, targetcentric:Boolean, iniFile:String): Unit = {
//    println(Visualizer.targetcentric)
    val goldDocuments = LoadTargetSubjectivity.fromFilename(goldFile,iniFile.nonEmpty,true,false)
    val predictDocuments = LoadTargetSubjectivity.fromFilename(predictFile,iniFile.nonEmpty,true,false)
    println("Read "+goldDocuments.flatMap(_.sentences).size+" gold sentences, and "+predictDocuments.flatMap(_.sentences).size+" testing ")
    println("Have "+goldDocuments.map(_.length).sum+" trainTokens "+predictDocuments.map(_.length).sum+" testTokens")
    Printer.saveHTML(goldDocuments,predictDocuments,outFile,targetcentric, iniFile.nonEmpty)
  }
}

object Visualizer extends Visualizer {
  var targetcentric = false
  // The "main", examine the command line and do some work
  def main(args: Array[String]): Unit = {
    if (args.length < 4) {
      println("Call like visualize.sh --gold gold.csv [--predict predict.csv] --output camera.html [--targetcentric] [--ini fileForFeatures]")
      System.exit(1)
    }
    // Parse command-line
    object opts extends DefaultCmdOptions {
      val fileGold = new CmdOption("gold", "", "FILE", "My own file format file.")
      val filePredict = new CmdOption("predict", "", "FILE", "My own file format file.")
      val outFile = new CmdOption("output", "", "FILE", "HTML output")
      val iniFile = new CmdOption("ini", "", "FILE", "to show extracted features")
      val targetcentric =   new CmdOption("targetcentric", "Turn on target centric output") { override def invoke = Visualizer.targetcentric = true }
    }
    opts.parse(args)
    if (opts.iniFile.value.nonEmpty) {
      TSProp.read(opts.iniFile.value)
      TargSubjSpanNER.tokenizer = if (TSProp.stanfordParser) new StanfordTokenizer(true) else new ArkTweetTokenizer(true)
    }
    visualize(opts.fileGold.value, opts.filePredict.value, opts.outFile.value, targetcentric, opts.iniFile.value)
  }
}
