/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

// A tokenizer here is not only splitting tokens in a sentence, but also doing the sentence detection.
// Some of these tokenizers also provide parses. The Stanford tools are for instance doing all that together.
// Therefore, the choice of the tokenizer has an impact of the features which can be used!

package sc.rk.targsubj.tools

import sc.rk.targsubj.TargSubjSpanNER

import collection.mutable.{ListBuffer, ArrayBuffer}
import cmu.arktweetnlp.Tagger
import edu.stanford.nlp.trees.{TypedDependency, TreeGraphNode, Tree}
import sc.rk.targsubj.data.TSProp
import collection.mutable
import collection.immutable.{HashMap, HashSet}

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 23.01.13
 * Time: 17:59
 */

class TempToken(val tokenText: String, var leftOffset: Int, var rightOffset: Int, val posTag: String, val node:TreeGraphNode = null) {
  def addToOffset(value:Int) {
    leftOffset += value
    rightOffset += value
  }
  override def toString:String = leftOffset+":"+rightOffset+";"+tokenText+";"+posTag
}

trait Tokenizer {
  def tokenize(s:String) : Array[TempToken]
  def sentenceSplitAndTokenize(s:String,id:String) : (ArrayBuffer[TempSentence], Array[TempToken])
}

class SimpleTokenizer extends Tokenizer {
  def simpleTokenize(s:String) : Array[String]  = {
    import java.util.Scanner
    val scanner = new Scanner(s)
    var r = new ArrayBuffer[String];
    while (scanner.hasNext()) {
      scanner.findInLine("[a-zA-Z0-9]+|[^ ]") // or "[a-zA-Z0-9-]+|[^ ]"
      val result = scanner.`match`()
      r += result.group()
    }
    r.toArray
  }

  def tokenize(s:String) : Array[TempToken]  = {
    import java.util.Scanner
    //    System.err.println(s)
    var ss = s
    ss = ss.replaceAll("\u0085"," ")
    ss = ss.replaceAll("\t"," ")
    val scanner = new Scanner(ss)
    var leftOffset = 0
    var rightOffset = -2
    var r = new ArrayBuffer[TempToken];
    while (scanner.hasNext()) {
      scanner.findInLine("[a-zA-Z0-9]+|[^ ]") // or "[a-zA-Z0-9-]+|[^ ]"
      val result = scanner.`match`()
      var tokenText = result.group()
      //      System.err.println(tokenText)
      leftOffset = s.indexOf(tokenText,leftOffset)
      rightOffset = leftOffset + tokenText.length
      r += new TempToken(result.group(),leftOffset,rightOffset,"EMPTYPOS")
      leftOffset = rightOffset
    }
    //println(r)
    r.toArray
  }

  def sentenceSplitAndTokenize(s:String,id:String) : (ArrayBuffer[TempSentence], Array[TempToken]) = {
    var resultSentences = new ArrayBuffer[TempSentence]()
    if (TSProp.sentencesplitting)
      resultSentences = SentenceSplitter.split(s)
    else
      resultSentences += new TempSentence(s,0,s.length)
    (resultSentences,tokenize(s))
  }
}

// IN HERE, the tokenizer can only work on sentences!!!
class StanfordTokenizer(withPos:Boolean) extends Tokenizer {
  lazy val parser = new Parser(TSProp.lang) // only called in one case, typically not!

  def sentenceSplitAndTokenize(s:String,id:String) : (ArrayBuffer[TempSentence], Array[TempToken]) = {
    val sentences = SentenceSplitter.split(s)
    val allTokens = new ListBuffer[TempToken]
    for(n <- 0 until sentences.size) {
      val sentence = sentences(n)
      val tree = if (TargSubjSpanNER.training && Parser.cacheExistent(id+"-"+n)) {
//        println("Loading "+id+"-"+n)
        Parser.loadFromCache(id+"-"+n)
      } else if (sentence.sentenceTxt.filter(c => c == ' ').size < 180) { // threshold not to wait forever
//        println("Dealing with "+id+"-"+n+"--"+sentence.sentenceTxt.filter(c => c == ' ').size)
//        println
        val t = parser.parse(sentence.sentenceTxt)
        if (TargSubjSpanNER.training) parser.saveToCache(id+"-"+n,t)
        t
      } else //empty, unfortunately
        parser.emptyTree
      val sd = Parser.treeToSD(tree)
      val fw = Parser.computeFloydWarshall(sd)
      sentence.tree = tree
      sentence.fw = fw
      val tokens = tokenize(sentence.sentenceTxt,sentence.tree,sd)
      tokens.foreach(_.addToOffset(sentence.leftOffsetInDocument))
      allTokens ++= tokens
      //println(sentence)
      //println(tree)
      //tokens.foreach(x=>println("\t"+x))
    }
    (sentences,allTokens.toArray)
  }

  def tokenize(s: String) : Array[TempToken]  = {
    if (withPos)
      tokenizeWithBuildingParseTree(s)
    else
      simpleTokenize(s)
  }

  private def simpleTokenize(s: String) : Array[TempToken]  = {
    var ss = s
    ss = ss.replaceAll("\u0085"," ")
    ss = ss.replaceAll("\t"," ")
    val tokens = parser.tokenizeOnly(s)
    var leftOffset = 0
    var rightOffset = -2
    var r = new ArrayBuffer[TempToken]
    for(tokenText <- tokens) {
      leftOffset = s.indexOf(tokenText,leftOffset)
      rightOffset = leftOffset + tokenText.length
      r += new TempToken(tokenText,leftOffset,rightOffset,"EMPTYPOS")
      leftOffset = rightOffset
    }
    r.toArray
  }

  def cleanPString(s: String): String = {
    s.replaceAll("-LSB-", "[").
      replaceAll("-RSB-", "]").
      replaceAll("-LRB-", "(").
      replaceAll("-RRB-", ")").
      replaceAll("''", "\"")
  }

  // this one is to be used with stanford parse trees, better
  private def tokenize(s:String,tree:Tree,sd:List[TypedDependency]) : Array[TempToken] = {
    var ss = s
    ss = ss.replaceAll("\u0085"," ")
    ss = ss.replaceAll("\t"," ")
    val sfTokensAndTags = Parser.treeToTokensAndPOS(tree)
    val allGraphNodes = new mutable.HashMap[Int,TreeGraphNode]
    //println(Parser.treeToSD(tree))
    //Parser.treeToSD(tree).foreach(td => println(td.dep+" -- "+ td.gov + " ; "+td.dep.index +":"+td.gov.index))
    sd.foreach(td => {allGraphNodes += td.dep.index -> td.dep ; allGraphNodes += td.gov.index -> td.gov})
//    val tokens = parser.tokenizeOnly(s)
//    println(s)
//    println(sfTokensAndTags)
//    println(tokens)
//    println
    var leftOffset = 0
    var rightOffset = -2
    var r = new ArrayBuffer[TempToken]
    for(i <- 0 until sfTokensAndTags.size) {
      var tokenText = cleanPString(sfTokensAndTags(i)._1)
      leftOffset = s.indexOf(tokenText,leftOffset)
      rightOffset = leftOffset + tokenText.length
      val node = if (allGraphNodes.contains(i+1) && tokenText == allGraphNodes(i+1).label().toString.substring(0,allGraphNodes(i+1).label().toString.lastIndexOf('-'))) {
        allGraphNodes(i+1)
      } else null.asInstanceOf[TreeGraphNode]

      r += new TempToken(tokenText,leftOffset,rightOffset,sfTokensAndTags(i)._2,node)
      leftOffset = rightOffset
    }
    r.toArray
  }

  private def tokenizeWithBuildingParseTree(s:String) : Array[TempToken] = {
    val tree = parser.parse(s)
    val sd = Parser.treeToSD(tree)
    tokenize(s,tree,sd)
  }
}

/**
 * This guy is optimized for tweets. *
 * @param withPos
 */
class ArkTweetTokenizer(withPos: Boolean) extends Tokenizer {
  val tagger = new Tagger
  val modelFilename = "/cmu/arktweetnlp/model.20120919";
  tagger.loadModel(modelFilename)

  def tokenize(s:String) : Array[TempToken] = {
    var ss = s
    ss = ss.replaceAll("\u0085"," ")
    ss = ss.replaceAll("\t"," ")
    //ss = ss.replaceAll(".[^ ]]",". ")
    val tokensAndTags = tagger.tokenizeAndTag(ss)
    var leftOffset = 0
    var rightOffset = -2
    var r = new ArrayBuffer[TempToken]
    for(i <- 0 until tokensAndTags.size) {
      var tokenText = tokensAndTags.get(i).token
      leftOffset = s.indexOf(tokenText,leftOffset)
      rightOffset = leftOffset + tokenText.length
      val posTag = if (withPos) tokensAndTags.get(i).tag else "EMPTYPOS"
      r += new TempToken(tokenText,leftOffset,rightOffset,posTag)
      leftOffset = rightOffset
    }
    r.toArray
  }

  def sentenceSplitAndTokenize(s:String,id:String) : (ArrayBuffer[TempSentence], Array[TempToken]) = {
    var resultSentences = new ArrayBuffer[TempSentence]()
    if (TSProp.sentencesplitting)
      resultSentences = SentenceSplitter.split(s)
    else
      resultSentences += new TempSentence(s,0,s.length)
    (resultSentences,tokenize(s))
  }
}


//object Tokenizer {
//
//
//
//
//
//
//
//
//
//
//  // returns a list of sentence from the full text given and a list of tokens, this is both independent from each other, somehow!
//  // s should be the whole text of a document, including different sentences here!
//  def tokenizeWithOffset(s:String, withPOS: Boolean) : (ArrayBuffer[(String,Int,Int)],Array[(String,Int,Int,String)])  = {
//    val sentences = if (TSProp.sentencesplitting) SentenceSplitter.split(s) else ArrayBuffer((s,0,s.length))
//    val tokens = if (withPOS) tokenizeWithOffsetAndPOS(s) else tokenizeWithOffset(s) // probably THIS HERE is a good place to implement another method doing tokenization with stanford, but how to store the tree?
//    (sentences,tokens)
//  }
//}
