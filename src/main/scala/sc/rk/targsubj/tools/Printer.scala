/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.tools

import cc.factorie._
import cc.factorie.optimize._
import cc.factorie.util.DefaultCmdOptions
import la.DenseTensor2
import sc.rk.targsubj.data._
import java.io.{PrintWriter, File}
import compat.Platform
import sc.rk.targsubj._
import collection.mutable
import io.Source

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 04.02.13
 * Time: 16:04
 */
object Printer {
  def printIOB(documents : Seq[Document], filename:String) {
    val file = new File(filename)
    println("Printing IOB to "+file.getAbsolutePath)
    val writer = new PrintWriter(file)
    for(doc <- documents) {
      writer.write("### "+doc.name+"\n")
      for(sentence <- doc.sentences) {
        //println("# "+doc.name)
        for(token <- sentence.tokens) {
          writer.write(
            token.string + "\t" +
              //token.nerLabel.categoryValue+"\t"+
              0+"\t"+0+"\t"+
              token.posLabel.value+"\t"+
              //              token.posTag+"\t"+
              (if (token.startsSpansOfClass[GoldNerSpan].nonEmpty) "|B-"+token.spansOfClass[GoldNerSpan].head.labelString
              else if
              (token.spansOfClass[GoldNerSpan].nonEmpty) "|I-"+token.spansOfClass[GoldNerSpan].head.labelString
              else "|O")+"\n"
          )
        }
      }
      writer.write("\n")
    }
    writer.close()
  }

  def printDocuments(documents:Seq[Document]) : Unit = {
    for (d <- documents) printDocument(d)
  }

  def simplePrintDocument(document:Document): Unit = {
    for (token <- document) {
      token.startsSpansOfClass[NerSpan].foreach(span => System.err.print("<"+span.label.value+">"))
      System.err.print(token.string)
      token.endsSpansOfClass[NerSpan].foreach(span => System.err.print("</"+span.label.value+">"))
      System.err.print(" ")
    }
    println
    for(span: NerSpan <- document.spansOfClass[NerSpan]) System.err.println(span.label.value+"\t"+span.phrase+"\t")
  }

  def printDocument(document:Document): Unit = {
    for (token <- document) {
      token.startsSpansOfClass[NerSpan].foreach(span => print("<"+span.label.value+">"))
      print(token.string)
      token.endsSpansOfClass[NerSpan].foreach(span => print("</"+span.label.value+">"))
      print(" ")
    }
    println
    for (span <- document.spansOfClass[NerSpan].sortForward(span => span.start.toDouble)) {
      println("%s len=%-2d %-8s %-15s %-30s %-15s".format(
        if (span.isCorrect) " " else "*",
        span.length,
        span.label.value,
        if (span.hasPredecessor(1)) span.predecessor(1).string else "<START>",
        span.phrase,
        if (span.hasSuccessor(1)) span.successor(1).string else "<END>"))
    }
  }

  def printAll(i:Int,trainDocuments:Seq[Document],testDocuments:Seq[Document],valDocuments:Seq[Document],sep:String,hsym:Char) {
    println(Printer.evalListHeader("             \t",sep,hsym))
    if (trainDocuments.nonEmpty) println("TRAIN  "+"%3d".format(i)+" "+sep+"\t"+Result.evalList(trainDocuments,sep))
    if (testDocuments.nonEmpty)  println("TEST   "+"%3d".format(i)+" "+sep+"\t"+Result.evalList(testDocuments,sep))
    if (valDocuments != null && valDocuments.nonEmpty) println("VAL    "+"%3d".format(i)+" "+sep+"\t"+Result.evalList(valDocuments,sep)+"\n"+Printer.line(160,hsym))
  }

  def evalListHeader(prefix:String, sep:String, sym:Char) : String = {
    line(240,sym)+"\n"+
      prefix+"exact                                   "+sep+" partial                                 "+sep+" exact              "+sep+" partial \n"+
      prefix+"target             "+sep+" subjective         "+sep+" target             "+sep+" subjective         "+sep+" micro-avg          "+sep+" micro-avg          "+sep+sep+"        "+sep+sep+" Relation exact     "+sep+" Relation partial   "+sep+" Polarity Positive "+sep+"Polarity Negative  "+sep+"\n"+
      prefix+"P     R     F1     "+sep+" P     R     F1     "+sep+" P     R     F1     "+sep+" P     R     F1     "+sep+" P     R     F1     "+sep+" P     R     F1     "+sep+sep+" SubjAcc"+sep+sep+" P     R     F1     "+sep+" P     R     F1     "+sep+" P     R     F1    "+sep+" P     R     F1    "+sep+" Acc"+"\n"+
      line(240,sym)+""
  }
  def line(length:Int,sym:Char) : String = {
    val l = new mutable.StringBuilder()
    for (o <- 0 until length) l += sym
    l.toString()
  }

  def printToken(token:Token) : Unit = {
    //print("printToken "+token.word+"  ")
    val spans = token.spansOfClass[NerSpan]
    for (span <- spans) {
      println("%s %-8s %-15s %-30s %-15s".format(
        if (span.isCorrect) " " else "*",
        span.label.value,
        if (span.hasPredecessor(1)) span.predecessor(1).string else "<START>",
        span.phrase,
        if (span.hasSuccessor(1)) span.successor(1).string else "<END>"))
      span.foreach(token => print(token.string+" ")); println
    }
  }


  def printPolarityResults(docs:Seq[Document]) {
    for(doc <- docs) {
      println(doc.name)
      for(s:Sentence <- doc.sentences) {
        val positiveSpans = s.subjectiveSpans.filter((s:NerSpan) => s.isPositive)
        val negativeSpans = s.subjectiveSpans.filter((s:NerSpan) => s.isNegative)
//        println(">>"+s.nerSpans+" "+doc.spansOfClass[NerSpan].length)
        println("\t"+s.string)//+" ("+positiveSpans.length+" positive and "+negativeSpans.length+" negative spans ("+s.subjectiveSpans.length+"))")

        for(positiveSpan <- positiveSpans) {
          val bestGoldSpan = positiveSpan.bestGoldSpan
          println("\t\t"+positiveSpan.phrase+" ("+positiveSpan.polarity.categoryValue+") =?= "+positiveSpan.hasCorrectPolarity(true)+"\t"+bestGoldSpan.phrase+" ("+bestGoldSpan.polarity.categoryValue+")")
        }
        for(negativeSpan <- negativeSpans) {
          val bestGoldSpan = negativeSpan.bestGoldSpan
          println("\t\t"+negativeSpan.phrase+" ("+negativeSpan.polarity.categoryValue+") =?= "+negativeSpan.hasCorrectPolarity(true)+"\t"+bestGoldSpan.phrase+" ("+bestGoldSpan.polarity.categoryValue+")")
        }
        println
      }
    }
  }


  def printRelationResults(docs:Seq[Document]) {
    for(doc <- docs) {
      println(doc.name)
      for(s <- doc.sentences) {
        println(s.string)
        val goldSpansOnSentence = for(token <- s.tokens ; goldSpan <- token.startsSpansOfClass[GoldNerSpan]) yield goldSpan
        for(goldSpan <- goldSpansOnSentence) {
          println("\t"+goldSpan)
          for(relation <- goldSpan.relations) {
            println("\t\t"+relation)
          }
        }
        println("PREDICT:")
        val predictSpansOnSentence = for(token <- s.tokens ; predSpan <- token.startsSpansOfClass[NerSpan]) yield predSpan
        for(predSpan <- predictSpansOnSentence) {
          println("\t"+predSpan)
          for(relation <- predSpan.relations) {
            val obj = doc.goldTsRelations.exists(goldRel => goldRel.subjectiveNerSpan === relation.subjectiveNerSpan && goldRel.targetNerSpan === relation.targetNerSpan)
            println("\t\t"+relation+"\t"+obj)

          }
        }
        println("----------------------------")
      }
      println("==================================")
      println
    }
  }

  // TODO enrich with relations
  def saveResultsToFile(filenameStubWithExtension:String, documents:Seq[Document]) = {
    if (filenameStubWithExtension != "false") {
      val filenamestub = filenameStubWithExtension.substring(0,filenameStubWithExtension.lastIndexOf("."))
      val csvFilename = filenamestub+".csv"
      val csvfile = new File(csvFilename)
      if (!csvfile.canWrite()) {
        System.err.println("Making all directories to " + csvFilename)
        csvfile.getParentFile.mkdirs()
      }
      val csvwriter = new PrintWriter(csvfile)
      val txtfile = new File(filenamestub+".txt")
      val txtwriter = new PrintWriter(txtfile)
      println("Saving documents to \n" + csvfile.getAbsolutePath + "\n" + txtfile.getAbsolutePath)
      for (document <- documents) {
        val id = document.name.replaceAll("SubjTarget-", "")
        txtwriter.write(id + "\t" + document.subjective + "\t" + document.originalText + "\n") // should be changed when multiple sentences are taken into account!?
        for (nerSpan <- document.spansOfClass[NerSpan]) {
          try {
            csvwriter.write(
              nerSpan.label.categoryValue.replaceAll("subjective", "subj") + "\t" +
                id + "\t" +
                nerSpan.head.txtOffset._1 + "\t" + nerSpan.last.txtOffset._2 + "\t" +
                document.originalText.substring(nerSpan.head.txtOffset._1, nerSpan.last.txtOffset._2) + "\t" +
                "DUMMID" + "\t" +
                nerSpan.getPolarityString + "\t" +
                "DUMMYRELATED" +"\n"
            )
          } catch {
            case e: StringIndexOutOfBoundsException => {
              println("Index error while writing file..."); csvwriter.write("\n")
            }
            case e: Exception => {
              println("Error while writing file..."); csvwriter.write("\n")
            }
          }
        }
      }
      csvwriter.close()
      txtwriter.close()
    }
  }

  def printHTMLHeader(writer: PrintWriter) = writer.write("<!DOCTYPE html>\n      <html><head>"+css+"\n"+script+"        </head><body>\n")
  def printHTMLFooter(writer: PrintWriter) = writer.write("    </body>\n      </html>\n")
  def css : String = {
    "<style>\n" +
      "body {color:black;font-family:\"Lucida Console\", Courier, sans-serif;}\n" +
      "subjective {color:black;border-style:solid;border-width:1px;border-color:lightblue;background-color:lightblue}\n" +
      "target {color:black;border-style:solid;border-width:1px;border-color:yellow;background-color:yellow}\n" +
      //      "div.falseMention {border-style:solid;border-width:1px;border-color:red;}\n" +
      //      "div.missedMention {border-style:solid;border-width:1px;border-color:blue;}\n" +
      "</style>"
  }
  def script : String = {"<script src=\"jquery.js\"></script>\n"}
  def intro = "<target>target</target></br><subjective>subjective</subjective></br> firstline: prediction, second line: gold</br></br>\n" +
    "<table border=\"2\">\n"+
    "<tr><td>Features</td><td>Target</td><td>Subjective</td><td>Missed Mention Target</td><td>False Mention Target</td><td>Missed Mention Subj</td><td>False Mention Subj</td><td>All</td><td>No Content</td></tr>"+
    "<tr>" +
    "<td><input type=\"button\" value=\"Show\" onclick=\"$('.features').show()\" /></td>" +
    "<td><input type=\"button\" value=\"Show\" onclick=\"$('.target').show()\" /></td>" +
    "<td><input type=\"button\" value=\"Show\" onclick=\"$('.subjective').show()\" /></td>" +
    "<td><input type=\"button\" value=\"Show\" onclick=\"$('.missedMentionTarget').show()\" /></td>" +
    "<td><input type=\"button\" value=\"Show\" onclick=\"$('.falseMentionTarget').show()\" /></td>" +
    "<td><input type=\"button\" value=\"Show\" onclick=\"$('.missedMentionSubj').show()\" /></td>" +
    "<td><input type=\"button\" value=\"Show\" onclick=\"$('.falseMentionSubj').show()\" /></td>" +
    "<td><input type=\"button\" value=\"Show\" onclick=\"$('.other').show()\" /></td>" +
    "<td><input type=\"button\" value=\"Show\" onclick=\"$('.nocontent').show()\" /></td>" +
    "</tr>"+
    "<tr>" +
    "<td><input type=\"button\" value=\"Hide\" onclick=\"$('.features').hide()\" /></td>" +
    "<td><input type=\"button\" value=\"Hide\" onclick=\"$('.target').hide()\" /></td>" +
    "<td><input type=\"button\" value=\"Hide\" onclick=\"$('.subjective').hide()\" /></td>" +
    "<td><input type=\"button\" value=\"Hide\" onclick=\"$('.missedMentionTarget').hide()\" /></td>" +
    "<td><input type=\"button\" value=\"Hide\" onclick=\"$('.falseMentionTarget').hide()\" /></td>" +
    "<td><input type=\"button\" value=\"Hide\" onclick=\"$('.missedMentionSubj').hide()\" /></td>" +
    "<td><input type=\"button\" value=\"Hide\" onclick=\"$('.falseMentionSubj').hide()\" /></td>" +
    "<td><input type=\"button\" value=\"Hide\" onclick=\"$('.other').hide()\" /></td>" +
    "<td><input type=\"button\" value=\"Hide\" onclick=\"$('.nocontent').hide()\" /></td>" +
    "</tr>"+
    "</table>"


  "<input type=\"button\"   \n" +
    "                value=\"Show all with missed mentions\"   \n" +
    "                onclick=\"$('.missedMention').toggle()\" /> \n " +
    "<div class = \"missedMention\"> TestContent</div>"

  def saveHTML(goldDocuments:Seq[Document], predictDocuments:Seq[Document], htmlFilename:String, targetcentric:Boolean, showFeatures:Boolean) = {
    println("Print features: "+showFeatures)
    val htmlFile = new File(htmlFilename)
    val writer = new PrintWriter(htmlFile)
    println("Saving HTML to "+htmlFile.getAbsolutePath+"\n")
    // prepare some stuff
    val predictMap = new mutable.HashMap[String,Document]()
    for(pd <- predictDocuments) predictMap += pd.name -> pd
    // go through gold, print gold and predicted
    printHTMLHeader(writer)
    writer.write(intro)
    if (targetcentric) { // target centric, therefore in order tp, fp, fn
    // get feature weight file
    val featureWeights = new mutable.HashMap[(String,String,String),String]()
      if (showFeatures && TSProp.saveWeightsToFile != "false" && new File(TSProp.saveWeightsToFile).exists) {
        for(weightLine <- Source.fromFile(new File(TSProp.saveWeightsToFile)).getLines) {
          //SPAN    B-POS=NNP       target  -0.68963544
          val e = weightLine.split("\t")
          val templateType=e(0) ; val featureName=e(1); val label=e(2); val weight=e(3)
          featureWeights += (templateType,featureName,label) -> weight
        }
      }
      for(document <- goldDocuments) {
        val id = document.name.replaceAll("SubjTarget-","")
        val pd = predictMap(document.name)
        // tp
        writer.write(id+"</br>\n")
        writer.write("<b>TP</b>"+"</br>\n")
        writer.write("<div align=\"center\" class=\""+ ( "other " ) + "\">")
        for(correct <- pd.spansOfClass[GoldNerSpan].filter(ps => { document.spansOfClass[GoldNerSpan].exists(gs => ps === gs) } ) ) {
          writer.write("<div class=\""+ correct.labelString + "\">")
          writer.write(""+correct.sgmlString+"</br></br>\n")
          if (showFeatures) {
            val features = new TSSpanFeatures(correct.toSeq,correct.label.categoryValue)
            val featuresJ = new TSSpanFeaturesJoint(correct.toSeq,correct.label.categoryValue)
            val featureString =  features.activeCategories.sortBy(f => (if (featureWeights.contains(("SPAN",f,correct.labelString))) featureWeights("SPAN",f,correct.labelString).toDouble else 0.0)).
              map(f => f+" = "+(if (featureWeights.contains(("SPAN",f,correct.labelString))) featureWeights("SPAN",f,correct.labelString) else 0.0))
            val featureJString =  featuresJ.activeCategories.sortBy(f => (if (featureWeights.contains(("JOINT",f,correct.labelString))) featureWeights("JOINT",f,correct.labelString).toDouble else 0.0)).
              map(f => f+" = "+(if (featureWeights.contains(("JOINT",f,correct.labelString))) featureWeights("JOINT",f,correct.labelString) else 0.0))
            writer.write("<div class=\"features\" align=\"right\">"+featureString.mkString("<br/>")+"</div>")
            writer.write("<div class=\"features\" align=\"right\">"+featureJString.mkString("<br/>")+"</div>")
          }
          writer.write("</div>")
        }
        writer.write("<hr/>\n")
        writer.write("</div>")
        // fp
        writer.write("<b>FP</b>"+"</br>\n")
        writer.write("<div align=\"center\" class=\""+ ( "other " ) + "\">")
        for(falsee <- pd.spansOfClass[GoldNerSpan].filter(ps => { ! document.spansOfClass[GoldNerSpan].exists(gs => ps === gs) } ) ) {
          writer.write("<div class=\""+ falsee.labelString + (if (falsee.labelString=="target") " falseMentionTarget" else " falseMentionSubj" ) +"\">")
          writer.write("<div style=\"color:black\">"+falsee.sgmlString+"</div>\n")
          writer.write("<div style=\"color:gray\">"+Token.sgmlString(document.tokens.slice(falsee.sgmlArea._1,falsee.sgmlArea._2))+"</div><br/>\n")
          if (showFeatures) {
            val features = new TSSpanFeatures(falsee.toSeq,falsee.label.categoryValue)
            val featuresJ = new TSSpanFeaturesJoint(falsee.toSeq,falsee.label.categoryValue)
            val featureString =  features.activeCategories.sortBy(f => (if (featureWeights.contains(("SPAN",f,falsee.labelString))) featureWeights("SPAN",f,falsee.labelString).toDouble else 0.0)).
              map(f => f+" = "+(if (featureWeights.contains(("SPAN",f,falsee.labelString))) featureWeights("SPAN",f,falsee.labelString) else 0.0))
            val featureJString =  featuresJ.activeCategories.sortBy(f => (if (featureWeights.contains(("JOINT",f,falsee.labelString))) featureWeights("JOINT",f,falsee.labelString).toDouble else 0.0)).
              map(f => f+" = "+(if (featureWeights.contains(("JOINT",f,falsee.labelString))) featureWeights("JOINT",f,falsee.labelString) else 0.0))
            writer.write("<div class=\"features\" align=\"right\">"+featureString.mkString("<br/>")+"</div>")
            writer.write("<div class=\"features\" align=\"right\">"+featureJString.mkString("<br/>")+"</div>")
          }
          writer.write("</div>")
        }
        writer.write("<hr/>\n")
        writer.write("</div>")
        // fn
        writer.write("<b>FN</b>"+"</br>\n")
        writer.write("<div align=\"center\" class=\""+ ( "other " ) + "\">")
        for(missed <- document.spansOfClass[GoldNerSpan].filter(gs => { ! pd.spansOfClass[GoldNerSpan].exists(ps => ps === gs) } ) ) {
          writer.write("<div class=\""+ missed.labelString + (if (missed.labelString=="target") " missedMentionTarget" else " missedMentionSubj" ) +"\">")
          writer.write("<div style=\"color:black\">"+Token.sgmlString(pd.tokens.slice(missed.sgmlArea._1,missed.sgmlArea._2))+"</div>\n")
          writer.write("<div style=\"color:gray\">"+missed.sgmlString+"</div><br/>\n")
          if (showFeatures) {
            val features = new TSSpanFeatures(missed.toSeq,missed.label.categoryValue)
            val featuresJ = new TSSpanFeaturesJoint(missed.toSeq,missed.label.categoryValue)
            val featureString =  features.activeCategories.sortBy(f => (if (featureWeights.contains(("SPAN",f,missed.labelString))) featureWeights("SPAN",f,missed.labelString).toDouble else 0.0)).
              map(f => f+" = "+(if (featureWeights.contains(("SPAN",f,missed.labelString))) featureWeights("SPAN",f,missed.labelString) else 0.0))
            val featureJString =  featuresJ.activeCategories.sortBy(f => (if (featureWeights.contains(("JOINT",f,missed.labelString))) featureWeights("JOINT",f,missed.labelString).toDouble else 0.0)).
              map(f => f+" = "+(if (featureWeights.contains(("JOINT",f,missed.labelString))) featureWeights("JOINT",f,missed.labelString) else 0.0))
            writer.write("<div class=\"features\" align=\"right\">"+featureString.mkString("<br/>")+"</div>")
            writer.write("<div class=\"features\" align=\"right\">"+featureJString.mkString("<br/>")+"</div>")
          }
          writer.write("</div>")
        }
        writer.write("<hr/>\n")
        writer.write("</div>")
      }
    } else {
      for(document <- goldDocuments) {
        if (!predictMap.isEmpty) {
          val pd = predictMap(document.name)
          val falseMentionTarget = pd.spansOfClass[GoldNerSpan].filter(s=>s.label.categoryValue=="target").exists(predSpan => !document.spansOfClass[GoldNerSpan].exists(goldSpan => goldSpan === predSpan))
          val missedMentionTarget = document.spansOfClass[GoldNerSpan].filter(s=>s.label.categoryValue=="target").exists(goldSpan => !pd.spansOfClass[GoldNerSpan].exists(predSpan => predSpan === goldSpan))
          val falseMentionSubj = pd.spansOfClass[GoldNerSpan].filter(s=>s.label.categoryValue=="subjective").exists(predSpan => !document.spansOfClass[GoldNerSpan].exists(goldSpan => goldSpan === predSpan))
          val missedMentionSubj = document.spansOfClass[GoldNerSpan].filter(s=>s.label.categoryValue=="subjective").exists(goldSpan => !pd.spansOfClass[GoldNerSpan].exists(predSpan => predSpan === goldSpan))
          val nocontent = document.spansOfClass[GoldNerSpan].size == 0 && pd.spansOfClass[GoldNerSpan].size == 0
          val id = document.name.replaceAll("SubjTarget-","")
          writer.write("<div class=\""+
            (if (falseMentionTarget) "falseMentionTarget " else "") +
            (if (missedMentionTarget) "missedMentionTarget " else "") +
            (if (falseMentionSubj) "falseMentionSubj " else "") +
            (if (missedMentionSubj) "missedMentionSubj " else "") +
            ( "other " ) +
            (if (nocontent) "nocontent " else "")
            +"\">")
          writer.write(id+"</br>\n")
          writer.write(""+document.sgmlString+"</br>\n")
          writer.write(""+pd.sgmlString+"</br>\n")
          writer.write("<hr/>\n")
          writer.write("</div>")
        } else { // join these two cases in a nicer way!
        val nocontent = document.spansOfClass[GoldNerSpan].size == 0
          val id = document.name.replaceAll("SubjTarget-","")
          writer.write("<div class=\""+
            ( "other " ) +
            (if (nocontent) "nocontent " else "")
            +"\">")
          writer.write(id+"</br>\n")
          writer.write(""+document.sgmlString+"</br>\n")
          writer.write("<hr/>\n")
          writer.write("</div>")
        }
      }
    }
    writer.close()
  }

  def printComparison(documents:Seq[Document], showFalseResults: Boolean, showLengthDistribution: Boolean, showAverageNumOfSpansPerToken: Boolean) : Unit = {
    if (showLengthDistribution) {
      // distribution of span lengths
      val goldlengthCount = new mutable.HashMap[Int,Int]
      val predlengthCount = new mutable.HashMap[Int,Int]
      for(d <- documents) {
        for (predictedSpan <- d.spansOfClass[NerSpan]) {
          if (!predlengthCount.contains(predictedSpan.length)) predlengthCount += predictedSpan.length -> 0
          predlengthCount(predictedSpan.length) += 1
        }
        for (goldSpan <- d.spansOfClass[GoldNerSpan]) {
          if (!goldlengthCount.contains(goldSpan.length)) goldlengthCount += goldSpan.length -> 0
          goldlengthCount(goldSpan.length) += 1
        }
      }
      println("Length\tpred\tgold")
      for(i <- 0 until 30) {
        print(i+"\t")
        print((if (predlengthCount.contains(i)) predlengthCount(i) else "") + "\t")
        print((if (goldlengthCount.contains(i)) goldlengthCount(i) else "") + "\t")
        println
      }
    }
    if (showFalseResults) {
      for(d <- documents) {
        val targResult = Result.eval(d,"target",false,false)
        val subjResult = Result.eval(d,"subjective",false,false)
        //(trueCount,predictedCount,correctCount)
        if (targResult._1 > targResult._3 || subjResult._1 > subjResult._3) {
          println(d.string)
          println(d.tokens.map(_.docSubstring).mkString(" "))
          println("Gold:")
          for(s <- d.spansOfClass[GoldNerSpan])
            println("\t"+s.labelString+"\t"+s.string)
          println("Predicted:")
          for(s <- d.spansOfClass[NerSpan])
            println("\t"+s.label.value+"\t"+s.string)
          println("Target results (true,predicted,correct)"+targResult)
          println("Subjec results (true,predicted,correct)"+subjResult)
        }
      }
    }
    if (showAverageNumOfSpansPerToken) {
      var sum = 0.0
      var num = 0.0
      for (d <- documents ; t <- d) {
        sum += t.spansOfClass[NerSpan].length.toDouble
        num += 1.0
      }
      println("Average number of spans of NerSpan per token: "+(sum/num))
    }
  }

  def saveWeightsToFile(model:SpanNerModel,filename:String) = {
    System.err.println("WRITING WEIGHTS IS CURRENTLY NOT WORKING!")
  }
  //    def matrix(pos:Int,dim1:Int,dim2:Int) : (Double,Int) = {
  ////      println(pos+" "+dim1+" "+dim2)
  //      val j = pos % dim2
  //      val i = (pos-j).toDouble/dim1.toDouble
  //      (i,j)
  //    }
  //    val f = new File(filename)
  //    val writer = new PrintWriter(f)
  //    // this is highly specific for my model!
  //    val biasmodel = model.subModels(0)  // TSNerDomain
  //    val spanmodel = model.subModels(1)  // TSSpanFeaturesDomain x TSNerDomain
  //    val jointmodel = model.subModels(2) // TSSpanFeaturesJointDomain x TSNerDomain
  //    // TODO fix this: might be templates, but as the model structure changed, this is not clear
  //
  ////    biasmodel.weightsTensor.foreachActiveElement((i,w) => writer.write("BIAS\t\t"+TSNerDomain(i)+"\t"+"%.8f".format(w)+"\n")) // TODO fix
  //    val spanweights = spanmodel.asInstanceOf[DotTemplate2[NerSpan,SpanNerLabel]].weights.asInstanceOf[DenseTensor2]
  //    for(i <- 0 until spanweights.dim1 ; j <- 0 until spanweights.dim2) {
  //      val w = spanweights(i,j)
  //      if (w != 0.0) writer.write("SPAN\t"+TSSpanFeaturesDomain.dimensionDomain(i)+"\t"+TSNerDomain(j)+"\t"+"%.8f".format(w)+"\n")
  //    }
  //    val jointweights = jointmodel.asInstanceOf[DotTemplate2[NerSpan,SpanNerLabel]].weights.asInstanceOf[DenseTensor2]
  //    for(i <- 0 until jointweights.dim1 ; j <- 0 until jointweights.dim2) {
  //      val w = jointweights(i,j)
  //      if (w != 0.0) writer.write("JOINT\t"+TSSpanFeaturesJointDomain.dimensionDomain(i)+"\t"+TSNerDomain(j)+"\t"+"%.8f".format(w)+"\n")
  //    }
  //    writer.close()
  //  }

  def main(args:Array[String]) = {
    val trainDocuments:Seq[Document] = Seq.empty[Document]
    val sep = "|"
    val hsym = '-'
    println(Printer.evalListHeader("             \t",sep,hsym))
    println("TRAIN  "+"%3d".format(5)+" "+sep+"\t"+new Result().toString(sep))
  }
}
