/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.tools

import sc.rk.targsubj.data.LoadTargetSubjectivity

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 07.03.13
 * Time: 11:29
 */
object ConvertToIOB {
  def main(args:Array[String]) {
    val trd = LoadTargetSubjectivity.fromFilename("/Users/rklinger/ub/workspace/target-subjectivity/data/input/jdpa/camera-train.csv",true,false,false)
    Printer.printIOB(trd, "/Users/rklinger/ub/workspace/target-subjectivity/data/input/jdpa/camera-train.iob")
    val ted = LoadTargetSubjectivity.fromFilename("/Users/rklinger/ub/workspace/target-subjectivity/data/input/jdpa/camera-test.csv",true,false,false)
    Printer.printIOB(ted, "/Users/rklinger/ub/workspace/target-subjectivity/data/input/jdpa/camera-test.iob")
  }
}
