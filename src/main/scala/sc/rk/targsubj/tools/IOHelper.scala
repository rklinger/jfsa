package sc.rk.targsubj.tools

import java.io._
import java.util.zip.{GZIPInputStream, GZIPOutputStream}

import sc.rk.targsubj._
import sc.rk.targsubj.data.{TSProp, SerializableContainerModel}


/**
 * Created by rklinger on 3/17/15.
 */
object IOHelper {
//  def saveModel(model:TemplateModel,fileName:String) = {
//    System.err.println("Storing model to "+fileName)
//    val oos = new ObjectOutputStream(new FileOutputStream(fileName))
//    oos.writeObject(model)
//    oos.close
//  }

  def saveModel(model:SerializableContainerModel,fileName:String) = {
    if (fileName == "empty") {
      System.err.println("Model not stored. No filename provided!")
    } else {
      System.err.println("Storing container model to "+fileName)
      val modelFile = new File(fileName)
      if (!modelFile.canWrite) {
        System.err.println("Making all directories to "+modelFile.getAbsolutePath)
        modelFile.getParentFile.mkdirs()
      }

      val oos = new ObjectOutputStream(new GZIPOutputStream(new FileOutputStream(fileName)))
      oos.writeObject(model)
      oos.close
    }
  }

  def loadModel(fileName:String) : SerializableContainerModel = {
    val modelFile = new File(fileName)
    if (!modelFile.exists()) {
      System.err.println("File does not exist: "+fileName)
      System.exit(1)
    }
    System.err.println("Reading a model from "+modelFile)
    val ois = new ObjectInputStream(new GZIPInputStream(new FileInputStream(modelFile)))
    val theobject = ois.readObject().asInstanceOf[SerializableContainerModel]
    // TSProp
    TSProp.properties = theobject.properties
    TSProp.storeProperties
    // Domains -- probably that does not suffice
    RelationFeaturesDomain.dimensionDomain ++= theobject.relationFeaturesDomain.categories
    TSSpanFeaturesDomain.dimensionDomain ++= theobject.tSSpanFeaturesDomain.categories
    TSSpanFeaturesJointDomain.dimensionDomain ++= theobject.tSSpanFeaturesJointDomain.categories
    SubjectiveClassificationFeaturesDomain.dimensionDomain ++= theobject.subjectiveClassificationFeaturesDomain.categories
    PolarityClassificationFeaturesDomain.dimensionDomain ++= theobject.polarityClassificationFeaturesDomain.categories
    theobject
  }
}
