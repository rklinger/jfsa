/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.tools

import sc.rk.targsubj.data._
import collection.mutable.HashSet
import cc.factorie.app.strings.PorterStemmer
import io.Source

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 05.11.13
 * Time: 14:09
 */
class WilsonDictionaryPriorPolarityChecker(filename:String) extends Serializable { // Following Wilson, Wiebe et al 2005
  val positive = new HashSet[String]
  val negative = new HashSet[String]
  
  readDictionary(filename)

  def tokenIs(t:Token, list:HashSet[String]) : Boolean = {
    val string = t.string.toLowerCase.trim
    val stem = PorterStemmer(string.trim).toLowerCase
//    System.err.println("Checking for "+string+" "+list.contains(string)+"-"+list.contains(stem)+"-"+list.exists(d => d.startsWith(stem)))
    list.contains(string) || list.contains(stem) || list.exists(d => d.startsWith(stem))
  }

  def tokenIsPositive(t:Token) : Boolean = tokenIs(t,positive)
  def tokenIsNegative(t:Token) : Boolean = tokenIs(t,negative)
  def polarity(t:Token) : String = if (tokenIsPositive(t)) "positive" else if (tokenIsNegative(t)) "negative" else "none"

  // the format is:
  // word\t[positive;negative]
  // TODO this still needs to be adapted for sentiwordnet to get phrases
  def readDictionary(filename:String) {
    val src = Source.fromFile(new java.io.File(filename))
    for(line <- src.getLines()) {
      val split = line.split("\t")
      val word = split(0)
      val polarity = split(1)
      if (polarity == "positive") {
//        System.err.println("Adding "+word)
        positive += word.toLowerCase
        positive += PorterStemmer(word).toLowerCase
      }
      if (polarity == "negative") {
        negative += word.toLowerCase
        negative += PorterStemmer(word).toLowerCase
      }
    }
  }

  def main(args:Array[String]) : Unit = {
    readDictionary("data/dic/wilson2005.csv")
    val str = ""
    val doc = new Document("TestDocument", "", str)
    val sentence = new Sentence(doc)(null)
    val t1 = new Token(sentence, "Great ") ; t1.setSentence(sentence)
    val t2 = new Token(sentence, "is ") ; t2.setSentence(sentence)
    val t3 = new Token(sentence, "without ") ; t3.setSentence(sentence)
    val t4 = new Token(sentence, "need ") ; t4.setSentence(sentence)
    val t5 = new Token(sentence, "token5 ") ; t5.setSentence(sentence)
    val t6 = new Token(sentence, "token6") ; t6.setSentence(sentence)

    for(t <- sentence) {
      println(t+" : "+t.string+":"+polarity(t))
    }
//    val tokenizer = new StanfordTokenizer(true)
//    val tokens = tokenizer.tokenize()
//    val tree = tokenizer.parser.parse("This is just a great blabla.")
//    tokenizer.parser.


  }
}
