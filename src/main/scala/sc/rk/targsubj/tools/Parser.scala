/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.tools

import edu.stanford.nlp.parser.lexparser.LexicalizedParser
import edu.stanford.nlp.trees._
import edu.stanford.nlp.pipeline.ParserAnnotatorUtils
import edu.stanford.nlp.ling.IndexedWord
import org.jgrapht.graph.{SimpleDirectedGraph, SimpleGraph}
import scala.collection.JavaConversions._
import org.jgrapht.alg.FloydWarshallShortestPaths
import collection.mutable.ListBuffer
import java.io._
import sc.rk.targsubj.data.TSProp


/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 15.03.13
 * Time: 15:35
 */
class Parser(lang:String) {
  val lp:LexicalizedParser = if (lang == "de") {
    System.err.println("No Dependency Model for German implemented! Turning off all parsing related features!")
    System.err.println("Turn off Stanford Parser for German and use something else!")
    System.err.println("I propose you set parsingShortestEdgePath = false and parsingOneEdge = false and stanfordParser = false")
    System.err.println("As a shortcut, I do that when LANG=de, so you should not see this message!")
    System.exit(1)
    LexicalizedParser.getParserFromSerializedFile("data/sfmodel/germanPCFG.ser.gz") // that does not work, it is just here to make the code compilable
  } else {
    LexicalizedParser.getParserFromSerializedFile("data/sfmodel/englishPCFG.caseless.ser.gz")
  }
  //lp.setOptionFlags("-outputFormat", "penn,typedDependenciesCollapsed", "-retainTmpSubcategories")
  lp.setOptionFlags("-outputFormat", "penn,typedDependenciesCollapsed")

  def exampleSP(tree:Tree) {
    val semGraph = ParserAnnotatorUtils.generateCCProcessedDependencies(tree)
    for (a <- semGraph.vertexListSorted().toArray ; b <- semGraph.vertexListSorted().toArray) {
      val sp = semGraph.getShortestUndirectedPathEdges(a.asInstanceOf[IndexedWord],b.asInstanceOf[IndexedWord])
      println(sp)
    }
  }

  def parse(sentenceTxt:String) : Tree= {
    // sometimes, the sentenceTxt happens to be empty, this should not lead to an error, workaround...
    var toParse = sentenceTxt
    if (sentenceTxt.trim.length==0) toParse = "."
    val parseTree:Tree = lp.apply(toParse)
    parseTree
  }

  def emptyTree() : Tree = {
    lp.apply(".")
  }

  def tokenizeOnly(s: String) : List[String] = {
    val tf = lp.getOp.tlpParams.treebankLanguagePack().getTokenizerFactory();
    val tokenizer = tf.getTokenizer(new BufferedReader(new StringReader(s)));
    val lst = tokenizer.tokenize();
    val rlst = new ListBuffer[String]
    lst.toList.foreach(rlst += _.word)
    rlst.toList
  }

  def saveToCache(id:String, tree:Tree) = {
    val printer = lp.getTreePrint()
    val writer = new PrintWriter(new File(TSProp.treetmp+"/"+id))
    printer.printTree(tree, writer)
  }
}

object Parser {
  def cacheExistent(id:String) : Boolean = {
    val f:File = new File(TSProp.treetmp+"/"+id)
    f.exists && f.canRead
  }

  def loadFromCache(id:String) : Tree = {
    val lstrf = new LabeledScoredTreeReaderFactory
    val tr = lstrf.newTreeReader(new FileReader(TSProp.treetmp+"/"+id))
    val tree = tr.readTree()
    tree
  }

  def treeToTokensAndPOS(tree:Tree) : List[(String,String)] = {
    val tl = new ListBuffer[(String,String)]
    for(x <- tree.taggedYield()) {
      //      println(x.word+" "+x.tag)
      tl += {(x.word,x.tag)}
    }
    tl.toList
  }

  def showAllShortestPaths(fwsp:FloydWarshallShortestPaths[TreeGraphNode,(GrammaticalRelation,Boolean)]) {
    val graph = fwsp.getGraph
    for(a <- graph.vertexSet() ; b <- graph.vertexSet()) {
      val sp = fwsp.getShortestPath(a,b)
      println(a+" -> "+b+": "+sp)
    }
  }

  def treeToSD(tree:Tree) : List[TypedDependency] = {
    val tlp:TreebankLanguagePack = new PennTreebankLanguagePack()
    val gsf:GrammaticalStructureFactory = tlp.grammaticalStructureFactory()
    val gs:GrammaticalStructure = gsf.newGrammaticalStructure(tree)
    val tdl = gs.typedDependenciesCollapsed()
    tdl.toList
  }

  def computeFloydWarshall(dependencies:List[TypedDependency]) : FloydWarshallShortestPaths[TreeGraphNode,(GrammaticalRelation,Boolean,Int)] = {
    var edgeIndex = 0
    val graph = new SimpleDirectedGraph[TreeGraphNode,(GrammaticalRelation,Boolean,Int)](classOf[(GrammaticalRelation,Boolean,Int)])
    for(td <- dependencies) {
      graph.addVertex(td.gov)
      graph.addVertex(td.dep)
      graph.addEdge(td.gov, td.dep, (td.reln,true,edgeIndex)) // forward
      graph.addEdge(td.dep, td.gov, (td.reln,false,edgeIndex)) // backward // xxx
      edgeIndex += 1
    }
    val fwsp = new FloydWarshallShortestPaths[TreeGraphNode,(GrammaticalRelation,Boolean,Int)](graph)
    fwsp
  }

  def main(args:Array[String]) = {
    TSProp.treetmp = "tmp"
    val sentence = "This guy is really a nice one, who should, when he knows, go home."
    val parser = new Parser("en")
    println(parser.emptyTree())
    println("----")
    val tree = parser.parse(sentence)
//    parser.saveToCache("someid",tree)

//    val anotherTree = Parser.loadFromCache("someid")
//    println(anotherTree)

//    parser.saveToCache("anotherId",anotherTree)

    val sd = Parser.treeToSD(tree)
    val fwsp = Parser.computeFloydWarshall(sd)
    println(fwsp.getGraph)
    println(fwsp.getGraph.vertexSet().head)
    println(fwsp.getGraph.edgesOf(fwsp.getGraph.vertexSet().head).getClass)
//    sd.
//    parser.showAllShortestPaths(fwsp)
//    val tokensNLabels = Parser.treeToTokensAndPOS(tree)
//    fwsp.getGraph.edgesOf(tree.get)
  }
}