/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */


package sc.rk.targsubj.tools

import sc.rk.targsubj.data.{Token, Sentence, Document}
import com.clearnlp.nlp.{NLPLib, NLPGetter}
import com.clearnlp.component.AbstractComponent
import com.clearnlp.tokenization.AbstractTokenizer
import com.clearnlp.dependency.DEPTree
import com.clearnlp.reader.AbstractReader
import scala.collection.JavaConversions._

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 28.11.13
 * Time: 18:16
 * A helper class to extract features via semantic role labeling. This is using ClearNLP.
 */
class SemanticRoleLabeler {
  val components = init

  def init() : List[AbstractComponent] = {
    System.err.println("Initializing SRL (clearNLP)...")
    val modelType = "general-en" // can be medical-en
    val language : String = AbstractReader.LANG_EN
    val tokenizer  = NLPGetter.getTokenizer(language);
    val tagger     = NLPGetter.getComponent(modelType, language, NLPLib.MODE_POS);
    val parser     = NLPGetter.getComponent(modelType, language, NLPLib.MODE_DEP);
    val identifier = NLPGetter.getComponent(modelType, language, NLPLib.MODE_PRED);
    val classifier = NLPGetter.getComponent(modelType, language, NLPLib.MODE_ROLE);
    val labeler    = NLPGetter.getComponent(modelType, language, NLPLib.MODE_SRL);
    val components = List(tagger, parser, identifier, classifier, labeler)
    System.err.println("...Initialized SRL (clearNLP)")
    components
  }

  def process(tokenizer:AbstractTokenizer, sentence:String) {
    val tree:DEPTree = NLPGetter.toDEPTree(tokenizer.getTokens(sentence))
    for (component <- components) component.process(tree)
//    System.out.println(tree.toStringSRL()+"\n")
  }
  def process(tokens: List[String]) : DEPTree = {
    val tree:DEPTree = NLPGetter.toDEPTree(tokens)
    for (component <- components) component.process(tree)
//    System.out.println(tree.toStringSRL()+"\n")
    tree
  }

  def labelDocuments(documents:Seq[Document]) : Unit = {
    documents.foreach(labelDocument(_))
  }
  def labelDocument(doc:Document) : Unit = {
    doc.sentences.foreach(labelSentence(_))
  }

  /**
   * Attaches SRL information to tokens.
   * @param sentence
   */
  def labelSentence(sentence:Sentence) : Unit = {
    //sentence.tokens.map(t => t.docSubstring).foreach(x => println(">"+x+"<"))
    val tokens = sentence.tokens.map(t => t.docSubstring.trim).toList
    val srltree = process(tokens)
    attachSRLToTokens(srltree,sentence)
  }

  def attachSRLToTokens(tree:DEPTree,sentence:Sentence) = {
//    println(tree.toStringSRL)
//    System.err.println(tree.size()+" -- "+sentence.size)
    for(i <- 1 until tree.size()) {
//      System.err.println(sentence.tokens(i-1)+" - "+tree.get(i).toStringSRL)
      val sheads = tree.get(i).getSHeads
      for (j <- 0 until sheads.size) {
//        System.err.println(sheads(j).getNode.id+" <> "+sheads(j).getLabel)
        val fromToken = sentence.tokens(i-1)
        val toToken = sentence.tokens(sheads(j).getNode.id-1)
        //System.err.println(fromToken+" -> "+toToken)
        val label = sheads(j).getLabel
        fromToken.addSRLLink(label,toToken,true)
        toToken.addSRLLink(label,fromToken,false)
      }
    }
  }
}

object SRLTest {
  def main(args : Array[String]) : Unit = {
    System.err.println("Test SRL...")
    val srl = new SemanticRoleLabeler
//    val start =  System.currentTimeMillis()
//    for(i <- 1 to 1000) {
//      val tokens = List("I","like","to","break","my","phone")
//      srl.process(tokens)
//    }
//    val end = System.currentTimeMillis()
//    val runtime = (end-start).toDouble/1000
//    System.err.println(runtime+"s")

    val str = "Here comes a test text."
    val doc = new Document("TestDocument", "", str)
    val sentence = new Sentence(doc)(null)
    val t1 = new Token(sentence, "Here ") ; t1.setSentence(sentence)
    val t2 = new Token(sentence, "comes ") ; t2.setSentence(sentence)
    val t3 = new Token(sentence, "a ") ; t3.setSentence(sentence)
    val t4 = new Token(sentence, "test ") ; t4.setSentence(sentence)
    val t5 = new Token(sentence, "text ") ; t5.setSentence(sentence)
    val t6 = new Token(sentence, ".") ; t6.setSentence(sentence)

    srl.labelSentence(sentence)

  }
}