/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.tools

import collection.immutable.HashSet
import sc.rk.targsubj.data.{Sentence, Document, Token}

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 31.10.13
 * Time: 13:48
 */
object NegationChecker { // that is by far not complete! Just a test!
  val negationTerms = Set(
  "without","not","doesn't","n't","avoid","hardly","should","would"
  )
  val contrastTerms = Set(
    "but","however","unfortunately"
  )
  def tokenIsNegation(t:Token) : Boolean = {
    val checkString = t.string.trim
//    println("Checking: "+checkString+" in "+negationTerms)
    negationTerms.contains(checkString)
  }

  def main(args:Array[String]) : Unit = {
    val str = ""
    val doc = new Document("TestDocument", "", str)
    val sentence = new Sentence(doc)(null)
    val t1 = new Token(sentence, "this ") ; t1.setSentence(sentence)
    val t2 = new Token(sentence, "is ") ; t2.setSentence(sentence)
    val t3 = new Token(sentence, "without ") ; t3.setSentence(sentence)
    val t4 = new Token(sentence, "need ") ; t4.setSentence(sentence)
    val t5 = new Token(sentence, "token5 ") ; t5.setSentence(sentence)
    val t6 = new Token(sentence, "token6") ; t6.setSentence(sentence)

    for(t <- sentence) {
      println(t+" : "+t.string+":"+tokenIsNegation(t))
    }
  }
}
