/**
 * Copyright (C) 2013 Roman Klinger
 *
 * This file is part of the Joint Fine-Grained Sentiment Analysis Tool (JFSA).
 * JFSA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * JFSA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Copyright (C).  If not, see <http://www.gnu.org/licenses/>.
 */

package sc.rk.targsubj.tools

import collection.mutable.ArrayBuffer
import scala.Predef._
import scala.Int
import edu.stanford.nlp.trees.{GrammaticalRelation, TreeGraphNode, Tree}
import org.jgrapht.alg.FloydWarshallShortestPaths

/**
 * Factor Graph oriented Sentiment Analysis
 * User: rklinger
 * Date: 15.03.13
 * Time: 12:11
 * A helper class to do sentence splitting with the easy simple sentence splitting from plain java.
 */
class TempSentence(val sentenceTxt:String,val leftOffsetInDocument:Int,val rightOffsetInDocument:Int,
                   var fw:FloydWarshallShortestPaths[TreeGraphNode,(GrammaticalRelation,Boolean,Int)]=null,var tree:Tree=null) {
  override def toString:String = leftOffsetInDocument+":"+rightOffsetInDocument+";"+sentenceTxt
}


object SentenceSplitter {
  private def sentenceSplit(txt:String) : ArrayBuffer[TempSentence] = {
    val sentences = new ArrayBuffer[TempSentence]
    val si = java.text.BreakIterator.getSentenceInstance
    si.setText(txt)
    var l:Int = 0 ;
    var b:Int = -2 ;
    while(b != -1) {
      b = si.next(1)
      if (b != -1) {
        val start = l
        val length = b-l // right offsets?
        //        println(start+" "+(length))
        val sentenceTxt = txt.substring(l, b)
        sentences += new TempSentence(sentenceTxt,start,length)
        l = b
      }
    }
    sentences
  }


  def split(txt:String) : ArrayBuffer[TempSentence] = {
    sentenceSplit(txt)
  }
}