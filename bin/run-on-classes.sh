#!/bin/bash 

ROOT=`dirname $0`/..
echo "Started on: "`hostname`
echo "ROOT="$ROOT
echo "PATH="$PATH
echo "HOME="$HOME
echo "JAVA_HOME="$JAVA_HOME

CLASSPATH=$CLASSPATH:$ROOT/target/classes
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/org/scala-lang/scala-library/2.10.1/scala-library-2.10.1.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/net/sourceforge/jregex/jregex/1.2_01/jregex-1.2_01.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/junit/junit/4.10/junit-4.10.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/org/hamcrest/hamcrest-core/1.1/hamcrest-core-1.1.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/org/mongodb/mongo-java-driver/2.7.3/mongo-java-driver-2.7.3.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/edu/stanford/nlp/stanford-parser/2.0.2/stanford-parser-2.0.2.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/edu/stanford/nlp/stanford-corenlp/1.3.4/stanford-corenlp-1.3.4.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/xom/xom/1.2.5/xom-1.2.5.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/xml-apis/xml-apis/1.3.03/xml-apis-1.3.03.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/xerces/xercesImpl/2.8.0/xercesImpl-2.8.0.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/xalan/xalan/2.7.0/xalan-2.7.0.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/joda-time/joda-time/2.1/joda-time-2.1.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/de/jollyday/jollyday/0.4.7/jollyday-0.4.7.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/javax/xml/bind/jaxb-api/2.2.7/jaxb-api-2.2.7.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/net/sf/jgrapht/jgrapht/0.8.3/jgrapht-0.8.3.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/com/clearnlp/clearnlp/2.0.1/clearnlp-2.0.1.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/args4j/args4j/2.0.23/args4j-2.0.23.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/log4j/log4j/1.2.17/log4j-1.2.17.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/com/carrotsearch/hppc/0.5.2/hppc-0.5.2.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/com/google/guava/guava/14.0.1/guava-14.0.1.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/com/clearnlp/clearnlp-dictionary/1.0/clearnlp-dictionary-1.0.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/com/clearnlp/clearnlp-general-en-pos/1.1/clearnlp-general-en-pos-1.1.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/com/clearnlp/clearnlp-general-en-dep/1.2/clearnlp-general-en-dep-1.2.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/com/clearnlp/clearnlp-general-en-srl/1.1/clearnlp-general-en-srl-1.1.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/cc/factorie/factorie/1.0.0-M7/factorie-1.0.0-M7.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/com/typesafe/akka/akka-actor_2.10/2.1.4/akka-actor_2.10-2.1.4.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/com/typesafe/config/1.0.0/config-1.0.0.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/org/scala-lang/scala-compiler/2.10.1/scala-compiler-2.10.1.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/org/scala-lang/scala-reflect/2.10.1/scala-reflect-2.10.1.jar
CLASSPATH=$CLASSPATH:$HOME/.m2/repository/org/jblas/jblas/1.2.3/jblas-1.2.3.jar
CLASSPATH=$CLASSPATH:$HOME/ub/workspace/target-subjectivity/3rdparty/ark-tweet-nlp/ark-tweet-nlp-0.3.2.jar

echo "CLASSPATH=$CLASSPATH"

java -Xmx3g -XX:+UseConcMarkSweepGC -cp $CLASSPATH sc.rk.targsubj.TargSubjSpanNER $1
