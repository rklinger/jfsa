#!/bin/bash 

ROOT=`dirname $0`/..
echo "Started on: "`hostname`
echo "ROOT="$ROOT
echo "PATH="$PATH
echo "HOME="$HOME
echo "JAVA_HOME="$JAVA_HOME

java -Xmx3g -XX:+UseConcMarkSweepGC -cp $ROOT/target/jfsa-0.1.jar:$ROOT/target/jfsa-0.1-jar-with-dependencies.jar sc.rk.targsubj.TargSubjSpanNER $@
